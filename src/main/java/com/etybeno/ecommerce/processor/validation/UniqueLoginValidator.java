package com.etybeno.ecommerce.processor.validation;

import com.etybeno.ecommerce.annotations.UniqueLogin;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by thangpham on 08/02/2018.
 */
public class UniqueLoginValidator implements ConstraintValidator<UniqueLogin, String> {

    @Override
    public void initialize(UniqueLogin uniqueLogin) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return true;
    }
}
