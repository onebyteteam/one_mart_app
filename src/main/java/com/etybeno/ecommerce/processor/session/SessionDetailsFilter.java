/*
 * Copyright 2014-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.etybeno.ecommerce.processor.session;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Inserts the session details into the session for every request. Some users may prefer
 * to insert session details only after authentication. This is fine, but it may be
 * valuable to the most up to date information so that if someone stole the user's session
 * id it can be observed.
 *
 * @author Rob Winch
 */
// tag::class[]
//@Component
//@Order(Ordered.LOWEST_PRECEDENCE)
public class SessionDetailsFilter
//        extends OncePerRequestFilter
{

    static final String UNKNOWN = "Unknown";


    // tag::dofilterinternal[]
//    @Override
//    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
//                                 FilterChain chain) throws IOException, ServletException {
//        chain.doFilter(request, response);
//        HttpSession session = request.getSession(false);
//        Cookie[] cookies = request.getCookies();
//        for(Cookie cookie : cookies) {
//            if(cookie.getName().equals("SESSION")) {
//                System.out.println(cookie.getValue());
//            }
//        }

//        if (session != null) {
//            Object session_details = session.getAttribute("SESSION_DETAILS");
//            if (null == session_details) {
//                System.out.println("yeah");
//                SessionDetails details = new SessionDetails();
//                details.setAccessType(request.getHeader("User-Agent"));
//                session.setAttribute("SESSION_DETAILS", details);
//            }
//        }
//    }
    // end::dofilterinternal[]

//    private String getRemoteAddress(HttpServletRequest request) {
//        String remoteAddr = request.getHeader("X-FORWARDED-FOR");
//        if (remoteAddr == null) {
//            remoteAddr = request.getRemoteAddr();
//        } else if (remoteAddr.contains(",")) {
//            remoteAddr = remoteAddr.split(",")[0];
//        }
//        return remoteAddr;
//    }
}
// end::class[]
