package com.etybeno.ecommerce.processor.importer;


import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.AbstractProduct;
import com.etybeno.ecommerce.data.entity.Permission;
import com.etybeno.ecommerce.data.enums.ProductType;
import com.etybeno.ecommerce.data.enums.ProductVisibility;
import com.etybeno.ecommerce.data.form.SellerForm;
import com.etybeno.ecommerce.data.imported.ImportedAttribute;
import com.etybeno.ecommerce.data.imported.ImportedCatalog;
import com.etybeno.ecommerce.data.imported.ImportedCateAtt;
import com.etybeno.ecommerce.data.imported.ImportedProduct;
import com.etybeno.ecommerce.data.model.Attribute;
import com.etybeno.ecommerce.data.model.Catalog;
import com.etybeno.ecommerce.data.model.Category;
import com.etybeno.ecommerce.repository.mongodb.I18NTranslationRepository;
import com.etybeno.ecommerce.service.*;
import com.etybeno.ecommerce.util.ConvenientUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 09/02/2018.
 */
@Service
public class DataImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataImporter.class);

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private AttributeService attributeService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private TermTaxonomyRelationshipService termTaxonomyRelationshipService;
    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;

    public void importPermissionsFromJson(String json) throws IOException {
        TypeReference<List<Permission>> permissionListType = new TypeReference<List<Permission>>() {
        };
        List<Permission> permissionList = StringUtil.OBJECT_MAPPER.readValue(json, permissionListType);
        for (Permission permission : permissionList) permissionService.createNewPermission(permission);
    }

    public void importCategoriesFromJson(String json, String locale) throws IOException {
        TypeReference<List<Category>> cateListType = new TypeReference<List<Category>>() {
        };
        List<Category> cateList = StringUtil.OBJECT_MAPPER.readValue(json, cateListType);
        for (Category category : cateList) insertCategory(category, locale);
    }

    private ObjectId insertCategory(Category category, String locale) {
        Category tmp = categoryService.createNewCategory(locale, category);
        if (null != category.getChildren() && !category.getChildren().isEmpty()) {
            List<ObjectId> childrenIds = new ArrayList<>();
            for (Category child : category.getChildren()) {
                child.setParentId(tmp.getCategoryId());
                ObjectId objectId = insertCategory(child, locale);
                childrenIds.add(objectId);
            }
            termTaxonomyRelationshipService.addRelationship(tmp.getCategoryObjectId(), childrenIds,
                    Constant.TTRelationship.CATEGORY_CHILDREN);
        }
        return tmp.getCategoryObjectId();
    }

    public void importCategoryAttributesFromJson(String json, String localeStr) throws IOException {
        TypeReference<List<ImportedCateAtt>> cateAttListType = new TypeReference<List<ImportedCateAtt>>() {
        };
        List<ImportedCateAtt> attributeList = StringUtil.OBJECT_MAPPER.readValue(json, cateAttListType);
        for (ImportedCateAtt cateAtt : attributeList) {
            Category tmp = categoryService.getCategoryBySlug(cateAtt.getCateSlug());
            if (null == tmp) {
                LOGGER.error(String.format("Category %s does not exist", cateAtt.getCateSlug()));
                continue;
            }
            Map<String, List<ObjectId>> attributeData = new HashMap<>();
            for (ImportedCateAtt.ImportedAttr entry : cateAtt.getAttributes()) {
                List<ObjectId> attributes = new ArrayList<>();
                String parentSlug = ConvenientUtil.parseToSlug(entry.getName());
                Attribute parentAttr = attributeService.getAttributeBySlug(entry.getGroup(), parentSlug, localeStr);
                ObjectId parentAttrId;
                if (null == parentAttr) {
                    parentAttrId = attributeService.addAttribute(entry.getGroup(), parentSlug, entry.getName(), "product attribute", localeStr);
                } else parentAttrId = parentAttr.getAttributeObjectId();
                for (String value : entry.getValues()) {
                    String slug = ConvenientUtil.parseToSlug(value);
                    Attribute attribute = attributeService.getAttributeBySlug(entry.getGroup(), slug, localeStr);
                    if (null == attribute) {
                        attribute = new Attribute(slug, value, entry.getGroup() + " attribute", localeStr, parentAttrId);
                        attributeService.addAttribute(entry.getGroup(), attribute);
                    }
                    attributes.add(attribute.getAttributeObjectId());
                }
                String termRel = attributeService.getTermRelByAttrType(entry.getGroup());
                termTaxonomyRelationshipService.addRelationship(parentAttrId, attributes, termRel);
                attributeData.put("pa_" + parentAttrId.toHexString(), attributes);
            }
            _importCategoryAttribute(tmp, attributeData);
            if (cateAtt.isApplyToChildren()) {
                List<Category> children = categoryService.getChildrenById(new ObjectId(tmp.getCategoryId()));
                children.forEach(category -> _importCategoryAttribute(category, attributeData));
            }
            if (cateAtt.isApplyToParent()) {
                List<Category> parents = categoryService.getParentCategoriesById(new ObjectId(tmp.getCategoryId()));
                parents.forEach(category -> _importCategoryAttribute(category, attributeData));
            }
        }
    }

    private void _importCategoryAttribute(Category category, Map<String, List<ObjectId>> attributeData) {
        for (Map.Entry<String, List<ObjectId>> entry : attributeData.entrySet()) {
            termTaxonomyRelationshipService.addRelationship(category.getCategoryObjectId(),
                    entry.getValue(), entry.getKey());
        }
    }

    public void importAttributesFromJson(String json, String locale) throws IOException {
//        TypeReference<List<ImportedAttribute>> attListType = new TypeReference<List<ImportedAttribute>>() {
//        };
//        List<ImportedAttribute> attributeList = StringUtil.OBJECT_MAPPER.readValue(json, attListType);
//        for (ImportedAttribute attribute : attributeList) {
//            String normalize = ConvenientUtil.parseToSlug(attribute.getName());
//            String language = StringUtil.isNullOrEmpty(attribute.getLanguage()) ? "" : "." + attribute.getLanguage();
//            String taxonomy = ConvenientUtil.buildTaxonomy(1, normalize + language);
//            for (String value : attribute.getValues()) {
//                String slug = ConvenientUtil.parseToSlug(value);
//                attributeService.addAttribute(taxonomy, slug, value, attribute.getName() + " attributes", locale);
//            }
//        }
    }


    public void importSupplierFromJson(String json) throws IOException {
        TypeReference<List<SellerForm>> sellerListType = new TypeReference<List<SellerForm>>() {
        };
        List<SellerForm> sellerList = StringUtil.OBJECT_MAPPER.readValue(json, sellerListType);
        for (SellerForm sellerForm : sellerList) supplierService.createSupplier(sellerForm);
    }

    public void importProductsFromJson(String json, String locale) throws IOException {
        TypeReference<List<ImportedProduct>> productListType = new TypeReference<List<ImportedProduct>>() {
        };
        List<ImportedProduct> productList = StringUtil.OBJECT_MAPPER.readValue(json, productListType);
        for (ImportedProduct importedProduct : productList) insertProduct(importedProduct, locale);
    }

    public void importCatalogsFromJson(String json, String locale) throws IOException {
        TypeReference<List<ImportedCatalog>> catalogListType = new TypeReference<List<ImportedCatalog>>() {
        };
        List<ImportedCatalog> catalogList = StringUtil.OBJECT_MAPPER.readValue(json, catalogListType);
        for (ImportedCatalog importedCatalog : catalogList) {

            List<ObjectId> productIds = importedCatalog.getProducts().stream()
                    .map(importedProduct -> {
                        if (importedProduct.getUseSku())
                            return productService.getProductIdBySku(importedProduct.getSku());
                        else try {
                            importedProduct.setVisibility(ProductVisibility.CATALOGUE);
                            return insertProduct(importedProduct, locale);
                        } catch (JsonProcessingException e) {
                            return null;
                        }
                    }).collect(Collectors.toList());
            Catalog catalog = new Catalog()
                    .setCode(importedCatalog.getCode())
                    .setDescription(importedCatalog.getDescription())
                    .setName(importedCatalog.getName())
                    .setSlug(importedCatalog.getSlug())
                    .setVisible(importedCatalog.getVisible())
                    .setImages(importedCatalog.getImages())
                    .setProductsAsObjectId(productIds);
            catalogService.createNewCatalog(catalog);
        }
    }

    private ObjectId insertProduct(ImportedProduct importedProduct, String locale) throws JsonProcessingException {
        ObjectId sellerId;
        if (StringUtil.isNullOrEmpty(importedProduct.getSupplier()))
            sellerId = supplierService.getDefaultSeller().getSellerObjectId();
        else sellerId = supplierService.getSellerBySlug(importedProduct.getSupplier()).getSellerObjectId();
        ObjectId i18nNameId = i18NTranslationRepository.addNew(ConvenientUtil.parseToSlug(importedProduct.getName()),
                importedProduct.getName(), Constant.I18NType.PRODUCT_NAME, locale);
        Set<ObjectId> categoryIds = importedProduct.getCategoriesSlug().stream()
                .map(s -> categoryService.getCategoryBySlug(s).getCategoryObjectId())
                .collect(Collectors.toSet());
        //
        AbstractProduct abstractProduct = new AbstractProduct();
        abstractProduct.setSku(importedProduct.getSku());
        abstractProduct.setSalePrice(importedProduct.getSalePrice());
        abstractProduct.setPrice(importedProduct.getSalePrice());
        abstractProduct.setRegularPrice(importedProduct.getPrice());
        abstractProduct.setProductVisibility(importedProduct.getVisibility());
        abstractProduct.setProductType(ProductType.NORMAL_PRODUCT);
        abstractProduct.setI18nNameId(i18nNameId);
        abstractProduct.setSupplierId(sellerId);
        abstractProduct.setCategoryIds(categoryIds);
        abstractProduct.setGalleryImageIds(importedProduct.getImages());
        if (!StringUtil.isNullOrEmpty(importedProduct.getDescription())) {
            ObjectId i18nDescId = i18NTranslationRepository.addNew(null, importedProduct.getDescription(),
                    Constant.I18NType.PRODUCT_DESCRIPTION, locale);
            abstractProduct.setI18nDescriptionId(i18nDescId);
        }
        if (!StringUtil.isNullOrEmpty(importedProduct.getShortDescription())) {
            ObjectId shortId = i18NTranslationRepository.addNew(null, importedProduct.getShortDescription(),
                    Constant.I18NType.PRODUCT_SHORT_DESCRIPTION, locale);
            abstractProduct.setI18nShortDescriptionId(shortId);
        }
        Set<ObjectId> searchAttrs = new HashSet<>();
        searchAttrs.addAll(categoryIds);
        abstractProduct.setSearchAttributeIds(searchAttrs);
        Map<String, List<String>> originators = importedProduct.getOriginators();
        if (originators != null && !originators.isEmpty()) {
            Map<String, Set<ObjectId>> originatorAttrs = loadAttributes(AttributeService.ORIGINATOR_TYPE, originators, locale);
            originatorAttrs.values().forEach(objectIds -> searchAttrs.addAll(objectIds));
            abstractProduct.setOriginators(originatorAttrs);
        }
//        Map<String, List<String>> optionStrTraits = importedProduct.getOptionTraits();
//        if(optionStrTraits != null && !optionStrTraits.isEmpty()) {
//            Map<ObjectId, Set<ObjectId>> optionTraits = loadAttributes(optionStrTraits, locale);
//            optionTraits.values().forEach(objectIds -> searchTraits.addAll(objectIds));
//            abstractProduct.setOptionAttributeIds(optionTraits);
//        }
        boolean b = productService.createNewProduct(abstractProduct);
        return abstractProduct.getId();
    }

    private Map<String, Set<ObjectId>> loadAttributes(int attrType, Map<String, List<String>> strAttrs, String locale) {
        Map<String, Set<ObjectId>> collectOption = new HashMap<>();
        strAttrs.forEach((key, values) -> {
            String slugKey = ConvenientUtil.parseToSlug(key);
            Attribute tmp = attributeService.getAttributeBySlug(attrType, slugKey, locale);
            if (null == tmp) {
                tmp = new Attribute(slugKey, key, "Product attribute", locale, null);
                attributeService.addAttribute(attrType, tmp);
            }
            ObjectId parentKeyId = tmp.getAttributeObjectId();
            Set<ObjectId> valueIds = values.stream()
                    .map(s -> {
                        String slugValue = ConvenientUtil.parseToSlug(s);
                        Attribute valueAttr = attributeService.getAttributeBySlug(attrType, slugValue, locale);
                        if (null == valueAttr) {
                            valueAttr = new Attribute(slugValue, s, key + " attribute", locale, parentKeyId);
                            attributeService.addAttribute(attrType, valueAttr);
                        }
                        return valueAttr.getAttributeObjectId();
                    })
                    .collect(Collectors.toSet());
            collectOption.put(parentKeyId.toHexString(), valueIds);
        });
        return collectOption;
    }
}
