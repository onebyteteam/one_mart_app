package com.etybeno.ecommerce.annotations;

import com.etybeno.ecommerce.processor.validation.UniqueLoginValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by thangpham on 08/02/2018.
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueLoginValidator.class)
public @interface UniqueLogin {
    String message() default "Not unique username";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
