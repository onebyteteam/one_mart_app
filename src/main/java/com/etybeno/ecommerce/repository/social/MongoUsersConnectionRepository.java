package com.etybeno.ecommerce.repository.social;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.service.social.SocialService;
import com.etybeno.ecommerce.util.ConvenientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by thangpham on 25/02/2018.
 */
public class MongoUsersConnectionRepository implements UsersConnectionRepository {

    @Autowired
    private SocialService socialService;
    @Autowired
    private ConnectionSignUp connectionSignUp;
    private ConnectionFactoryLocator connectionFactoryLocator;
    private TextEncryptor textEncryptor;

    public MongoUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator, ConnectionSignUp connectionSignUp,
                                          TextEncryptor textEncryptor) {
        this.connectionFactoryLocator = connectionFactoryLocator;
//        this.connectionSignUp = connectionSignUp;
        this.textEncryptor = textEncryptor;
    }

    @Override
    public List<String> findUserIdsWithConnection(Connection<?> connection) {
        System.out.println("findUserIdsWithConnection");
        ConnectionKey key = connection.getKey();
        LoginType loginType = ConvenientUtil.parseProvider(key.getProviderId());
        if (StringUtil.isNullOrEmpty(loginType))
            throw new IllegalArgumentException("Provider " + key.getProviderId() + " has not supported yet");
        List<String> localUserIds = socialService.getUsername(loginType, Arrays.asList(key.getProviderUserId()));
        if (localUserIds.size() == 0 && connectionSignUp != null) {
            String newUserId = connectionSignUp.execute(connection);
            if (newUserId != null) {
                createConnectionRepository(newUserId).addConnection(connection);
                return Arrays.asList(newUserId);
            }
        }
        return localUserIds;
    }

    @Override
    public Set<String> findUserIdsConnectedTo(String providerId, Set<String> providerUserIds) {
        System.out.println("findUserIdsConnectedTo");
        LoginType loginType = ConvenientUtil.parseProvider(providerId);
        final Set<String> rs = new HashSet<>();
        if (null == loginType) return new HashSet<>();
        else {
            rs.addAll(socialService.getUsername(loginType, providerUserIds));
            return rs;
        }
    }

    @Override
    public ConnectionRepository createConnectionRepository(String userId) {
        if (StringUtil.isNullOrEmpty(userId))
            throw new IllegalArgumentException("UserId cannot be null or empty");
        System.out.println("createConnectionRepository");
        return new MongoConnectionRepository(userId, connectionFactoryLocator, socialService);
    }

}
