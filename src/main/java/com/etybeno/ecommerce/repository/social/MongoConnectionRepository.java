package com.etybeno.ecommerce.repository.social;

import com.etybeno.ecommerce.service.social.SocialService;
import com.etybeno.ecommerce.util.ConvenientUtil;
import org.springframework.social.connect.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 25/02/2018.
 */
public class MongoConnectionRepository implements ConnectionRepository {

    private SocialService socialService;

    private ConnectionFactoryLocator connectionFactoryLocator;
    private String username;

    public MongoConnectionRepository(String username, ConnectionFactoryLocator connectionFactoryLocator,
                                     SocialService socialService) {
        this.username = username;
        this.socialService = socialService;
        this.connectionFactoryLocator = connectionFactoryLocator;
    }

    @Override
    public MultiValueMap<String, Connection<?>> findAllConnections() {
        System.out.println("findAllConnections");
        List<ConnectionData> allConnection = socialService.findAllConnection(username);
        List<Connection<?>> resultList = allConnection.stream()
                .map(o -> connectionFactoryLocator.getConnectionFactory(o.getProviderId()).createConnection(o))
                .collect(Collectors.toList());
        MultiValueMap<String, Connection<?>> connections = new LinkedMultiValueMap<>();
        Set<String> registeredProviderIds = connectionFactoryLocator.registeredProviderIds();
        for (String registeredProviderId : registeredProviderIds) {
            connections.put(registeredProviderId, Collections.<Connection<?>>emptyList());
        }
        for (Connection<?> connection : resultList) {
            String providerId = connection.getKey().getProviderId();
            if (connections.get(providerId).size() == 0) {
                connections.put(providerId, new LinkedList<>());
            }
            connections.add(providerId, connection);
        }
        return connections;
    }

    @Override
    public List<Connection<?>> findConnections(String providerId) {
        System.out.println("findConnections");
        List<ConnectionData> allConnection = socialService.findAllConnection(username, ConvenientUtil.parseProvider(providerId));
        return allConnection.stream()
                .map(o -> connectionFactoryLocator.getConnectionFactory(o.getProviderId()).createConnection(o))
                .collect(Collectors.toList());
    }

    @Override
    public <A> List<Connection<A>> findConnections(Class<A> apiType) {
        System.out.println("findConnections");
        List<?> connections = findConnections(getProviderId(apiType));
        return (List<Connection<A>>) connections;
    }

    @Override
    public MultiValueMap<String, Connection<?>> findConnectionsToUsers(MultiValueMap<String, String> providerUserIds) {
        System.out.println("findConnectionsToUsers");
//        if (providerUserIds == null || providerUserIds.isEmpty()) {
//            throw new IllegalArgumentException("Unable to execute find: no providerUsers provided");
//        }
//        StringBuilder providerUsersCriteriaSql = new StringBuilder();
//        MapSqlParameterSource parameters = new MapSqlParameterSource();
//        parameters.addValue("userId", userId);
//        for (Iterator<Map.Entry<String, List<String>>> it = providerUserIds.entrySet().iterator(); it.hasNext();) {
//            Map.Entry<String, List<String>> entry = it.next();
//            String providerId = entry.getKey();
//            providerUsersCriteriaSql.append("providerId = :providerId_").append(providerId)
//                    .append(" and providerUserId in (:providerUserIds_").append(providerId).append(")");
//            parameters.addValue("providerId_" + providerId, providerId);
//            parameters.addValue("providerUserIds_" + providerId, entry.getValue());
//            if (it.hasNext()) {
//                providerUsersCriteriaSql.append(" or " );
//            }
//        }
//
//        List<Connection<?>> resultList = new NamedParameterJdbcTemplate(jdbcTemplate).query(selectFromUserConnection() +
//                " where userId = :userId and " + providerUsersCriteriaSql + " order by providerId, rank", parameters, connectionMapper);
//        MultiValueMap<String, Connection<?>> connectionsForUsers = new LinkedMultiValueMap<String, Connection<?>>();
//        for (Connection<?> connection : resultList) {
//            String providerId = connection.getKey().getProviderId();
//            List<String> userIds = providerUsers.get(providerId);
//            List<Connection<?>> connections = connectionsForUsers.get(providerId);
//            if (connections == null) {
//                connections = new ArrayList<Connection<?>>(userIds.size());
//                for (int i = 0; i < userIds.size(); i++) {
//                    connections.add(null);
//                }
//                connectionsForUsers.put(providerId, connections);
//            }
//            String providerUserId = connection.getKey().getProviderUserId();
//            int connectionIndex = userIds.indexOf(providerUserId);
//            connections.set(connectionIndex, connection);
//        }
//        return connectionsForUsers;
        return null;
    }

    @Override
    public Connection<?> getConnection(ConnectionKey key) {
        System.out.println("getConnection");
        ConnectionData single = socialService.findSingleConnection(username,
                ConvenientUtil.parseProvider(key.getProviderId()), key.getProviderUserId());
        return connectionFactoryLocator.getConnectionFactory(single.getProviderId()).createConnection(single);
    }

    @Override
    public <A> Connection<A> getConnection(Class<A> apiType, String providerUserId) {
        System.out.println("getConnection");
        String providerId = getProviderId(apiType);
        return (Connection<A>) getConnection(new ConnectionKey(providerId, providerUserId));
    }

    @Override
    public <A> Connection<A> getPrimaryConnection(Class<A> apiType) {
        System.out.println("getPrimaryConnection");
        String providerId = getProviderId(apiType);
        Connection<A> connection = (Connection<A>) findPrimaryConnection(providerId);
        if (connection == null) {
            throw new NotConnectedException(providerId);
        }
        return connection;
    }

    @Override
    public <A> Connection<A> findPrimaryConnection(Class<A> apiType) {
        System.out.println("findPrimaryConnection");
        String providerId = getProviderId(apiType);
        return (Connection<A>) findPrimaryConnection(providerId);
    }

    @Override
    public void addConnection(Connection<?> connection) {
        System.out.println("addConnection");
        try {
            socialService.insertConnectionData(username, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateConnection(Connection<?> connection) {
        System.out.println("updateConnection");
        socialService.updateConnectionData(username, connection.createData());
    }

    @Override
    public void removeConnections(String providerId) {
        System.out.println("removeConnections");
        socialService.deleteConnection(username, ConvenientUtil.parseProvider(providerId));
    }

    @Override
    public void removeConnection(ConnectionKey key) {
        System.out.println("removeConnection");
        socialService.deleteConnection(username, ConvenientUtil.parseProvider(key.getProviderId()), key.getProviderUserId());
    }

    private <A> String getProviderId(Class<A> apiType) {
        System.out.println("getProviderId");
        return connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();
    }

    private Connection<?> findPrimaryConnection(String providerId) {
        System.out.println("findPrimaryConnection");
        List<Connection<?>> connections = findConnections(providerId);
        if (connections.size() > 0) return connections.get(0);
        else return null;
    }
}
