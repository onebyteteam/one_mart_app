package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.User;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 20/01/2018.
 */
@Repository
public class UserRepository extends BaseDataAccess<User> {

    public static final int SIMPLE_CUSTOMER_PROJECTION = 1;
    public static final int SOCIAL_DATA_PROJECTION = 2;
    public static final int SECURITY_USER_PROJECTION = 3;

    public UserRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<User> collection = getCollection();
        collection.createIndex(new Document("username", 1), new IndexOptions().unique(true));
    }

    public boolean checkTargetExist(String key, String value) {
        long count = getCollection().count(Filters.eq(key, value));
        return count == 1;
    }

    public ObjectId getIdByUsername(String username) {
        Document document = getNonTypeCollection().find(Filters.eq("username", username)).first();
        return null == document ? null : document.getObjectId("_id");
    }

    public User createUser(User user) throws JsonProcessingException {
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(user))
                .append("created_date", user.getCreatedDate())
                .append("modified_date", user.getModifiedDate());
        getNonTypeCollection().insertOne(document);
        user.setId(document.getObjectId("_id"));
        return user;
    }

    public boolean updateUser(ObjectId userId, Map<String, Object> values) {
        UpdateResult updateResult = getCollection().updateOne(
                Filters.and(Filters.eq("_id", userId)),
                Updates.combine(values.keySet().stream()
                        .map(k -> Updates.set(k, values.get(k)))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }

    public boolean deleteUser(ObjectId objectId) {
        DeleteResult deleteResult = getCollection().deleteOne(Filters.eq("_id", objectId));
        return deleteResult.getDeletedCount() == 1;
    }

    public <T> List<T> getAllLoginData(String username, int projectionType, Class<T> outputClass) {
        AggregateIterable<T> aggregate = getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("username", username)),
                Aggregates.lookup("login_data", "_id", "user_id", "login"),
                Aggregates.unwind("$login"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects", Arrays.asList("$login", "$$ROOT")))),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass);
        List<T> rs = new ArrayList();
        for (T t : aggregate) rs.add(t);
        return rs;
    }

    public <T> List<T> getAllLoginData(String username, LoginType loginType, int projectionType, Class<T> outputClass) {
        AggregateIterable<T> aggregate = getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("username", username)),
                Aggregates.lookup("login_data", "_id", "user_id", "login"),
                Aggregates.unwind("$login"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects", Arrays.asList("$login", "$$ROOT")))),
                Aggregates.match(Filters.eq("login_type", loginType.name())),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass);
        List<T> rs = new ArrayList();
        for (T t : aggregate) rs.add(t);
        return rs;
    }

    private <T> T getUserInformationByUniqueKey(String key, Object value, LoginType loginType, int projectionType, Class<T> clazz) {
        return getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq(key, value)),
                Aggregates.lookup("login_data", "_id", "user_id", "logins"),
                Aggregates.lookup("account", "_id", "user_id", "accounts"),
                Aggregates.addFields(Arrays.asList(
                        new Field<>("authorizations", new Document("$map",
                                new Document("input", new Document("$filter",
                                        new Document("input", "$accounts")
                                                .append("as", "acc")
                                                .append("cond", new Document("$eq", Arrays.asList("$$acc.active", Boolean.TRUE)))))
                                        .append("as", "acc")
                                        .append("in", "$$acc")
                        )),
                        new Field<>("authentications", new Document("$map",
                                new Document("input", null == loginType ? "$logins" :
                                        new Document("$filter", new Document("input", "$logins")
                                                .append("as", "login")
                                                .append("cond", new Document("$eq", Arrays.asList("$$login.login_type", loginType.getValue())))))
                                        .append("as", "t")
                                        .append("in", "$$t")
                        ))
                )),
//                new Document("$replaceRoot", new Document("newRoot",
//                        new Document("$mergeObjects", Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$tmp", 0)), "$$ROOT")))),
                Aggregates.project(buildProjection(projectionType))
        ), clazz).first();
    }

    public <T> T getUserInformationByUsername(String username, LoginType loginType, int projectionType, Class<T> outputClass) {
        return getUserInformationByUniqueKey("username", username, loginType, projectionType, outputClass);
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case SIMPLE_CUSTOMER_PROJECTION:
                return buildCustomerProjection();
            case SOCIAL_DATA_PROJECTION:
                return buildSocialDataProjection();
            case SECURITY_USER_PROJECTION:
                return buildSecurityUserProjection();
            default:
                return null;
        }
    }

    private Bson buildSecurityUserProjection() {
        return Projections.include("username", "validation", "authorizations");
    }

    private Bson buildCustomerProjection() {
        return Projections.include("username", "display_name", "email", "first_name", "last_name", "avatar_uri"
                , "authorizations", "authentications");
    }

    private Bson buildSocialDataProjection() {
        return Projections.fields(Projections.include("username", "identification", "first_name", "last_name",
                "profile_url", "avatar_uri", "login.login_type", "login.validation", "login.refresh_token", "login.expire_time"));
    }

}
