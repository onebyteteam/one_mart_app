package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.OrderProduct;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 27/03/2018.
 */
@Repository
public class OrderProductRepository extends BaseDataAccess<OrderProduct> {

    public OrderProductRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        getCollection().createIndex(new Document()
                .append("order_id", 1).append("product_id", 1), new IndexOptions().unique(true));
    }

    public List<ObjectId> createNewOrderProducts(List<OrderProduct> orderProducts) throws JsonProcessingException {
        List<ObjectId> returnIds = orderProducts.stream()
                .map(o -> {
                    try {
                        Document parse = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(o));
                        parse.put("product_id", o.getProductId());
                        parse.put("quantity", o.getQuantity().intValue());
                        System.out.println(parse);
                        getNonTypeCollection().insertOne(parse);
                        return parse.getObjectId("_id");
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                })
                .filter(document -> document != null)
                .collect(Collectors.toList());
        return returnIds;
    }
}
