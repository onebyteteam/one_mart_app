package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.data.entity.AbstractCollection;
import com.etybeno.ecommerce.data.enums.CollectionVisibility;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.etybeno.mongodb.model.TargetValues;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 16/02/2018.
 */
@Repository
public class AbstractCollectionRepository extends BaseDataAccess<AbstractCollection> {

    public static final int CATALOG_DETAIL_PROJECTION = 1;
    public static final int CATALOG_LIST_PROJECTION = 2;

    public AbstractCollectionRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<AbstractCollection> collection = getCollection();
        collection.createIndex(new Document("slug", 1), new IndexOptions().unique(true));
        collection.createIndex(new Document("name", 1));
    }

    public AbstractCollection createNewAbtractCollection(AbstractCollection collection) {
        Document document = new Document()
                .append("name", collection.getName())
                .append("slug", collection.getSlug())
                .append("code", collection.getCode())
                .append("created_date", new Date())
                .append("modified_date", new Date())
                .append("product_ids", collection.getProductIds())
                .append("collection_visibility", collection.getCollectionVisibility().name())
                .append("description", collection.getDescription())
                .append("gallery_image_ids", collection.getGalleryImageIds());
        getNonTypeCollection().insertOne(document);
        collection.setId(document.getObjectId("_id"));
        return collection;
    }

    public <T> T getCollectionByCode(String code, int projectType, Class<T> outputClass) {
        List<KeyValue> targets = Arrays.asList(
                new TargetValue("code", code),
                new TargetValues("collection_visibility", Arrays.asList(CollectionVisibility.VISIBLE.name(),
                        CollectionVisibility.HIDDEN.name())));
        return getByTarget(targets, projectType, outputClass);
    }

    private <T> T getByTarget(List<KeyValue> targets, int projectionType, Class<T> outputClass) {
        return getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(buildFilters(targets))),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass).first();
    }

    public <T> List<T> getAllCollection(List<KeyValue> targets, int from, int size, int projectionType, Class<T> outputClass) {
        AggregateIterable<T> aggregate = getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(buildFilters(targets))),
                Aggregates.skip(from), Aggregates.limit(size),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass);
        List<T> result = new ArrayList();
        for (T t : aggregate) result.add(t);
        return result;
    }

    public long countAll(List<TargetValues> targets) {
        if (null == targets || targets.isEmpty()) return getCollection().count();
        else return getCollection().count(Filters.and(targets.stream()
                .map(o -> Filters.in(o.getTarget(), o.getValues()))
                .collect(Collectors.toList())));
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case CATALOG_DETAIL_PROJECTION:
                return buildCollectionDetailProjection();
            case CATALOG_LIST_PROJECTION:
                return buildCollectionListProjection();
            default:
                return null;
        }
    }

    private Bson buildCollectionDetailProjection() {
        return Projections.include("code", "slug", "name", "description", "product_ids");
    }

    private Bson buildCollectionListProjection() {
        return Projections.include("code", "slug", "name", "description", "gallery_image_ids");
    }
}
