package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.Order;
import com.etybeno.ecommerce.data.enums.OrderStatus;
import com.etybeno.ecommerce.data.model.SaleDataReport;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by thangpham on 27/03/2018.
 */
@Repository
public class OrderRepository extends BaseDataAccess<Order> {

    public static final int ORDER_REPORT_PROJECTION = 1, ORDER_ID_PROJECTION = 2;

    @Autowired
    @Lazy
    private ConfigurationRepository configurationRepository;

    public OrderRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        getCollection().createIndex(new Document("created_date", 1).append("modified_date", 1));
        getCollection().createIndex(new Document("cart_id", 1), new IndexOptions().unique(true));
    }

    /**
     * Get order report for render chart
     * @param dateFormat
     * @param from
     * @param to
     * @param aggs
     * @return
     */
    public Map<String, Map<String, Object>> getOrderReport(String dateFormat, Date from, Date to, List<String> aggs) {
        final Document parseDateKey = new Document("$dateToString", new Document("format", dateFormat).append("date", "$modified_date"));
        List<Facet> facets = aggs.stream().map(s -> {
            switch (s) {
                case "revenue":
                    return new Facet(s, Aggregates.match(Filters.eq("order_status", OrderStatus.COMPLETED.name())),
                            Aggregates.group(parseDateKey, Accumulators.sum("revenue", "$total_amount")));
                case "order":
                    return new Facet(s, Aggregates.match(Filters.not(Filters.eq("order_status", OrderStatus.AWAITING_FULFILMENT.name()))),
                            Aggregates.group(parseDateKey, Accumulators.sum("order", 1)));
                case "sale":
                    return new Facet(s, Aggregates.match(Filters.eq("order_status", OrderStatus.COMPLETED.name())),
                            Aggregates.group(parseDateKey, Accumulators.sum("sale", 1)));
                default:
                    return null;
            }
        }).filter(facet -> null != facet).collect(Collectors.toList());
        Document aggregate = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(
                        Filters.gte("created_date", from),
                        Filters.lte("created_date", to)
                )),
                Aggregates.facet(facets)
        )).first();
        Map<String, Map<String, Object>> reports = new TreeMap<>();
        for(String agg: aggs) {
            List<Document> list = aggregate.get(agg, List.class);
            Map<String, Object> series = reports.get(agg);
            if(null == series) {
                series = new TreeMap<>();
                reports.put(agg, series);
            }
            for(Document d : list) series.put(d.getString("_id"), d.get(agg));
        }
        return reports;
    }

    public Order createNewOrder(Order order) throws JsonProcessingException {
        String orderId = buildOrderId();
        order.setOrderId(orderId);
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(order))
                .append("cart_id", order.getCartId())
                .append("user_id", order.getUserId())
                .append("created_date", order.getCreatedDate())
                .append("modified_date", order.getModifiedDate());
        getNonTypeCollection().insertOne(document);
        return order;
    }

    public boolean updateOrder(String orderId, TargetValue... values) {
        UpdateResult updateResult = getCollection().updateOne(Filters.eq("_id", orderId),
                Updates.combine(Stream.of(values)
                        .map(o -> Updates.set(o.getTarget(), o.getValue()))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }

    public Order loadOrderbyId(String orderId) {
        return getCollection().find(Filters.eq("_id", orderId)).first();
    }

    private String buildOrderId() {
        long hour = System.currentTimeMillis() / 3600000;
        String orderId = RandomStringUtils.randomAlphanumeric(4) + "-" + hour + "-" +
                StringUtils.leftPad(configurationRepository.getNextSequenceOrderId().toString(), 7, '0');
        return orderId;
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case ORDER_REPORT_PROJECTION:
                return buildOrderReportProjection();
            case ORDER_ID_PROJECTION:
                return buildOrderIdProjection();
            default:
                return null;
        }
    }

    private Bson buildOrderIdProjection() {
        return Projections.include("_id");
    }

    private Bson buildOrderReportProjection() {
        return Projections.include("_id", "product_num", "total_amount", "created_date", "modified_date", "order_status");
    }
}
