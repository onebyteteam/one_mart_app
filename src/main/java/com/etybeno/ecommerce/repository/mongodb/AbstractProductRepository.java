package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.HashUtil;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.AbstractProduct;
import com.etybeno.ecommerce.data.entity.I18NTranslation;
import com.etybeno.ecommerce.data.enums.ProductVisibility;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static com.etybeno.ecommerce.Constant.I18NType;

/**
 * Created by thangpham on 06/01/2018.
 */
public class AbstractProductRepository extends BaseDataAccess<AbstractProduct> {

    public static final int PRODUCT_LIST_PROJECTION = 1, PRODUCT_DETAIL_PROJECTION = 2;

    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;
    @Autowired
    private TermRepository termRepository;
    @Resource
    private AbstractProductRepository self;

    public AbstractProductRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<AbstractProduct> collection = getCollection();
        collection.createIndex(new Document("sku", 1), new IndexOptions().unique(true));
    }

    public ObjectId getProductIdBySku(String sku) {
        Document first = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("sku", sku)),
                Aggregates.project(Projections.include("_id"))
        )).first();
        return null == first ? null : first.getObjectId("_id");
    }

    public boolean createProduct(AbstractProduct product) throws JsonProcessingException {
        Date current = new Date();
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(product));
        document.put("i18n_name_id", product.getI18nNameId());
        document.put("i18n_desc_id", product.getI18nDescriptionId());
        document.put("i18n_short_desc_id", product.getI18nShortDescriptionId());
        document.put("category_ids", product.getCategoryIds());
        document.put("search_attr_ids", product.getSearchAttributeIds());
        if (null != product.getOriginators() && !product.getOriginators().isEmpty()) {
            document.put("originators", product.getOriginators());
        }
        if (null != product.getOptionAttributeIds() && !product.getOptionAttributeIds().isEmpty()) {
            document.put("option_attr_ids", product.getOptionAttributeIds());
        }
        document.put("supplier_id", product.getSupplierId());
        document.put("created_date", current);
        document.put("modified_date", current);
        getNonTypeCollection().insertOne(document);
        product.setId(document.getObjectId("_id"));
        return true;
    }

    public boolean updateProduct(ObjectId id, List<TargetValue> values) {
        UpdateResult updateResult = getCollection().updateOne(Filters.eq("_id", id),
                Updates.combine(values.stream()
                        .map(o -> Updates.set(o.getTarget(), o.getValue()))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }

    public boolean updateProductForOrder(ObjectId id, int numPurchase) {
        AbstractProduct first = getCollection().find(Filters.eq("_id", id)).first();
        UpdateResult updateResult;
        if (first.getManageStock() != null && first.getManageStock()) {
            updateResult = getCollection().updateOne(
                    Filters.and(Filters.eq("_id", id), Filters.gte("stock_quantity", numPurchase)),
                    Updates.combine(Updates.inc("stock_quantity", -numPurchase),
                            Updates.inc("total_sales", numPurchase)));

        } else {
            updateResult = getCollection().updateOne(
                    Filters.and(Filters.eq("_id", id)),
                    Updates.inc("total_sales", numPurchase));
        }
        return updateResult.getModifiedCount() == 1;
    }


    public List<Document> getAllSearch(String keyword, List<KeyValue> targets, String locale, int from, int size, Map<String, Object> sorts) {
        List<I18NTranslation> productsTran = i18NTranslationRepository.searchText(keyword, locale, Arrays.asList(I18NType.PRODUCT_NAME), from, size);
        List<I18NTranslation> searchAttrTran = i18NTranslationRepository.searchText(keyword, locale,
                Arrays.asList(I18NType.ORIGINATOR_NAME, I18NType.ATTRIBUTE_NAME), from, size);
        List<Document> searchAttr = termRepository.getByTermI18nIdsAndTaxonomies(
                searchAttrTran.stream().map(t -> t.getObjectId()).collect(Collectors.toList()),
                Constant.Taxonomy.PRODUCT_ATTRIBUTE, Constant.Taxonomy.PRODUCT_ORIGINATOR);
        List<Bson> searchFilter = new ArrayList<>();
        searchFilter.add(Filters.eq("sku", keyword));
        if (null != productsTran && !productsTran.isEmpty())
            searchFilter.add(Filters.in("i18n_name_id", productsTran.stream().map(t -> t.getObjectId()).collect(Collectors.toSet())));
        if (null != searchAttrTran && !searchAttrTran.isEmpty())
            searchFilter.add(Filters.in("search_attr_ids", searchAttr.stream().map(t -> t.getObjectId("_id")).collect(Collectors.toSet())));
        //
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.or(searchFilter));
        if (targets != null && !targets.isEmpty()) filters.addAll(buildFilters(targets));
        //
        List<Bson> aggregates = new ArrayList();
        aggregates.add(Aggregates.match(Filters.and(filters)));
        if (sorts != null && !sorts.isEmpty())
            aggregates.add(Aggregates.sort(new Document(sorts)));
        aggregates.add(Aggregates.skip(from));
        aggregates.add(Aggregates.limit(size));
        aggregates.add(Aggregates.project(Projections.include("_id")));
        AggregateIterable<Document> documents = getNonTypeCollection().aggregate(aggregates);
        List<Document> result = new ArrayList();
        for (Document t : documents) result.add(t);
        return result;
    }

    @Cacheable(value = "product", key = "{#methodName, #hashQuery, #from, #size}", unless = "#result == null || #result.empty")
    public List<ObjectId> queryAllProducts(BigInteger hashQuery, List<Bson> filters, Bson sorters, int from, int size) {
        AggregateIterable<Document> aggregate = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(filters)),
                Aggregates.skip(from),
                Aggregates.limit(size),
                Aggregates.sort(sorters),
                Aggregates.project(Projections.include("_id"))
        ));
        List<ObjectId> rs = new ArrayList();
        for (Document d : aggregate) rs.add(d.getObjectId("_id"));
        return rs;
    }

    public List<ObjectId> queryProductsWithAttrs(List<KeyValue> targets, List<Set<ObjectId>> attrIds, int from, int size, List<TargetValue> sorts) throws JsonProcessingException, NoSuchAlgorithmException {
        List<Bson> filters = new ArrayList();
        if (targets != null && !targets.isEmpty()) filters.addAll(buildFilters(targets));
        if (attrIds != null && !attrIds.isEmpty()) {
            List<Bson> groupFilter = attrIds.stream().map(idSet -> Filters.in("search_attr_ids", idSet)).collect(Collectors.toList());
            filters.add(Filters.and(groupFilter));
        }
        Document sort = new Document();
        if (null != sorts) sorts.forEach(targetValue -> sort.put(targetValue.getTarget(), targetValue.getValue()));
        String filterStr = filters.stream().map(f -> f.toString()).collect(Collectors.joining("|"));
        BigInteger hash = HashUtil.createHash(filterStr + "@" + sort.toString(), HashUtil.SHA_256).asBigInteger();
        return self.queryAllProducts(hash, filters, sort, from, size);
    }

    public long countByAttributes(List<Set<ObjectId>> attrIds, List<ProductVisibility> visibilities) throws NoSuchAlgorithmException {
        if (null == attrIds) throw new NullPointerException("Attribute should not be null");
        List<Bson> filters = new ArrayList();
        filters.add(Filters.in("product_visibility", visibilities.stream().map(v -> v.name()).collect(Collectors.toList())));
        if (!attrIds.isEmpty()) {
            List<Bson> groupFilter = attrIds.stream().map(idSet -> Filters.in("search_attr_ids", idSet)).collect(Collectors.toList());
            filters.add(Filters.and(groupFilter));
        }
        Bson and = Filters.and(filters);
        long l = HashUtil.createHash(StringUtil.GSON.toJson(and), HashUtil.MD5).asBigInteger().longValue();
        return self.countByFilter(l, and);
    }

    public long countAllSearch(String keyword, List<KeyValue> targets, String locale) throws NoSuchAlgorithmException {
        return i18NTranslationRepository.countSearch(keyword, locale, Arrays.asList(I18NType.PRODUCT_NAME));
    }

    public long countAll(List<KeyValue> targets, List<Set<ObjectId>> attrIds) throws NoSuchAlgorithmException {
        List<Bson> filters = new ArrayList<>();
        if (null != targets && !targets.isEmpty()) filters.addAll(buildFilters(targets));
        if (null != attrIds && !attrIds.isEmpty()) {
            List<Bson> groupFilter = attrIds.stream().map(idSet -> Filters.in("search_attr_ids", idSet)).collect(Collectors.toList());
            filters.add(Filters.and(groupFilter));
        }
        //
        if (filters.isEmpty()) return self.countByFilter(0, null);
        else {
            Bson and = Filters.and(filters);
            long l = HashUtil.createHash(StringUtil.GSON.toJson(and), HashUtil.MD5).asBigInteger().longValue();
            return self.countByFilter(l, and);
        }
    }

    @Cacheable(value = "count_product", key = "{#root.methodName, #hashId}")
    protected long countByFilter(long hashId, Bson filter) {
        if (null == filter) return getCollection().count();
        else return getCollection().count(filter);
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case PRODUCT_LIST_PROJECTION:
                return buildProductListProjection();
            case PRODUCT_DETAIL_PROJECTION:
                return buildProductDetailProjection();
            default:
                return null;
        }
    }

    private Bson buildProductListProjection() {
        return Projections.include("sku", "name", "price", "slug", "sale_price",
                "description", "gallery_image_ids");
    }

    private Bson buildProductDetailProjection() {
        return Projections.include("sku", "i18n_name_id", "price", "sale_price",
                "i18n_short_desc_id", "i18n_desc_id", "gallery_image_ids", "category_ids", "originators", "option_attr_ids");
    }
}