package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.data.entity.Configuration;
import com.etybeno.mongodb.base.BaseDataAccess;
import org.springframework.stereotype.Repository;

/**
 * Created by thangpham on 10/04/2018.
 */
@Repository
public class ConfigurationRepository extends BaseDataAccess<Configuration> {


    public ConfigurationRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
    }

    public Long getNextSequenceOrderId() {
        return this.getNextLongSequence("order_seq_id", "value");
    }
}
