package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.data.entity.Account;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by thangpham on 31/01/2018.
 */
public class AccountRepository extends BaseDataAccess<Account> {

    public final static int ACCOUNT_REPORT_PROJECTION = 1;

    public AccountRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<Account> collection = getCollection();
        collection.createIndex(new Document("user_id", 1).append("account_type", 1),
                new IndexOptions().unique(true));
    }

    public <T> List<T> getAccountReportData(int projectionType, Class<T> outputClass) {
        AggregateIterable<T> aggregate = getCollection().aggregate(Arrays.asList(
                Aggregates.lookup("user", "user_id", "_id", "user"),
                Aggregates.unwind("$user"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects", Arrays.asList("$user", "$$ROOT")))),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass);
        List<T> rs = new ArrayList<>();
        for(T t : aggregate) rs.add(t);
        return rs;
    }

    public Account createNewAccount(Account account) {
        Document document = new Document()
                .append("user_id", account.getUserId())
                .append("account_type", account.getAccountType().name())
                .append("active", account.getActive())
                .append("authorizations", account.getAuthorizations())
                .append("created_date", account.getCreatedDate())
                .append("modified_date", account.getModifiedDate());
        getNonTypeCollection().insertOne(document);
        account.setId(document.getObjectId("_id"));
        return account;
    }

    public boolean deleteAccount(ObjectId objectId) {
        DeleteResult deleteResult = getCollection().deleteOne(Filters.eq("_id", objectId));
        return deleteResult.getDeletedCount() == 1;
    }

    public boolean updateAccount(Account account) {
        return false;
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case ACCOUNT_REPORT_PROJECTION: return buildAccountReportProjection();
            default: return null;
        }
    }

    private Bson buildAccountReportProjection() {
        return Projections.fields(Projections.include("username", "display_name", "avatar_uri", "account_type", "active", "authorizations"));
    }
}
