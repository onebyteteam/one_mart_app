package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.data.entity.I18NTranslation;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.TargetValue;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thangpham on 10/06/2018.
 */
public class I18NTranslationRepository extends BaseDataAccess<I18NTranslation> {

    public I18NTranslationRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<I18NTranslation> collection = getCollection();
        collection.createIndex(new Document("object_id", 1).append("locale", 1).append("type", 1), new IndexOptions().unique(true));
        collection.createIndex(new Document("text", "text"));
    }

    public ObjectId addNew(String slug, String text, String type, String locale) {
        ObjectId objectId = ObjectId.get();
        I18NTranslation model = new I18NTranslation();
        model.setId(this.getNextLongSequence("trans_id", "seq_trans_id"));
        model.setLocale(locale);
        model.setObjectId(objectId);
        model.setSlug(slug);
        model.setText(text);
        model.setType(type);
        getCollection().insertOne(model);
        return objectId;
    }

    public boolean update(Long id, String slug, String text) {
        UpdateResult updateResult = getCollection().updateOne(Filters.eq("_id", id),
                Updates.combine(Arrays.asList(Updates.set("slug", slug), Updates.set("text", text))));
        return updateResult.getModifiedCount() == 1;
    }

    public I18NTranslation getModel(ObjectId objectId, String locale) {
        return getOne(Arrays.asList(
                new TargetValue("object_id", objectId),
                new TargetValue("locale", locale)));
    }

    public List<I18NTranslation> getModels(ObjectId objectId) {
        return getAll(Arrays.asList(new TargetValue("object_id", objectId)));
    }

    public List<I18NTranslation> getModels(String slug, String type) {
        return getAll(Arrays.asList(
                new TargetValue("slug", slug),
                new TargetValue("type", type)
        ));
    }

    public I18NTranslation getModel(String slug, String type, String locale) {
        return getOne(Arrays.asList(
                new TargetValue("slug", slug),
                new TargetValue("type", type),
                new TargetValue("locale", locale)
        ));
    }

    public List<I18NTranslation> searchText(String keyword, String locale, List<String> types, int from, int size) {
        AggregateIterable<I18NTranslation> documents = getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(
                        Filters.eq("locale", locale),
                        Filters.in("type", types),
                        Filters.text(keyword, new TextSearchOptions().diacriticSensitive(true))
                )),
                Aggregates.skip(0), Aggregates.limit(size)
        ));
        List<I18NTranslation> rs = new ArrayList<>();
        for (I18NTranslation doc : documents) rs.add(doc);
        return rs;
    }

    public long countSearch(String keyword, String locale, List<String> types) {
        return getCollection().count(Filters.and(
                Filters.eq("locale", locale),
                Filters.in("type", types),
                Filters.text(keyword, new TextSearchOptions().diacriticSensitive(true))));
    }
}
