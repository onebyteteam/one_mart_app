package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.Cart;
import com.etybeno.ecommerce.data.enums.CartStatus;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by thangpham on 07/03/2018.
 */
@Repository
public class CartRepository extends BaseDataAccess<Cart> {

    public final static int CART_SESSION_PROJECTION = 1;

    public CartRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        getCollection().createIndex(new Document().append("session_id", 1));
        getCollection().createIndex(new Document().append("user_id", 1));
    }

    public Cart createNewCart(Cart cart) throws JsonProcessingException {
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(cart))
                .append("user_id", cart.getUserId())
                .append("cart_status", cart.getCartStatus().name())
                .append("expire_on", cart.getExpireOn())
                .append("created_date", cart.getCreatedDate())
                .append("modified_date", cart.getModifiedDate());
        getNonTypeCollection().insertOne(document);
        cart.setId(document.getObjectId("_id"));
        return cart;
    }

    public boolean updateCart(ObjectId id, Collection<TargetValue> values) {
        return this.updateCart(id, values.toArray(new TargetValue[values.size()]));
    }

    public boolean updateCart(ObjectId id, TargetValue... values) {
        UpdateResult updateResult = getCollection().updateOne(Filters.eq("_id", id),
                Updates.combine(Stream.of(values)
                        .map(o -> Updates.set(o.getTarget(), o.getValue()))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }

    public Cart getAllRelatedCart(ObjectId userId, String sessionId) {
        List<Bson> uniqueFilters = new ArrayList<>();
//        uniqueFilters.add(Filters.eq("cart_status", CartStatus.ACTIVE.name()));
        if (!StringUtil.isNullOrEmpty(userId)) uniqueFilters.add(Filters.eq("user_id", userId));
        uniqueFilters.add(Filters.eq("session_id", sessionId));
        Cart cart = getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(Filters.or(uniqueFilters), Filters.eq("cart_status", CartStatus.ACTIVE.name())))
        )).first();
        return cart;
    }

    public <T> T getByTargets(List<KeyValue> targets, int projectType, Class<T> outputClass) {
        return getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(buildFilters(targets))),
                Aggregates.lookup("cart_product", "_id", "cart_id", "cart_products"),
                Aggregates.project(buildProjection(projectType))
        ), outputClass).first();
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case CART_SESSION_PROJECTION:
                return buildCartSessionProjection();
            default:
                return null;
        }
    }

    private Bson buildCartSessionProjection() {
        return Projections.include("user_id", "session_id",
                "cart_products._id", "cart_products.product_id", "cart_products.quantity");
    }
}
