package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.Permission;
import com.etybeno.ecommerce.data.enums.AccountType;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import org.bson.Document;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by thangpham on 20/02/2018.
 */
@Repository
public class PermissionRepository extends BaseDataAccess<Permission> {

    public PermissionRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<Permission> collection = getCollection();
        collection.createIndex(new Document("name", 1).append("account_type", 1), new IndexOptions().unique(true));
    }

    public boolean checkPermissionId(Integer id) {
        return 1 == getCollection().count(Filters.eq("_id", id));
    }

    public Permission createPermission(Permission permission) throws JsonProcessingException {
        if (permission.getId() == null || checkPermissionId(permission.getId()))
            permission.setId(getNextIntegerSequence("permission_seq_id", "value"));
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(permission));
        Date current = new Date();
        document.put("created_date", current);
        document.put("modified_date", current);
        getNonTypeCollection().insertOne(document);
        return permission;
    }

    public Permission getById(int id) {
        return getCollection().find(Filters.eq("_id", id)).first();
    }

    public Permission getPermission(String name, AccountType accountType) {
        return getCollection().find(Filters.and(
                Filters.eq("name", name),
                Filters.eq("account_type", accountType.name())
        ), Permission.class).first();
    }

}
