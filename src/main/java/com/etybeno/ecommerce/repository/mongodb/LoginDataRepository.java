package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.LoginData;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValues;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 31/01/2018.
 */
@Repository
public class LoginDataRepository extends BaseDataAccess<LoginData> {

    public static final int USERNAME_PROJECTION = 1;

    public LoginDataRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<LoginData> collection = getCollection();
        collection.createIndex(new Document("user_id", 1).append("login_type", 1).append("identification", 1),
                new IndexOptions().unique(true));
    }

    public LoginData createNewLoginData(LoginData loginData) throws JsonProcessingException {
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(loginData))
                .append("user_id", loginData.getUserId())
                .append("login_type", loginData.getLoginType().name())
                .append("created_date", loginData.getCreatedDate())
                .append("modified_date", loginData.getModifiedDate());
        getNonTypeCollection().insertOne(document);
        loginData.setId(document.getObjectId("_id"));
        return loginData;
    }

    public boolean updateLoginData(ObjectId id, Map<String, Object> values) {
        UpdateResult updateResult = getCollection().updateOne(
                Filters.and(Filters.eq("user_id", id), Filters.eq("login_type", values.get("login_type"))),
                Updates.combine(values.keySet().stream()
                        .map(k -> Updates.set(k, values.get(k)))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }

    public <T> List<T> getUserdetailByTargets(List<KeyValue> targets, int projectionType, Class<T> outputClass) {
        AggregateIterable<T> aggregate = getCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.and(buildFilters(targets))),
                Aggregates.lookup("user", "user_id", "_id", "users"),
                Aggregates.unwind("$users"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList("$users", "$$ROOT")))),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass);
        List<T> result = new ArrayList();
        for (T t : aggregate) result.add(t);
        return result;
    }

    public boolean deleteLoginData(ObjectId objectId) {
        DeleteResult deleteResult = getCollection().deleteOne(Filters.eq("_id", objectId));
        return deleteResult.getDeletedCount() == 1;
    }

    public boolean deleteLoginData(ObjectId userId, LoginType loginType, String identification) {
        DeleteResult deleteResult = getCollection().deleteOne(Filters.and(Filters.eq("user_id", userId),
                Filters.eq("identification", identification),
                Filters.eq("login_type", loginType.name())));
        return deleteResult.getDeletedCount() == 1;
    }

    public boolean deleteLoginData(ObjectId userId, LoginType loginType) {
        DeleteResult deleteResult = getCollection().deleteOne(Filters.and(Filters.eq("user_id", userId),
                Filters.eq("login_type", loginType.name())));
        return deleteResult.getDeletedCount() == 1;
    }

    public boolean checkLoginData(String identification, String validation, LoginType loginType) {
        long count = getCollection().count(Filters.and(
                Filters.eq("identification", identification),
                Filters.eq("validation", validation),
                Filters.eq("login_type", loginType.name())));
        return count == 1;
    }

    public boolean checkLoginData(ObjectId userId, LoginType loginType) {
        long count = getCollection().count(
                Filters.and(Filters.eq("user_id", userId),
                Filters.eq("login_type", loginType.name())));
        return count == 1;
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case USERNAME_PROJECTION:
                return buildCategoryProjection();
            default:
                return null;
        }
    }

    private Bson buildCategoryProjection() {
        return Projections.fields(Projections.include("username"));
    }

}
