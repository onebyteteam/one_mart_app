package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.I18NTranslation;
import com.etybeno.ecommerce.data.entity.Term;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static com.etybeno.common.util.StringUtil.OBJECT_MAPPER;

/**
 * Created by thangpham on 25/12/2017.
 */
@Repository
public class TermRepository extends BaseDataAccess<Term> {

    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;

    public TermRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<Term> collection = getCollection();
        collection.createIndex(new Document("i18n_name_id", 1), new IndexOptions().unique(true));
    }

    public boolean checkTermIdExist(ObjectId termId) {
        long term_id = getCollection().count(Filters.eq("_id", termId));
        return term_id == 1;
    }

    public Term getTermById(ObjectId termId) {
        FindIterable<Term> dbObject = getCollection().find(new Document("_id", termId));
        return dbObject.first();
    }

    /**
     * * If the term already exists by slug, get the old one
     * @param slug
     * @param name
     * @param locale
     * @return
     */
    public Term insertNewTerm(String slug, String name, String locale, String i18nType) {
        List<KeyValue> targets = Arrays.asList(
                new TargetValue("slug", slug),
                new TargetValue("locale", locale),
                new TargetValue("type", i18nType));
        I18NTranslation termBySlug = i18NTranslationRepository.getOne(targets);
        if(null != termBySlug) {
             if(termBySlug.getText().equals(name)) {
                 return getOne(Arrays.asList(new TargetValue("i18n_name_id", termBySlug.getObjectId())));
             } else throw new IllegalArgumentException("This slug: " + slug + " already exist but not same name");
        }
        ObjectId i18nId = i18NTranslationRepository.addNew(slug, name, i18nType, locale);
        Term term = new Term(ObjectId.get(), i18nId);
        getCollection().insertOne(term);
        return term;
    }

    public Document getByTermI18nIdAndTaxonomy(ObjectId i18nNameId, String taxonomy) {
        return getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("i18n_name_id", i18nNameId)),
                Aggregates.lookup("term_taxonomy", "_id", "term_id", "term_taxonomy"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList("$$ROOT", new Document("$arrayElemAt", Arrays.asList("$term_taxonomy", 0)))
                        ))),
                Aggregates.match(Filters.eq("taxonomy", taxonomy))
        )).first();
    }

    public List<Document> getByTermI18nIdsAndTaxonomies(List<ObjectId> i18nNameIds, String... taxonomies) {
        AggregateIterable<Document> aggregate = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.in("i18n_name_id", i18nNameIds)),
                Aggregates.lookup("term_taxonomy", "_id", "term_id", "term_taxonomy"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList("$$ROOT", new Document("$arrayElemAt", Arrays.asList("$term_taxonomy", 0)))
                        ))),
                Aggregates.match(Filters.in("taxonomy", taxonomies))
        ));
        List<Document> documents = new ArrayList<>();
        for(Document d : aggregate) documents.add(d);
        return documents;
    }

}
