package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.data.entity.CartProduct;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.TargetValue;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 09/03/2018.
 */
@Repository
public class CartProductRepository extends BaseDataAccess<CartProduct> {


    public CartProductRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        getCollection().createIndex(new Document()
                .append("cart_id", 1).append("product_id", 1), new IndexOptions().unique(true));
    }

    /**
     * Add or update a product to cart. If the product is already exist by #cart_id (session_id),
     * it will return false, otherwise true.
     * @param cartProduct
     * @return
     */
    public boolean createOrUpdateCartProduct(CartProduct cartProduct) {
        Bson filters = Filters.and(
                Filters.eq("cart_id", cartProduct.getCartId()),
                Filters.eq("product_id", cartProduct.getProductId()));
        UpdateResult updateResult = getCollection().updateOne(filters,
                Updates.combine(
                        Updates.set("cart_id", cartProduct.getCartId()),
                        Updates.set("product_id", cartProduct.getProductId()),
                        Updates.inc("quantity", cartProduct.getQuantity())),
                new UpdateOptions().upsert(true));
        BsonValue upsertedId = updateResult.getUpsertedId();
        if (null == upsertedId || upsertedId.isNull()){
            ObjectId id = getNonTypeCollection().find(filters).first().getObjectId("_id");
            cartProduct.setId(id);
            return updateResult.getModifiedCount() == 1;
        } else {
            cartProduct.setId(upsertedId.asObjectId().getValue());
            return true;
        }
    }

    public boolean updateCartProduct(ObjectId cartProductId, List<TargetValue> values) {
        UpdateResult updateResult = getCollection().updateOne(Filters.eq("_id", cartProductId),
                Updates.combine(values.stream()
                        .map(o -> Updates.set(o.getTarget(), o.getValue()))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }

    public boolean deleteCartProduct(ObjectId cartId, ObjectId productId) {
        long deletedCount = getCollection().deleteOne(Filters.and(
                Filters.eq("cart_id", cartId),
                Filters.eq("product_id", productId))).getDeletedCount();
        return 1 == deletedCount;
    }
}
