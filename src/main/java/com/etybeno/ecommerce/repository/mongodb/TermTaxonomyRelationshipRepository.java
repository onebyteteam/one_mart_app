package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.data.entity.TermTaxonomyRelationship;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thangpham on 04/05/2018.
 */
@Repository
public class TermTaxonomyRelationshipRepository extends BaseDataAccess<TermTaxonomyRelationship> {

    public TermTaxonomyRelationshipRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<TermTaxonomyRelationship> collection = getCollection();
        collection.createIndex(new Document("term_taxonomy_id", 1).append("type", 1),
                new IndexOptions().unique(true));
    }

    public List<TermTaxonomyRelationship> getTermTaxonomyRelationshipWithPrefix(ObjectId ttId, String prefix) {
        FindIterable<TermTaxonomyRelationship> queryRS = getCollection().find(Filters.and(
                Filters.eq("term_taxonomy_id", ttId),
                Filters.regex("type", "^" + prefix)));
        List<TermTaxonomyRelationship> rs = new ArrayList<>();
        for (TermTaxonomyRelationship ttr : queryRS) rs.add(ttr);
        return rs;
    }

    public ObjectId createOrUpdateTermTaxonomyRelationship(TermTaxonomyRelationship ttRelationship) {
        Bson filters = Filters.and(
                Filters.eq("term_taxonomy_id", ttRelationship.getTermTaxonomyId()),
                Filters.eq("type", ttRelationship.getType()));
        UpdateResult updateResult = getCollection().updateOne(filters,
                Updates.combine(
                        Updates.set("term_taxonomy_id", ttRelationship.getTermTaxonomyId()),
                        Updates.set("type", ttRelationship.getType()),
                        Updates.addEachToSet("relational_tt_ids", ttRelationship.getRelationalTTIds())),
                new UpdateOptions().upsert(true));
        BsonValue upsertedId = updateResult.getUpsertedId();
        if (null == upsertedId || upsertedId.isNull())
            return getNonTypeCollection().find(filters).first().getObjectId("_id");
        else return upsertedId.asObjectId().getValue();
    }
}
