package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.Supplier;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Map;

import static com.etybeno.common.util.StringUtil.OBJECT_MAPPER;

/**
 * Created by thangpham on 16/05/2018.
 */
@Repository
public class SupplierRepository extends BaseDataAccess<Supplier> {

    public static final int SELLER_DETAIL_PROJECTION = 1, SELLER_LIST_PROJECTION = 2;

    public SupplierRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        getCollection().createIndex(new Document("slug", 1), new IndexOptions().unique(true));
        getCollection().createIndex(new Document("name", 1));
    }

    public boolean createNewSupplier(Supplier supplier) throws JsonProcessingException {
        Date current = new Date();
        Document document = Document.parse(StringUtil.OBJECT_MAPPER.writeValueAsString(supplier));
        document.put("created_date", current);
        document.put("modified_date", current);
        document.put("location", supplier.getLocation());
        document.put("accounts", supplier.getAccounts());
        getNonTypeCollection().insertOne(document);
        supplier.setId(document.getObjectId("_id"));
        return true;
    }

    public boolean updateSupplier(Supplier supplier) {
        UpdateResult updateResult = getCollection().updateOne(
                Filters.or(Filters.eq("_id", supplier.getId())),
                new Document(OBJECT_MAPPER.convertValue(supplier, Map.class)));
        return updateResult.getModifiedCount() == 1;
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case SELLER_DETAIL_PROJECTION:
                return buildSellerProjection();
            case SELLER_LIST_PROJECTION:
                return buildSellerListProjection();
            default:
                return null;
        }
    }

    private Bson buildSellerListProjection() {
        return Projections.fields(Projections.include("name", "slug", "logo"), Projections.computed("seller_id", "$_id"));
    }

    private Bson buildSellerProjection() {
        return Projections.fields(Projections.include("name", "slug", "logo", "address", "phone", "description"),
                Projections.computed("seller_id", "$_id"));
    }
}
