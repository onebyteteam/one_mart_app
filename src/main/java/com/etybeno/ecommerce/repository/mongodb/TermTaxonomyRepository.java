package com.etybeno.ecommerce.repository.mongodb;

import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.TermTaxonomy;
import com.etybeno.mongodb.base.BaseDataAccess;
import com.etybeno.mongodb.model.TargetValue;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static com.etybeno.common.util.StringUtil.OBJECT_MAPPER;

/**
 * Created by thangpham on 25/12/2017.
 */
@Repository
public class TermTaxonomyRepository extends BaseDataAccess<TermTaxonomy> {

    public final static int CATEGORY_PROJECTION = 1, ATTRIBUTE_PROJECTION = 2;

    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;

    public TermTaxonomyRepository(String clientName) throws Exception {
        super(clientName);
    }

    @Override
    protected void initCollection() {
        MongoCollection<TermTaxonomy> collection = getCollection();
        collection.createIndex(new Document().append("term_id", 1).append("taxonomy", 1), new IndexOptions().unique(true));
        collection.createIndex(new Document().append("select_id", 1), new IndexOptions().unique(true));
        collection.createIndex(new Document().append("taxonomy", 1));
    }

    public boolean checkById(ObjectId id) {
        return 1 == getCollection().count(Filters.eq("_id", id));
    }

    public boolean checkById(String id) {
        return checkById(new ObjectId(id));
    }

    public boolean checkByTaxonomy(String taxonomy) {
        return getCollection().count(Filters.eq("taxonomy", taxonomy)) == 1;
    }

    public TermTaxonomy getByTaxonomyAndTermId(ObjectId termId, String taxonomy) {
        return getCollection().find(
                Filters.and(Filters.eq("term_id", termId), Filters.eq("taxonomy", taxonomy))).first();
    }

    public TermTaxonomy insertNewTermTaxonomy(ObjectId termId, String taxonomy, String description, ObjectId parentId, String locale,
                                              String i18nType) {
        TermTaxonomy tt = getByTaxonomyAndTermId(termId, taxonomy);
        if (null != tt) return tt;
        ObjectId descriptionId = i18NTranslationRepository.addNew(null, description, i18nType, locale);
        int selectId = getNextIntegerSequence("select_id", "seq_select_id");
        tt = new TermTaxonomy();
        tt.setId(ObjectId.get());
        tt.setTermId(termId);
        tt.setTaxonomy(taxonomy);
        tt.setI18nDescriptionId(descriptionId);
        tt.setParentId(parentId);
        tt.setSelectId(selectId);
        getCollection().insertOne(tt);
        return tt;
    }

    public boolean updateTermTaxonomy(ObjectId id, List<TargetValue> values) {
        UpdateResult updateResult = getCollection().updateOne(Filters.eq("_id", id),
                Updates.combine(values.stream()
                        .map(o -> Updates.set(o.getTarget(), o.getValue()))
                        .collect(Collectors.toList())));
        return updateResult.getModifiedCount() == 1;
    }


    public boolean updateTermTaxonomy(TermTaxonomy termTaxonomy) {
        UpdateResult updateResult = getCollection().updateOne(
                Filters.eq("_id", termTaxonomy.getId()),
                new Document(OBJECT_MAPPER.convertValue(termTaxonomy, Map.class)));
        return updateResult.getModifiedCount() == 1;
    }

    public boolean deleteTermTaxonomy(ObjectId termId) {
        DeleteResult deleteResult = getCollection().deleteOne(Filters.eq("_id", termId));
        return deleteResult.getDeletedCount() == 1;
    }

    public Document getByTermTaxonomyId(ObjectId termTaxonomyId) {
        return getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("_id", termTaxonomyId)),
                Aggregates.lookup("term", "term_id", "_id", "term"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$term", 0)), "$$ROOT")
                        )))
        )).first();
    }

    public Document getByTermTaxonomySelectId(int selectId) {
        return getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("select_id", selectId)),
                Aggregates.lookup("term", "term_id", "_id", "term"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$term", 0)), "$$ROOT")
                        )))
        )).first();
    }

    private <T> List<T> getAllByTargets(Map<String, List<Object>> targets, int projectionType, Class<T> outputClass) {
        AggregateIterable<T> aggregate = getCollection().aggregate(Arrays.asList(
                Aggregates.lookup("term", "term_id", "_id", "term"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$term", 0)), "$$ROOT")
                        ))),
                Aggregates.match(Filters.and(
                        targets.keySet().stream()
                                .map(s -> Filters.in(s, targets.get(s)))
                                .collect(Collectors.toSet()))),
                Aggregates.project(buildProjection(projectionType))
        ), outputClass);
        List<T> result = new ArrayList();
        for (T t : aggregate) result.add(t);
        return result;
    }

    public <T> List<T> getAllByParentId(ObjectId parentId, int projectionType, Class<T> clazz) {
        Map<String, List<Object>> targets = new HashMap<>();
        targets.put("parent_id", Arrays.asList(parentId));
        List<T> allByTargets = getAllByTargets(targets, projectionType, clazz);
        return allByTargets;
    }

    /**
     * Get parent term by child's _id. Use maxDepth to query how depth parent are, -1 is maximum depth
     *
     * @param termId
     * @param maxDepth
     * @return
     */
    public List<Document> getParentTermTaxonomies(ObjectId termId, int maxDepth) {
        AggregateIterable<Document> aggregate = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("_id", termId)),
                Aggregates.graphLookup("term_taxonomy", "$parent_id", "parent_id", "_id", "parents",
                        new GraphLookupOptions().maxDepth(maxDepth == -1 ? Integer.MAX_VALUE : maxDepth)),
                Aggregates.unwind("$parents"),
                new Document("$replaceRoot", new Document("newRoot", "$parents")),
                Aggregates.lookup("term", "term_id", "_id", "term"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$term", 0)), "$$ROOT")
                        ))),
                Aggregates.project(Projections.include("_id"))
        ));
        List<Document> result = new ArrayList();
        for (Document t : aggregate) result.add(t);
        return result;
    }

    /**
     * Get children term by parent's _id. Use maxDepth to query how depth children are, -1 is maximum depth
     *
     * @param id
     * @param maxDepth
     * @return
     */
    public List<Document> getChildrenTermTaxonomies(ObjectId id, int maxDepth) {
        AggregateIterable<Document> aggregate = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("_id", id)),
                Aggregates.graphLookup("term_taxonomy", "$_id", "_id", "parent_id", "children"
                        , new GraphLookupOptions().maxDepth(maxDepth == -1 ? Integer.MAX_VALUE : maxDepth)),
                Aggregates.unwind("$children"),
                new Document("$replaceRoot", new Document("newRoot", "$children")),
                Aggregates.lookup("term", "term_id", "_id", "term"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$term", 0)), "$$ROOT")
                        ))),
                Aggregates.project(Projections.include("_id"))
        ));
        List<Document> result = new ArrayList();
        for (Document t : aggregate) result.add(t);
        return result;
    }

    public List<Document> getAllByTaxonomy(String taxonomy) {
        AggregateIterable<Document> aggregate = getNonTypeCollection().aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("taxonomy", taxonomy)),
                Aggregates.lookup("term", "term_id", "_id", "term"),
                new Document("$replaceRoot", new Document("newRoot",
                        new Document("$mergeObjects",
                                Arrays.asList(new Document("$arrayElemAt", Arrays.asList("$term", 0)), "$$ROOT")
                        ))),
                Aggregates.sort(Sorts.ascending("rank")),
                Aggregates.project(Projections.include("_id"))
        ));
        List<Document> result = new ArrayList();
        for (Document t : aggregate) result.add(t);
        return result;
    }

    @Override
    protected Bson buildProjection(int projectionType) {
        switch (projectionType) {
            case CATEGORY_PROJECTION:
                return buildCategoryProjection();
            case ATTRIBUTE_PROJECTION:
                return buildAttributeProjection();
            default:
                return null;
        }
    }

    private Bson buildAttributeProjection() {
        return Projections.fields(Projections.include("slug", "name", "description"),
                Projections.computed("attribute_id", "$_id"));
    }

    private Bson buildCategoryProjection() {
        return Projections.fields(Projections.include("parent_id", "slug", "description", "locale"),
                Projections.computed("category_id", "$_id"),
                Projections.computed("name", "$text"));
    }
}