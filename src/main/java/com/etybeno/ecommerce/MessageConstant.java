package com.etybeno.ecommerce;

/**
 * Created by thangpham on 19/05/2018.
 */
public interface MessageConstant {

    String AUTHEN_GREETING = "general.authentication.greeting";
    String BEST_SELLER = "general.best_seller";
    String NEW_ARRIVAL = "general.new_arrival";
    String ERROR_BAD_AUTHEN = "error.login.bad_authen";
}