package com.etybeno.ecommerce.configuration.production;

import com.etybeno.common.util.StringUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;

/**
 * Created by thangpham on 30/01/2018.
 */
@Configuration
@EnableCaching
@PropertySource("file:config/redis-info-configs.properties")
@Profile("prod")
public class CachingConfig {

    private @Value("${data.redis.ecommerceCache}") String redisAddress;

    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        String[] split = redisAddress.split(":");
        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(split[0]);
        factory.setPort(StringUtil.safeParseInt(split[1], 6379));
        factory.setUsePool(true);
        return factory;
    }

    @Bean
    RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        return redisTemplate;
    }

    @Bean
    public CacheManager cacheManager() {
        return new RedisCacheManager(redisTemplate(), Arrays.asList("cart", "order", "user", "permission", "product", "product_list",
                "category", "attribute", "catalog", "seller", "count_product", "common"));
    }

}
