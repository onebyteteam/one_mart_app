package com.etybeno.ecommerce.configuration.production;

import com.etybeno.ecommerce.repository.mongodb.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static com.etybeno.ecommerce.Constant.MongoDB.DEV_ECOMMERCE_NAME;
import static com.etybeno.ecommerce.Constant.MongoDB.PRODUCTION_ECOMMERCE_NAME;

/**
 * Created by thangpham on 25/12/2017.
 */
@Configuration
@Profile("prod")
public class RepositoryBean {

    @Bean
    public ConfigurationRepository configurationRepository() throws Exception {
        return new ConfigurationRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public PermissionRepository permissionRepository() throws Exception {
        return new PermissionRepository(PRODUCTION_ECOMMERCE_NAME);
    }
    @Bean
    public TermRepository termRepository() throws Exception {
        return new TermRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public TermTaxonomyRepository termTaxonomyRepository() throws Exception {
        return new TermTaxonomyRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public AbstractProductRepository abstractProductRepository() throws Exception {
        return new AbstractProductRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public UserRepository userRepository() throws Exception {
        return new UserRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public LoginDataRepository loginDataRepository() throws Exception {
        return new LoginDataRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public AccountRepository accountRepository() throws Exception {
        return new AccountRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public AbstractCollectionRepository abstractCollectionRepository() throws Exception {
        return new AbstractCollectionRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public CartRepository cartRepository() throws Exception {
        return new CartRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public CartProductRepository cartProductRepository() throws Exception {
        return new CartProductRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public OrderRepository orderRepository() throws Exception {
        return new OrderRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public OrderProductRepository orderProductRepository() throws Exception {
        return new OrderProductRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean(name = {"termTaxonomyRelationshipRepository", "ttRelationshipRepository"})
    public TermTaxonomyRelationshipRepository termTaxonomyRelationshipRepository() throws Exception {
        return new TermTaxonomyRelationshipRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public SupplierRepository supplierRepository() throws Exception {
        return new SupplierRepository(PRODUCTION_ECOMMERCE_NAME);
    }

    @Bean
    public I18NTranslationRepository i18NTranslationRepository() throws Exception {
        return new I18NTranslationRepository(PRODUCTION_ECOMMERCE_NAME);
    }

}
