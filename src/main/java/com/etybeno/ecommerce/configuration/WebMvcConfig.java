package com.etybeno.ecommerce.configuration;

import com.etybeno.ecommerce.Constant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * Created by thangpham on 19/05/2018.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.forLanguageTag("vi-VN"));
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/robots.txt").addResourceLocations("file:resources/root-serve/robots.txt");
        registry.addResourceHandler("/adminlte/**").addResourceLocations("file:resources/static/adminlte/")
                .setCachePeriod(Constant.Time.THREE_DAYS_IN_SECONDS);
        registry.addResourceHandler("/electro/**").addResourceLocations("file:resources/static/electro/")
                .setCachePeriod(Constant.Time.THREE_DAYS_IN_SECONDS);
        registry.addResourceHandler("/products/**").addResourceLocations("file:resources/products/")
                .setCachePeriod(Constant.Time.THREE_DAYS_IN_SECONDS);
        registry.addResourceHandler("/catalogs/**").addResourceLocations("file:resources/catalogs/")
                .setCachePeriod(Constant.Time.THREE_DAYS_IN_SECONDS);
        registry.addResourceHandler("/.well-known/**").addResourceLocations("file:resources/.well-known/");
    }
}
