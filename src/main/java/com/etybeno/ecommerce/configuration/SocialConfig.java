package com.etybeno.ecommerce.configuration;

import com.etybeno.ecommerce.controller.mvc.CustomConnectController;
import com.etybeno.ecommerce.repository.social.MongoUsersConnectionRepository;
import com.etybeno.ecommerce.service.social.SocialConnectionSignup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;

/**
 * Created by thangpham on 24/02/2018.
 */
@EnableSocial
@Configuration
public class SocialConfig implements SocialConfigurer {

    public static final String FACEBOOK_APPID_KEY = "ecommerce.social.facebook.appId";
    public static final String FACEBOOK_APPSECRET_KEY = "ecommerce.social.facebook.appSecret";
    public static final String GOOGLE_CLIENTID_KEY = "ecommerce.social.google.clientId";
    public static final String GOOGLE_CLIENTSECRET_KEY = "ecommerce.social.google.clientSecret";

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig, Environment env) {
        cfConfig.addConnectionFactory(
                new FacebookConnectionFactory(env.getProperty(FACEBOOK_APPID_KEY), env.getProperty(FACEBOOK_APPSECRET_KEY)));
        cfConfig.addConnectionFactory(
                new GoogleConnectionFactory(env.getProperty(GOOGLE_CLIENTID_KEY), env.getProperty(GOOGLE_CLIENTSECRET_KEY)));
    }

    @Override
    public UserIdSource getUserIdSource() {
        return new AuthenticationNameUserIdSource();
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        return new MongoUsersConnectionRepository(connectionFactoryLocator, new SocialConnectionSignup(), Encryptors.noOpText());
    }

    @Bean
    public ConnectController connectController(ConnectionFactoryLocator locator, ConnectionRepository repository) {
        ConnectController connectController = new CustomConnectController(locator, repository);
        connectController.setViewPath("/dang-ky");
        return connectController;
    }
}