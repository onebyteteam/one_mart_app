package com.etybeno.ecommerce.configuration.dev;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

/**
 * Created by thangpham on 30/01/2018.
 */
@Configuration
@EnableCaching
@Profile("dev")
public class DevCachingConfig {

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new ConcurrentMapCache("cart", true),
                new ConcurrentMapCache("order"),
                new ConcurrentMapCache("user"),
                new ConcurrentMapCache("permission"),
                new ConcurrentMapCache("count_product"),
                new ConcurrentMapCache("product"),
                new ConcurrentMapCache("product_list"),
                new ConcurrentMapCache("category"),
                new ConcurrentMapCache("attribute"),
                new ConcurrentMapCache("seller"),
                new ConcurrentMapCache("catalog")));
        return cacheManager;
    }

}
