package com.etybeno.ecommerce.configuration.dev;

import com.etybeno.ecommerce.repository.mongodb.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static com.etybeno.ecommerce.Constant.MongoDB.DEV_ECOMMERCE_NAME;
import static com.etybeno.ecommerce.Constant.MongoDB.PRODUCTION_ECOMMERCE_NAME;


/**
 * Created by thangpham on 08/02/2018.
 */
@Configuration
@Profile("dev")
public class DevRepositoryBean {

    @Bean
    public ConfigurationRepository configurationRepository() throws Exception {
        return new ConfigurationRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public TermRepository termRepository() throws Exception {
        return new TermRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public TermTaxonomyRepository termTaxonomyRepository() throws Exception {
        return new TermTaxonomyRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public AbstractProductRepository abstractProductRepository() throws Exception {
        return new AbstractProductRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public UserRepository userRepository() throws Exception {
        return new UserRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public LoginDataRepository loginDataRepository() throws Exception {
        return new LoginDataRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public AccountRepository accountRepository() throws Exception {
        return new AccountRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public AbstractCollectionRepository abstractCollectionRepository() throws Exception {
        return new AbstractCollectionRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public PermissionRepository permissionRepository() throws Exception {
        return new PermissionRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public CartRepository cartRepository() throws Exception {
        return new CartRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public CartProductRepository cartProductRepository() throws Exception {
        return new CartProductRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public OrderRepository orderRepository() throws Exception {
        return new OrderRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public OrderProductRepository orderProductRepository() throws Exception {
        return new OrderProductRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean(name = {"termTaxonomyRelationshipRepository", "ttRelationshipRepository"})
    public TermTaxonomyRelationshipRepository termTaxonomyRelationshipRepository() throws Exception {
        return new TermTaxonomyRelationshipRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public SupplierRepository supplierRepository() throws Exception {
        return new SupplierRepository(DEV_ECOMMERCE_NAME);
    }

    @Bean
    public I18NTranslationRepository i18NTranslationRepository() throws Exception {
        return new I18NTranslationRepository(DEV_ECOMMERCE_NAME);
    }
}
