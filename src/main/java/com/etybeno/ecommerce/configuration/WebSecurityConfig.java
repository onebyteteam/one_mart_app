package com.etybeno.ecommerce.configuration;

import com.etybeno.ecommerce.service.social.RegistryUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;
import org.springframework.social.security.SpringSocialConfigurer;

import javax.inject.Inject;

import static com.etybeno.ecommerce.Constant.StringConstant.CART_SESSION_NAME;
import static com.etybeno.ecommerce.Constant.StringConstant.MAIN_SESSION_NAME;

/**
 * Created by thangpham on 03/02/2018.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(2)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RegistryUserDetailService registryUserDetailService;
    @Autowired
    private FindByIndexNameSessionRepository findByIndexNameSessionRepository;
    @Inject
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Inject
    private String themeName;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        // Allows for static content
        http.authorizeRequests()
                .antMatchers("/" + themeName + "/css/**").permitAll()
                .antMatchers("/" + themeName + "/js/**").permitAll()
                .antMatchers("/" + themeName + "/fonts/**").permitAll()
                .antMatchers("/" + themeName + "/images/**").permitAll();
        // Allows for pages
        http.authorizeRequests()
                .antMatchers("/", "/san-pham/{product_name}-{product_sku}", "/{category_slug}", "/index").permitAll();
        //
        SpringSocialConfigurer socialConfigurer = new SpringSocialConfigurer()
                .signupUrl("/");
        //
        http
            .formLogin()
                .loginPage("/dang-nhap").loginProcessingUrl("/process/login")
                .failureHandler(new LoginFailureHandler())
                .defaultSuccessUrl("/").permitAll()
                .and()
            .logout()
                .logoutUrl("/dang-xuat").logoutSuccessUrl("/")
                .deleteCookies(MAIN_SESSION_NAME, CART_SESSION_NAME).invalidateHttpSession(true)
                .permitAll()
            .and()
            .apply(socialConfigurer);
//
        http.sessionManagement()
                .sessionFixation().migrateSession()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .maximumSessions(5)
                .sessionRegistry(sessionRegistry());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(registryUserDetailService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    @SuppressWarnings("unchecked")
    public SpringSessionBackedSessionRegistry sessionRegistry() {
        return new SpringSessionBackedSessionRegistry(this.findByIndexNameSessionRepository);
    }

}
