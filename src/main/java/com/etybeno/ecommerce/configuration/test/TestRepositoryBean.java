package com.etybeno.ecommerce.configuration.test;

import com.etybeno.ecommerce.repository.mongodb.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static com.etybeno.ecommerce.Constant.MongoDB.TEST_ECOMMERCE_NAME;


/**
 * Created by thangpham on 08/02/2018.
 */
@Configuration
@Profile("test")
public class TestRepositoryBean {
    @Bean
    public TermRepository termRepository() throws Exception {
        return new TermRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public TermTaxonomyRepository termTaxonomyRepository() throws Exception {
        return new TermTaxonomyRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public AbstractProductRepository abstractProductRepository() throws Exception {
        return new AbstractProductRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public UserRepository userRepository() throws Exception {
        return new UserRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public LoginDataRepository loginDataRepository() throws Exception {
        return new LoginDataRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public AccountRepository accountRepository() throws Exception {
        return new AccountRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean(name = {"termTaxonomyRelationshipRepository", "ttRelationshipRepository"})
    public TermTaxonomyRelationshipRepository termTaxonomyRelationshipRepository() throws Exception {
        return new TermTaxonomyRelationshipRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public AbstractCollectionRepository abstractCollectionRepository() throws Exception {
        return new AbstractCollectionRepository(TEST_ECOMMERCE_NAME);
    }

    @Bean
    public PermissionRepository permissionRepository() throws Exception {
        return new PermissionRepository(TEST_ECOMMERCE_NAME);
    }

}
