package com.etybeno.ecommerce.configuration;

import com.etybeno.ecommerce.service.social.RegistryUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;
import org.springframework.social.security.SpringSocialConfigurer;

import javax.inject.Inject;

import static com.etybeno.ecommerce.Constant.StringConstant.CART_SESSION_NAME;
import static com.etybeno.ecommerce.Constant.StringConstant.MAIN_SESSION_NAME;

/**
 * Created by thangpham on 15/04/2018.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(1)
public class AdminWebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RegistryUserDetailService registryUserDetailService;
    @Autowired
    private FindByIndexNameSessionRepository findByIndexNameSessionRepository;
    @Inject
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Inject
    private String adminThemeName;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        // Allows for static content
        http.authorizeRequests()
                .antMatchers("/" + adminThemeName + "/css/**").permitAll()
                .antMatchers("/" + adminThemeName + "/js/**").permitAll()
                .antMatchers("/" + adminThemeName + "/fonts/**").permitAll()
                .antMatchers("/" + adminThemeName + "/images/**").permitAll();
        //
        SpringSocialConfigurer socialConfigurer = new SpringSocialConfigurer()
                .connectionAddedRedirectUrl("/admin/")
                .postLoginUrl("/admin/")
                .signupUrl("/admin/login");
        //
        http
            .antMatcher("/admin/**")
                .authorizeRequests().anyRequest().hasAnyAuthority("ADMINISTRATOR@normal")
                .and()
            .formLogin()
                .loginPage("/admin/login").loginProcessingUrl("/admin/process/login")
                .defaultSuccessUrl("/admin/").permitAll()
                .and()
            .logout()
                .logoutSuccessUrl("/admin/login").logoutUrl("/admin/process/logout")
                .permitAll()
                .and()
            .apply(socialConfigurer);
//
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
                .maximumSessions(1)
                .sessionRegistry(sessionRegistry());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(registryUserDetailService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    @SuppressWarnings("unchecked")
    public SpringSessionBackedSessionRegistry sessionRegistry() {
        return new SpringSessionBackedSessionRegistry(this.findByIndexNameSessionRepository);
    }

}
