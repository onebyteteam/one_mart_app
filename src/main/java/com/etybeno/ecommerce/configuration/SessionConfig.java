package com.etybeno.ecommerce.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import static com.etybeno.ecommerce.Constant.StringConstant.MAIN_SESSION_NAME;
import static com.etybeno.ecommerce.Constant.Time.THIRTY_DAYS_IN_SECONDS;

/**
 * Created by thangpham on 10/03/2018.
 */
@Configuration
@EnableRedisHttpSession(redisNamespace = "ecommerce", maxInactiveIntervalInSeconds = THIRTY_DAYS_IN_SECONDS)
public class SessionConfig extends AbstractHttpSessionApplicationInitializer {


    @Value("${ecommerce.server.cookie.domain}")
    private String cookieDomain;
//    @Bean
//    public JedisConnectionFactory connectionFactory() {
//        return new JedisConnectionFactory();
//    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName(MAIN_SESSION_NAME); // <1>
        serializer.setCookiePath("/"); // <2>
        serializer.setDomainName(cookieDomain);
        serializer.setCookieMaxAge(THIRTY_DAYS_IN_SECONDS);
        return serializer;
    }

}
