package com.etybeno.ecommerce.service;

import com.etybeno.common.util.DateTimeUtil;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.*;
import com.etybeno.ecommerce.data.enums.CartStatus;
import com.etybeno.ecommerce.data.enums.OrderStatus;
import com.etybeno.ecommerce.data.form.OrderForm;
import com.etybeno.ecommerce.data.model.*;
import com.etybeno.ecommerce.repository.mongodb.CartProductRepository;
import com.etybeno.ecommerce.repository.mongodb.CartRepository;
import com.etybeno.ecommerce.repository.mongodb.OrderProductRepository;
import com.etybeno.ecommerce.repository.mongodb.OrderRepository;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.util.concurrent.AtomicDouble;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static com.etybeno.ecommerce.Constant.Time.SEVEN_DAYS_IN_SECONDS;
import static com.etybeno.ecommerce.repository.mongodb.CartRepository.CART_SESSION_PROJECTION;

/**
 * Created by thangpham on 09/03/2018.
 */
@Service
public class ShoppingService {

    static final Logger LOGGER = LoggerFactory.getLogger(ShoppingService.class);

    @Autowired
    private ProductService productService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartProductRepository cartProductRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderProductRepository orderProductRepository;
    @Resource
    private ShoppingService self;

    //    @Cacheable(value = "cart", key = "{'cart_session', #username, #session, #locale}", unless = "result != null")
    public CartSession getCartSession(ObjectId userId, String session, String locale) throws JsonProcessingException, IllegalAccessException {
        Cart currentCart = self.getActiveCart(session, userId);
        if (null == currentCart || currentCart.getCartStatus() == CartStatus.EXPIRED) return null;
        CartSession cartSession = cartRepository.getByTargets(Arrays.asList(new TargetValue("_id", currentCart.getId())),
                CART_SESSION_PROJECTION, CartSession.class);
        List<CartProduct> cartProducts = cartProductRepository.getAll(Arrays.asList(new TargetValue("cart_id", cartSession.getCartIdAsObjectId())));
        List<CartProductSession> cpsList = new ArrayList<>();
        if (!cartProducts.isEmpty()) {
            double totalSum = 0;
            for (CartProduct cartProduct : cartProducts) {
                CartProductSession cps = new CartProductSession();
                Product product = productService.getProductById(cartProduct.getProductId(), locale);
                cps.setProduct(product);
                double total = product.getSalePrice() * cartProduct.getQuantity();
                cps.setTotal(total);
                totalSum += total;
                cps.setQuantity(cartProduct.getQuantity());
                cps.setIdAsObjectId(cartProduct.getId());
                cps.setProductIdAsObjectId(cartProduct.getProductId());
                cpsList.add(cps);
            }
            cartSession.setTotalSum(totalSum);
        }
        cartSession.setCartProducts(cpsList);
        return cartSession;
    }

    //    @CacheEvict(value = "cart", key = "{'cart_session', #username, #session, #locale}")
    public boolean addNewProductToCart(String username, String session, String productSku, int quantity)
            throws NoSuchAlgorithmException, IllegalAccessException, JsonProcessingException {
        ObjectId userId = customerService.getIdByUsername(username);
        Cart currentCart = getActiveCart(session, userId);
        if (null == currentCart || currentCart.getCartStatus() == CartStatus.EXPIRED)
            currentCart = createNewCart(userId, session);
        ObjectId productId = productService.getProductIdBySku(productSku);
        if (null == productId) throw new IllegalArgumentException("Sku does not exist");
        //
        CartProduct cartProduct = new CartProduct();
        cartProduct.setCartId(currentCart.getId());
        cartProduct.setProductId(productId);
        cartProduct.setQuantity(quantity);
        boolean updateState = cartProductRepository.createOrUpdateCartProduct(cartProduct);
        return updateState;
    }

    //    @CacheEvict(value = "cart", key = "{'cart_session', #username, #session}")
    public boolean deleteProductFromCart(String username, String session, String productSku) throws JsonProcessingException, IllegalAccessException {
        ObjectId userId = customerService.getIdByUsername(username);
        Cart currentCart = getActiveCart(session, userId);
        if (null == currentCart || currentCart.getCartStatus() == CartStatus.EXPIRED) return false;
        ObjectId productIdBySku = productService.getProductIdBySku(productSku);
        return cartProductRepository.deleteCartProduct(currentCart.getId(), productIdBySku);
    }

    @Cacheable(value = "cart", key = "{'active_cart', #userId, #session}",
            unless = "#result == null || #result.cartStatus == T(com.etybeno.ecommerce.data.enums.CartStatus).EXPIRED")
    public Cart getActiveCart(String session, ObjectId userId) throws JsonProcessingException, IllegalAccessException {
        Date today = new Date();
        Cart cart;
        if (StringUtil.isNullOrEmpty(session)) throw new IllegalArgumentException("Session does not exist.");
        else {
            cart = cartRepository.getAllRelatedCart(userId, session);
            if (null != cart) {
                if (null != cart.getUserId() && !cart.getUserId().equals(userId)) {
                    throw new IllegalAccessException(
                            String.format("This cart with session_id=%s does not belong to username=%s", session, userId));
                } else {
                    List<TargetValue> updates = new ArrayList<>();
                    if (null == cart.getUserId() && null != userId) {
                        cart.setUserId(userId);
                        updates.add(new TargetValue("user_id", userId));
                    }
                    if (today.after(cart.getExpireOn())) {
                        cart.setCartStatus(CartStatus.EXPIRED);
                        updates.add(new TargetValue("cart_status", CartStatus.EXPIRED.name()));
                    } else {
                        updates.add(new TargetValue("expire_on",
                                DateTimeUtil.addTime(today, SEVEN_DAYS_IN_SECONDS, Calendar.SECOND)));
                    }
                    updates.add(new TargetValue("modified_date", today));
                    cartRepository.updateCart(cart.getId(), updates);
                }
            }
        }
        //
        return cart;
    }

    @Cacheable(value = "order", key = "{'getCustomerOrderById', #orderId}", unless = "#result == null")
    public CustomerOrder getCustomerOrderById(String orderId) {
        Order order = orderRepository.getOne(Arrays.asList(new TargetValue("_id", orderId)));
        CustomerOrder customerOrder = new CustomerOrder()
                .setOrderId(order.getOrderId())
                .setOrderStatus(order.getOrderStatus())
                .setCreatedDate(order.getCreatedDate())
                .setTotalAmount(order.getTotalAmount())
                .setShippingAddress(order.getShippingAddress())
                .setNote(order.getNote())
                .setUserId(order.getUserId());
        if(order.getUserId() != null) {
            User userById = customerService.getUserById(order.getUserId());
            customerOrder.setDisplayName(userById.getDisplayName());
        }

        List<OrderProduct> orderProducts = orderProductRepository.getAll(Arrays.asList(new TargetValue("order_id", order.getOrderId())));
        if (!orderProducts.isEmpty()) {
            List<CustomerOrderProduct> customerOrderProducts = orderProducts.stream()
                    .map(op -> {
                        CustomerOrderProduct cop = new CustomerOrderProduct()
                                .setOrderProductAsObjectId(op.getId())
                                .setPrice(op.getPrice())
                                .setQuantity(op.getQuantity())
                                .setProductName(op.getProductName())
                                .setProductId(op.getProductId());
                        return cop;
                    }).collect(Collectors.toList());
            StringBuilder sb = new StringBuilder(orderProducts.get(0).getProductName());
            if (orderProducts.size() > 1)
                sb.append(String.format("... và %s sản phẩm khác", StringUtils.leftPad((customerOrderProducts.size() - 1) + "", 2, '0')));
            customerOrder.setShortProductNames(sb.toString());
            customerOrder.setOrderProducts(customerOrderProducts);
        }
        return customerOrder;
    }

    public List<CustomerOrder> getAllOrderByUsername(String username) {
        ObjectId userId = customerService.getIdByUsername(username);
        List<KeyValue> targetValues = Arrays.asList(new TargetValue("user_id", userId));
        List<TargetValue> sorts = Arrays.asList(new TargetValue("created_date", -1));
        List<Document> orderIds = orderRepository.getRawAll(targetValues, -1, -1,
                sorts, OrderRepository.ORDER_ID_PROJECTION);
        List<CustomerOrder> customerOrders = new ArrayList<>();
        for (Document orderId : orderIds) {
            CustomerOrder customerOrder = self.getCustomerOrderById(orderId.getString("_id"));
            customerOrders.add(customerOrder);
        }
        return customerOrders;
    }

    @CacheEvict(value = "cart", key = "{'active_cart', #userId, #session}")
    public Cart createNewCart(ObjectId userId, String session) throws JsonProcessingException {
        Date today = new Date();
        Cart cart = new Cart();
        cart.setUserId(userId);
        cart.setCreatedDate(today);
        cart.setModifiedDate(today);
        cart.setCartStatus(CartStatus.ACTIVE);
        cart.setSessionId(session);
        cart.setExpireOn(DateTimeUtil.addTime(today, SEVEN_DAYS_IN_SECONDS, Calendar.SECOND));
        return cartRepository.createNewCart(cart);
    }

    public String createNewOrder(ObjectId userId, String session, String locale) throws JsonProcessingException, IllegalAccessException {
        CartSession cartSession = getCartSession(userId, session, locale);//TODO: dùng tạm, sẽ optimize sau
        if (cartSession == null || cartSession.getCartProducts() == null || cartSession.getCartProducts().isEmpty())
            throw new IllegalArgumentException("Could not make new order because cart is empty");
        Order order = orderRepository.getOne(Arrays.asList(new TargetValue("cart_id", cartSession.getCartIdAsObjectId())));
        if (null == order) {
            order = new Order();
            Date today = new Date();
            order.setCartId(cartSession.getCartIdAsObjectId());
            order.setUserId(cartSession.getUserId());
            order.setCreatedDate(today);
            order.setModifiedDate(today);
            Integer collect = cartSession.getCartProducts().stream()
                    .map(cp -> cp.getQuantity()).collect(Collectors.summingInt(value -> value));
            order.setProductNum(collect);
            order.setTotalAmount(cartSession.getTotalSum());
            order.setOrderStatus(OrderStatus.AWAITING_FULFILMENT);
            orderRepository.createNewOrder(order);
        }
        return order.getOrderId();
    }

    //    @Cacheable(value = "order", key = "{#root.methodName, #orderId}", unless = "#result != null")
    public Order loadOrderById(String orderId) {
        return orderRepository.loadOrderbyId(orderId);
    }

    /**
     * Process the order, it will get all product in cart to persist to order, and updates
     * cart's status and order'status for further process
     *
     * @param userName
     * @param session
     * @param orderForm
     * @return
     * @throws JsonProcessingException
     * @throws IllegalAccessException
     */
    @CacheEvict(value = "cart", key = "{'active_cart', #userName, #session}", condition = "#result")
    public boolean processOrder(String userName, String session, OrderForm orderForm, String locale) throws JsonProcessingException, IllegalAccessException {
        Order order = self.loadOrderById(orderForm.getOrderId());
        User userByUsername = customerService.getUserByUsername(userName);
        if (null == order)
            throw new NullPointerException("Order with id: " + orderForm.getOrderId() + " does not exist");
        CartSession cartSession = self.getCartSession(userByUsername.getId(), session, locale); //TODO: dùng tạm, sẽ optimize sau
        if (cartSession == null || cartSession.getCartProducts() == null || cartSession.getCartProducts().isEmpty())
            throw new IllegalArgumentException("Could not make new order because cart is empty");
        List<ObjectId> orderProductIds = null;
        boolean result = true;
        try {
            AtomicDouble totalAmount = new AtomicDouble();
            Date today = new Date();
            List<OrderProduct> collect = cartSession.getCartProducts().stream()
                    .map(o -> {
                        OrderProduct orderProduct = new OrderProduct();
                        orderProduct.setOrderId(orderForm.getOrderId());
                        orderProduct.setPrice(o.getProduct().getSalePrice());
                        orderProduct.setProductId(o.getProductIdAsObjectId());
                        orderProduct.setProductName(o.getProduct().getName());
                        orderProduct.setQuantity(o.getQuantity());
                        totalAmount.addAndGet(o.getQuantity() * o.getProduct().getSalePrice());
                        return orderProduct;
                    })
                    .collect(Collectors.toList());
            orderProductIds = orderProductRepository.createNewOrderProducts(collect);
            //
            ShippingAddress shippingAddress = new ShippingAddress();
            shippingAddress.setAddress(orderForm.getAddress());
            shippingAddress.setCity(orderForm.getCity());
            shippingAddress.setPhone(orderForm.getPhone());
            if(userByUsername != null) shippingAddress.setEmail(userByUsername.getEmail());
            //
            for (OrderProduct op : collect) {
                if (!result) break;
                result &= productService.updateProductForOrder(op.getProductId(), op.getQuantity(), locale);
            }
            //
            result &= orderRepository.updateOrder(orderForm.getOrderId(),
                    new TargetValue("order_status", OrderStatus.VERIFICATION_REQUIRED.name()),
                    new TargetValue("shipping_address", shippingAddress),
                    new TargetValue("total_amount", totalAmount.get()),
                    new TargetValue("modified_date", today),
                    new TargetValue("note", orderForm.getNote()));
            //
            result &= cartRepository.updateCart(cartSession.getCartIdAsObjectId(),
                    new TargetValue("modified_date", today),
                    new TargetValue("cart_status", CartStatus.ORDERED.name()));

        } catch (Exception ex) {
            LOGGER.error("Error when process order: " + StringUtil.GSON.toJson(orderForm), ex);
            result = false;
        }
        if (!result) {//rollback
            orderProductRepository.deleteByIds(orderProductIds);
            orderRepository.updateOrder(orderForm.getOrderId(),
                    new TargetValue("order_status", OrderStatus.AWAITING_FULFILMENT.name()));
            cartRepository.updateCart(cartSession.getCartIdAsObjectId(),
                    new TargetValue("cart_status", CartStatus.ACTIVE.name()));
        }
        return true;
    }

    @CacheEvict(value = "order", key = "{'getCustomerOrderById', #customerOrder.orderId}", condition = "#result")
    public boolean updateOrder(CustomerOrder customerOrder) {
        return orderRepository.updateOrder(customerOrder.getOrderId(),
                new TargetValue("order_status", customerOrder.getOrderStatus().name()));
    }

}
