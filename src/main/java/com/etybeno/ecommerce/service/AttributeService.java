package com.etybeno.ecommerce.service;

import com.etybeno.ecommerce.Constant.*;
import com.etybeno.ecommerce.data.entity.I18NTranslation;
import com.etybeno.ecommerce.data.entity.Term;
import com.etybeno.ecommerce.data.entity.TermTaxonomy;
import com.etybeno.ecommerce.data.entity.TermTaxonomyRelationship;
import com.etybeno.ecommerce.data.model.Attribute;
import com.etybeno.ecommerce.repository.mongodb.I18NTranslationRepository;
import com.etybeno.ecommerce.repository.mongodb.TermRepository;
import com.etybeno.ecommerce.repository.mongodb.TermTaxonomyRepository;
import com.etybeno.mongodb.model.TargetValue;
import org.apache.commons.lang3.SerializationUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 04/05/2018.
 */
@Service
public class AttributeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttributeService.class);

    public static final int ATTRIBUTE_TYPE = 1, ORIGINATOR_TYPE = 2;

    @Autowired
    private TermRepository termRepository;
    @Autowired
    private TermTaxonomyRepository termTaxonomyRepository;
    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;
    @Autowired
    private TermTaxonomyRelationshipService termTaxonomyRelationshipService;
    @Resource
    private AttributeService self;

    public ObjectId addAttribute(int attrType, String slug, String name, String description, String locale) {
        String i18nNameType = getI18NTypeNameByAttrType(attrType);
        String i18nDescType = getI18NTypeDescByAttrType(attrType);
        String taxonomy = getTaxonomyByAttrType(attrType);
        Term term = termRepository.insertNewTerm(slug, name, locale, i18nNameType);
        TermTaxonomy termTaxonomy = termTaxonomyRepository.insertNewTermTaxonomy(term.getId(), taxonomy, description, null, locale, i18nDescType);
        return termTaxonomy.getId();
    }

    public Attribute addAttribute(int attrType, Attribute attribute) {
        String i18nNameType = getI18NTypeNameByAttrType(attrType);
        String i18nDescType = getI18NTypeDescByAttrType(attrType);
        String taxonomy = getTaxonomyByAttrType(attrType);
        Term term = termRepository.insertNewTerm(attribute.getSlug(), attribute.getName(), attribute.getLocale(), i18nNameType);
        TermTaxonomy termTaxonomy = termTaxonomyRepository.insertNewTermTaxonomy(term.getId(), taxonomy, attribute.getDescription(),
                attribute.getParentObjectId(), attribute.getLocale(), i18nDescType);
        attribute.setAttributeObjectId(termTaxonomy.getId());
        return attribute;
    }

    @Cacheable(value = "attribute", key = "{#methodName, #slug, #attrType}", unless = "#result == null")
    public Attribute getAttributeBySlug(int attrType, String slug, String locale) {
        String i18nNameType = getI18NTypeNameByAttrType(attrType);
        I18NTranslation nameI18N = i18NTranslationRepository.getModel(slug, i18nNameType, locale);
        if (null == nameI18N) return null;
        String taxonomy = getTaxonomyByAttrType(attrType);
        Document document = termRepository.getByTermI18nIdAndTaxonomy(nameI18N.getObjectId(), taxonomy);
        String description = null;
        if (document.containsKey("i18n_description_id")) {
            I18NTranslation descriptionI18N = i18NTranslationRepository.getModel(document.getObjectId("i18n_description_id"), locale);
            description = descriptionI18N.getText();
        }
        Attribute attribute = new Attribute(document.getObjectId("_id"), nameI18N.getSlug(), nameI18N.getText(), description, locale);
        attribute.setParentObjectId(document.getObjectId("parent_id"));
        attribute.setSelectId(document.getInteger("select_id"));
        return attribute;
    }

    @Cacheable(value = "attribute", key = "{#methodName, #selectId}", unless = "#result == null")
    public Attribute getAttributeBySelectId(int selectId) {
        Document document = termTaxonomyRepository.getByTermTaxonomySelectId(selectId);
        List<I18NTranslation> nameI18N = i18NTranslationRepository.getModels(document.getObjectId("i18n_name_id"));
        Map<String, String> i18nName = new HashMap<>();
        Map<String, String> i18nSlug = new HashMap<>();
        Map<String, String> i18NDescription = new HashMap<>();
        nameI18N.forEach(i18NTranslation -> {
            i18nName.put(i18NTranslation.getLocale(), i18NTranslation.getText());
            i18nSlug.put(i18NTranslation.getLocale(), i18NTranslation.getSlug());
        });
        if (document.containsKey("i18n_description_id")) {
            List<I18NTranslation> descriptionI18N = i18NTranslationRepository.getModels(document.getObjectId("i18n_description_id"));
            descriptionI18N.forEach(i18NTranslation -> i18NDescription.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }

        Attribute attribute = new Attribute(document.getObjectId("_id"), i18nSlug, i18nName, i18NDescription);
        attribute.setParentObjectId(document.getObjectId("parent_id"));
        attribute.setSelectId(document.getInteger("select_id"));
        return attribute;
    }

    @Cacheable(value = "attribute", key = "{#methodName, #id}", unless = "#result == null")
    public Attribute getAttributeById(ObjectId id) {
        Document document = termTaxonomyRepository.getByTermTaxonomyId(id);
        List<I18NTranslation> nameI18N = i18NTranslationRepository.getModels(document.getObjectId("i18n_name_id"));
        Map<String, String> i18nName = new HashMap<>();
        Map<String, String> i18nSlug = new HashMap<>();
        Map<String, String> i18NDescription = new HashMap<>();
        nameI18N.forEach(i18NTranslation -> {
            i18nName.put(i18NTranslation.getLocale(), i18NTranslation.getText());
            i18nSlug.put(i18NTranslation.getLocale(), i18NTranslation.getSlug());
        });
        if (document.containsKey("i18n_description_id")) {
            List<I18NTranslation> descriptionI18N = i18NTranslationRepository.getModels(document.getObjectId("i18n_description_id"));
            descriptionI18N.forEach(i18NTranslation -> i18NDescription.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }

        Attribute attribute = new Attribute(document.getObjectId("_id"), i18nSlug, i18nName, i18NDescription);
        attribute.setParentObjectId(document.getObjectId("parent_id"));
        attribute.setSelectId(document.getInteger("select_id"));
        return attribute;
    }


    @Cacheable(value = "attribute", key = "{#methodName, #id, #relationType}", unless = "#result == null")
    public Set<Attribute> getAttributesBelongToIdAndType(ObjectId id, String relationType) {
        TermTaxonomyRelationship relationship = termTaxonomyRelationshipService.getRelationship(id, relationType);
        if (null != relationship && !relationship.getRelationalTTIds().isEmpty()) {
            Set<Attribute> collect = relationship.getRelationalTTIds().stream()
                    .map(childId -> self.getCloneAttribute(childId))
                    .collect(Collectors.toSet());
            return collect;
        } else return null;
    }

    public Map<String, Set<Attribute>> getAttributesBelongToIdAndPrefix(ObjectId id, String prefix) {
        List<TermTaxonomyRelationship> relationships = termTaxonomyRelationshipService.getRelationships(id, prefix);
        if (null != relationships && !relationships.isEmpty()) {
            Map<String, Set<Attribute>> rs = new HashMap<>();
            for (TermTaxonomyRelationship relationship : relationships) {
                Set<Attribute> collect = relationship.getRelationalTTIds().stream()
                        .map(childId -> self.getCloneAttribute(childId))
                        .collect(Collectors.toSet());
                rs.put(relationship.getType(), collect);
            }
            return rs;
        } else return null;
    }

    public Set<Attribute> getChildrenById(int attrType, ObjectId cateId) {
        String termRel = getTermRelByAttrType(attrType);
        TermTaxonomyRelationship values = termTaxonomyRelationshipService.getRelationship(cateId, termRel);
        Set<Attribute> rs = new HashSet<>();
        if (null != values && null != values.getRelationalTTIds() && !values.getRelationalTTIds().isEmpty())
            for (ObjectId value : values.getRelationalTTIds()) {
                Set<Attribute> childrenByIds = self.getChildrenById(attrType, value);
                Attribute thisAttr = self.getCloneAttribute(value);
                if (!childrenByIds.isEmpty()) thisAttr.setChildren(childrenByIds);
                rs.add(thisAttr);
            }
        return rs;
    }

    public Set<Attribute> getAllAttributesByType(int attrType, String locale) {
        String taxonomy = getTaxonomyByAttrType(attrType);
        List<Document> documents = termTaxonomyRepository.getRawAll(
                Arrays.asList(new TargetValue("taxonomy", taxonomy), new TargetValue("parent_id", null)),
                -1, -1, null, -1);
        Set<Attribute> rs = new HashSet<>();
        for (Document doc : documents) {
            ObjectId keyId = doc.getObjectId("_id");
            Attribute attribute = self.getCloneAttribute(keyId);
            rs.add(attribute);
            Set<Attribute> children = self.getChildrenById(attrType, attribute.getAttributeObjectId());
            attribute.setChildren(children);
        }
        return rs;
    }

    public Attribute getCloneAttribute(ObjectId id) {
        Attribute attribute = self.getAttributeById(id);
        return SerializationUtils.clone(attribute);
    }

    public String getTermRelByAttrType(int attrType) {
        switch (attrType) {
            case ATTRIBUTE_TYPE:
                return TTRelationship.ATTRIBUTE_CHILDREN;
            case ORIGINATOR_TYPE:
                return TTRelationship.ORIGINATOR_CHILDREN;
            default:
                return null;
        }
    }

    public String getTaxonomyByAttrType(int attrType) {
        switch (attrType) {
            case ATTRIBUTE_TYPE:
                return Taxonomy.PRODUCT_ATTRIBUTE;
            case ORIGINATOR_TYPE:
                return Taxonomy.PRODUCT_ORIGINATOR;
            default:
                return null;
        }
    }

    private String getI18NTypeNameByAttrType(int attrType) {
        switch (attrType) {
            case ATTRIBUTE_TYPE:
                return I18NType.ATTRIBUTE_NAME;
            case ORIGINATOR_TYPE:
                return I18NType.ORIGINATOR_NAME;
            default:
                return null;
        }
    }

    private String getI18NTypeDescByAttrType(int attrType) {
        switch (attrType) {
            case ATTRIBUTE_TYPE:
                return I18NType.ATTRIBUTE_DESCRIPTION;
            case ORIGINATOR_TYPE:
                return I18NType.ORIGINATOR_DESCRIPTION;
            default:
                return null;
        }
    }
}
