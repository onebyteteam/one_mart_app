package com.etybeno.ecommerce.service;

import com.etybeno.common.enums.Granularity;
import com.etybeno.common.model.IntervalModel;
import com.etybeno.common.util.DateTimeUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.entity.TermTaxonomy;
import com.etybeno.ecommerce.data.enums.AccountType;
import com.etybeno.ecommerce.data.enums.OrderStatus;
import com.etybeno.ecommerce.data.model.*;
import com.etybeno.ecommerce.repository.mongodb.*;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.etybeno.ecommerce.repository.mongodb.TermTaxonomyRepository.CATEGORY_PROJECTION;

/**
 * Created by thangpham on 05/04/2018.
 */
@Service
public class DataMonitorService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private LoginDataRepository loginDataRepository;
    @Autowired
    private TermTaxonomyRepository termTaxonomyRepository;
    @Autowired
    private AbstractProductRepository abstractProductRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;

    public long countTotalOrder() {
        return orderRepository.count(null);
    }

    public long countTotalSale() {
        return orderRepository.count(Arrays.asList(new TargetValue("order_status", OrderStatus.COMPLETED.name())));
    }

    public long countTotalCustomer() {
        return accountRepository.count(Arrays.asList(new TargetValue("account_type", AccountType.CUSTOMER.name())));
    }

    public long countTotalProduct() throws NoSuchAlgorithmException {
        return abstractProductRepository.countAll(null, null);
    }

    public List<Category> getAllCategory() throws JsonProcessingException {
        List<TermTaxonomy> all = termTaxonomyRepository.getAll(Arrays.asList(new TargetValue("taxonomy", Constant.Taxonomy.PRODUCT_CATEGORY)));
        List<Category> categories = all.stream()
                .map(tt -> categoryService.getCloneCategory(tt.getId()))
                .collect(Collectors.toList());
        return categories;
    }

    public List<OrderReport> getOrderList(int from, int size) {
        List<OrderReport> allByTarget = orderRepository.getAll(null, from, size, buildSorts("modified_date"),
                OrderRepository.ORDER_REPORT_PROJECTION, OrderReport.class);
        return allByTarget;
    }

    public List<Product> getProductList(String locale, int from, int size) {
        List<Document> rawAll = abstractProductRepository.getRawAll(null, from, size,
                Arrays.asList(new TargetValue("modified_date", -1)), -1);
        List<Product> products = rawAll.stream()
                .map(raw -> productService.getProductById(raw.getObjectId("_id"), locale))
                .collect(Collectors.toList());
        return products;
    }

    public List<Seller> getSellerList(int from, int size) {
        List<Seller> allByTarget = supplierRepository.getAll(null, from, size, buildSorts("modified_date"),
                SupplierRepository.SELLER_LIST_PROJECTION, Seller.class);
        return allByTarget;
    }

    public List<AccountReport> getAccountList(int from, int size) {
        return accountRepository.getAccountReportData(AccountRepository.ACCOUNT_REPORT_PROJECTION, AccountReport.class);
    }

    public Map<String, Map<String, Object>> getSaleData(String quickRange) throws ParseException {
        Date current = new Date();
        switch (quickRange.toUpperCase()) {
            case DateTimeUtil.TODAY:
                return getSaleData(Granularity.HOUR, DateTimeUtil.truncateDateTime(current, Calendar.DATE), current);
            case DateTimeUtil.YESTERDAY:
                return getSaleData(Granularity.HOUR, DateTimeUtil.truncateDateTime(
                        DateTimeUtil.addTime(current, -1, Calendar.DATE), Calendar.DATE), current);
            case DateTimeUtil.LAST_24HOURS:
                return getSaleData(Granularity.HOUR, DateTimeUtil.addTime(current, -24, Calendar.HOUR_OF_DAY), current);
            case DateTimeUtil.LAST_72HOURS:
                return getSaleData(Granularity.HOUR, DateTimeUtil.addTime(current, -72, Calendar.HOUR_OF_DAY), current);
            case DateTimeUtil.LAST_3DAYS:
                return getSaleData(Granularity.DAY, DateTimeUtil.addTime(current, -3, Calendar.DATE), current);
            case DateTimeUtil.LAST_7DAYS:
                return getSaleData(Granularity.DAY, DateTimeUtil.addTime(current, -7, Calendar.DATE), current);
            case DateTimeUtil.LAST_14DAYS:
                return getSaleData(Granularity.DAY, DateTimeUtil.addTime(current, -14, Calendar.DATE), current);
            case DateTimeUtil.LAST_30DAYS:
                return getSaleData(Granularity.DAY, DateTimeUtil.addTime(current, -30, Calendar.DATE), current);
            default:
                throw new IllegalArgumentException("Cannot process report with getSaleData with quickRange " + quickRange);
        }
    }

    /**
     * Calculate data for sale report, which uses granularity as timeseries groupby and duration. Can accept
     * format hh/dd/mm/yyyy-hh/dd/mm/yyyy or dd/mm/yyyy-dd/mm/yyyy
     *
     * @param granularity
     * @param duration
     * @return
     * @throws ParseException
     */
    public Map<String, Map<String, Object>> getSaleData(String granularity, String duration) throws ParseException {
        String[] split = duration.split("-");
        Granularity gran = Granularity.valueOf(granularity);
        switch (gran) {
            case HOUR:
                return getSaleData(gran, DateTimeUtil.parseDate(split[0].trim(), DateTimeUtil.HHDDMMYYYY_SPLASH),
                        DateTimeUtil.parseDate(split[1].trim(), DateTimeUtil.HHDDMMYYYY_SPLASH));
            case DAY:
                return getSaleData(gran, DateTimeUtil.parseDate(split[0].trim(), DateTimeUtil.DDMMYYYY_SPLASH),
                        DateTimeUtil.parseDate(split[1].trim(), DateTimeUtil.DDMMYYYY_SPLASH));
            default:
                throw new UnsupportedOperationException(String.format("The granularity %s is not supported", gran.name()));
        }
    }

    public Map<String, Map<String, Object>> getSaleData(Granularity granularity, Date from, Date to) throws ParseException {
        String mongoFormat; IntervalModel itv;
        switch (granularity) {
            case HOUR:
                mongoFormat = "%Y-%m-%d %H:%M:%s";
                itv = IntervalModel.getInstance(DateTimeUtil.truncateDateTime(from, Calendar.HOUR_OF_DAY),
                        DateTimeUtil.stretchDateTime(to, Calendar.HOUR_OF_DAY), "yyyy-MM-dd HH:mm:ss", Calendar.HOUR_OF_DAY);
                break;
            case DAY:
                mongoFormat = "%Y-%m-%d";
                itv = IntervalModel.getInstance(DateTimeUtil.truncateDateTime(from, Calendar.DATE),
                        DateTimeUtil.stretchDateTime(to, Calendar.DATE), "yyyy-MM-dd", Calendar.DATE);
                break;
            default:
                throw new UnsupportedOperationException(String.format("The granularity %s is not supported", granularity.name()));
        }
        Map<String, Map<String, Object>> revenueReport = orderRepository.getOrderReport(mongoFormat, itv.getFromDateTime(),
                itv.getToDateTime(), Arrays.asList("revenue", "order", "sale"));
        Date next;
        while ((next = itv.next()) != null) {
            String key = DateTimeUtil.formatDate(next, itv.getFormat());
            revenueReport.forEach((s1, valueMap) -> {
                if(valueMap.containsKey(key)) valueMap.put(key, valueMap.get(key));
                else valueMap.put(key, 0);
            });
        }
        return revenueReport;
    }

    private Map<String, Object> buildSorts(String... fields) {
        Map<String, Object> sorts = new HashMap<>();
        Stream.of(fields).forEach(s -> sorts.put(s, 1));
        return sorts;
    }

}
