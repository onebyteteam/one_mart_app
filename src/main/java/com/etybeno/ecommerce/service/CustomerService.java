package com.etybeno.ecommerce.service;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.Account;
import com.etybeno.ecommerce.data.entity.LoginData;
import com.etybeno.ecommerce.data.entity.User;
import com.etybeno.ecommerce.data.enums.AccountType;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.data.form.RegistryForm;
import com.etybeno.ecommerce.data.form.UserInfoForm;
import com.etybeno.ecommerce.data.model.Authorization;
import com.etybeno.ecommerce.data.model.Customer;
import com.etybeno.ecommerce.repository.mongodb.AccountRepository;
import com.etybeno.ecommerce.repository.mongodb.LoginDataRepository;
import com.etybeno.ecommerce.repository.mongodb.UserRepository;
import com.etybeno.ecommerce.util.ConvenientUtil;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.etybeno.mongodb.model.TargetValues;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 20/01/2018.
 */
@Service
public class CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LoginDataRepository loginDataRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Resource
    private CustomerService self;

    public User getUserById(ObjectId userId) {
        return userRepository.getOne(Arrays.asList(new TargetValue("_id", userId)));
    }

    public User getUserByUsername(String username) {
        return userRepository.getOne(Arrays.asList(new TargetValue("username", username)));
    }

    @Cacheable(value = "user", key = "{#root.methodName, #loginType, #identification}", unless = "#result == null || #result.empty")
    public List<String> getUsername(LoginType loginType, String... identifications) {
        List<KeyValue> targets = Arrays.asList(
                new TargetValues("identification", Arrays.asList(identifications)),
                new TargetValue("login_type", loginType.name()));
        List<User> users = loginDataRepository.getUserdetailByTargets(targets,
                LoginDataRepository.USERNAME_PROJECTION, User.class);
        return users.stream().map(u -> u.getUsername()).collect(Collectors.toList());
    }

    @Cacheable(value = "user", key = "{#root.methodName, #username}", unless = "#result == null")
    public ObjectId getIdByUsername(String username) {
        if (StringUtil.isNullOrEmpty(username)) return null;
        return userRepository.getIdByUsername(username);
    }

    public boolean updateUserInfoByUsername(String username, UserInfoForm form) throws JsonProcessingException {
        ObjectId id = getIdByUsername(username);
        if (null == id) return false;
        boolean rs = false;
        if (form.getChangePassword()) {
            boolean checkLoginData = loginDataRepository.checkLoginData(id, LoginType.REGISTRY);
            String encryptPassword = bCryptPasswordEncoder.encode(form.getNewPassword());
            if (checkLoginData) {
                Map<String, Object> values = new HashMap<>();
                values.put("login_type", LoginType.REGISTRY.name());
                values.put("validation", encryptPassword);
                rs |= loginDataRepository.updateLoginData(id, values);
            } else {
                LoginData loginData = new LoginData();
                loginData.setUserId(id);
                loginData.setLoginType(LoginType.REGISTRY);
                String identification;
                if(form.getUsername().contains("@")) identification = form.getUsername().split("@")[0];
                else identification = form.getUsername();
                loginData.setIdentification(identification);
                loginData.setValidation(encryptPassword);
                rs |= loginDataRepository.createNewLoginData(loginData).getId() != null;
            }
        }
        Map<String, Object> values = new HashMap<>();
        values.put("first_name", form.getFirstName());
        values.put("last_name", form.getLastName());
        values.put("display_name", form.getDisplayName());
        rs |= userRepository.updateUser(id, values);
        return rs;
    }

    public boolean updateLoginDataByUsername(String username, Map<String, Object> values) {
        ObjectId id = getIdByUsername(username);
        if (null == id) return false;
        return loginDataRepository.updateLoginData(id, values);
    }

    public boolean checkCustomerByUsername(String value) {
        return userRepository.checkTargetExist("username", value);
    }

    public boolean checkCustomerByEmail(String value) {
        return userRepository.checkTargetExist("email", value);
    }

    public Customer getCustomerByUsername(String username, LoginType loginType) {
        Customer customer = userRepository.getUserInformationByUsername(username, loginType,
                UserRepository.SIMPLE_CUSTOMER_PROJECTION, Customer.class);
        //
        if (null == customer) return null;
        for (Authorization auth : customer.getAuthorizations()) {
            Set<String> collect = auth.getPermissionIds().stream()
                    .map(id -> permissionService.getPermission(id).getName())
                    .collect(Collectors.toSet());
            auth.setPermissions(collect);
        }
        //
        return customer;
    }

    /********************** Login data section *****************/

    public List<Document> getAllLoginData(String username) {
        return userRepository.getAllLoginData(username, UserRepository.SOCIAL_DATA_PROJECTION, Document.class);
    }

    public List<Document> getAllLoginData(String username, LoginType loginType) {
        return userRepository.getAllLoginData(username, loginType, UserRepository.SOCIAL_DATA_PROJECTION, Document.class);
    }

    /********************* Account data section ***************/
    public boolean createNewSocialCustomer(String username, Connection<?> connection) {
        User user = new User();
        LoginData loginData = new LoginData();
        Account account = new Account();
        Date today = new Date();
        try {
            ConnectionData data = connection.createData();
            UserProfile userProfile = connection.fetchUserProfile();
            user.setFirstName(userProfile.getFirstName());
            user.setLastName(userProfile.getLastName());
            user.setUsername(username);
            user.setEmail(userProfile.getEmail());
            user.setAvatarUri(data.getImageUrl());
            user.setModifiedDate(today);
            user.setCreatedDate(today);
            user.setDisplayName(data.getDisplayName());
            userRepository.createUser(user);

            loginData.setUserId(user.getId());
            loginData.setIdentification(data.getProviderUserId());
            loginData.setValidation(data.getAccessToken());
            loginData.setLoginType(ConvenientUtil.parseProvider(data.getProviderId()));
            loginData.setImageUrl(data.getImageUrl());
            loginData.setRefreshToken(data.getRefreshToken());
            loginData.setProfileUrl(data.getProfileUrl());
            loginData.setExpireTime(data.getExpireTime());
            loginData.setCreatedDate(today);
            loginData.setModifiedDate(today);
            loginData.setRank(1);
            loginDataRepository.createNewLoginData(loginData);

            account.setAccountType(AccountType.CUSTOMER);
            account.setActive(Boolean.TRUE);
            account.setUserId(user.getId());
            account.setCreatedDate(today);
            account.setModifiedDate(today);
            account.setAuthorizations(Arrays.asList(
                    permissionService.getPermission("normal", AccountType.CUSTOMER).getId()));
            accountRepository.createNewAccount(account);
        } catch (Exception ex) {
            LOGGER.error("Could not create new social customer", ex);
            if (null != user && user.getId() != null) userRepository.deleteUser(user.getId());
            if (null != account && account.getId() != null) accountRepository.deleteAccount(account.getId());
            if (null != loginData && loginData.getId() != null) loginDataRepository.deleteLoginData(loginData.getId());
            return false;
        }
        return true;
    }

    public boolean createNewRegistryCustomer(RegistryForm registryForm, LoginType loginType) {
        User user = new User();
        LoginData loginData = new LoginData();
        Account account = new Account();
        Date today = new Date();
        try {
            user.setFirstName(registryForm.getFirstName());
            user.setLastName(registryForm.getLastName());
            user.setUsername(registryForm.getUsername());
            user.setEmail(registryForm.getEmail());
            user.setModifiedDate(today);
            user.setCreatedDate(today);
            String displayName = ((registryForm.getFirstName() == null ? "" : registryForm.getFirstName()) +
                    " " + (registryForm.getLastName() == null ? "" : registryForm.getLastName())).trim();
            user.setDisplayName(displayName);
            userRepository.createUser(user);

            loginData.setIdentification(registryForm.getIdentification());
            loginData.setValidation(loginType == LoginType.REGISTRY ?
                    bCryptPasswordEncoder.encode(registryForm.getValidation()) :
                    registryForm.getValidation());
            loginData.setLoginType(loginType);
            loginData.setCreatedDate(today);
            loginData.setModifiedDate(today);
            loginData.setUserId(user.getId());
            loginDataRepository.createNewLoginData(loginData);

            account.setAccountType(AccountType.CUSTOMER);
            account.setActive(Boolean.TRUE);
            account.setUserId(user.getId());
            account.setCreatedDate(today);
            account.setModifiedDate(today);
            account.setAuthorizations(Arrays.asList(
                    permissionService.getPermission("normal", AccountType.CUSTOMER).getId()));
            accountRepository.createNewAccount(account);
            return true;
        } catch (Exception ex) {
            LOGGER.error("Could not create new customer", ex);
            if (null != user && user.getId() != null) userRepository.deleteUser(user.getId());
            if (null != loginData && loginData.getId() != null) loginDataRepository.deleteLoginData(loginData.getId());
            if (null != account && account.getId() != null) accountRepository.deleteAccount(account.getId());
            return false;
        }
    }

}
