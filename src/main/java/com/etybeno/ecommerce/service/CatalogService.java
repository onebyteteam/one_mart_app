package com.etybeno.ecommerce.service;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.entity.AbstractCollection;
import com.etybeno.ecommerce.data.enums.CollectionVisibility;
import com.etybeno.ecommerce.data.model.Catalog;
import com.etybeno.ecommerce.data.model.Product;
import com.etybeno.ecommerce.repository.mongodb.AbstractCollectionRepository;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.etybeno.ecommerce.repository.mongodb.AbstractCollectionRepository.CATALOG_DETAIL_PROJECTION;
import static com.etybeno.ecommerce.repository.mongodb.AbstractCollectionRepository.CATALOG_LIST_PROJECTION;

/**
 * Created by thangpham on 16/02/2018.
 */
@Service
public class CatalogService {

    @Autowired
    private ProductService productService;
    @Autowired
    private AbstractCollectionRepository abstractCollectionRepository;
    @Resource
    private CatalogService self;

    public Catalog createNewCatalog(Catalog catalog) {
        AbstractCollection collection = new AbstractCollection();
        collection.setName(catalog.getName());
        collection.setDescription(catalog.getDescription());
        collection.setCode(catalog.getCode());
        collection.setSlug(StringUtil.isNullOrEmpty(catalog.getSlug()) ?
                StringUtil.stripAccents(catalog.getName().trim()).replaceAll(" ", "-") :
                catalog.getSlug());
        collection.setProductIds(catalog.getProductsAsObjectId());
        collection.setCollectionVisibility(catalog.getVisible());
        collection.setGalleryImageIds(catalog.getImages());
        abstractCollectionRepository.createNewAbtractCollection(collection);
        catalog.setCatalogId(collection.getId());
        return catalog;
    }

    public long countAllCatalog() {
        return self.countCatalog(0l, null);
    }

    @Cacheable(value = "catalog", key = "hashId")
    protected long countCatalog(long hashId, List<TargetValues> targets) {
        return abstractCollectionRepository.countAll(targets);
    }

    public List<Catalog> getAllCatalog(int from, int size) {
        List<KeyValue> targets = Arrays.asList(new TargetValues("collection_visibility",Arrays.asList(CollectionVisibility.VISIBLE.name())
        ));
//        Authentication authentication = contextsHolder.getAuthentication();
//        if(authentication.isAuthenticated()) {
//
//        }
        return abstractCollectionRepository.getAllCollection(targets, from, size, CATALOG_LIST_PROJECTION, Catalog.class);
    }

    public Catalog getCatalogByCode(String code, String locale, int from, int size) {
        Catalog catalog = abstractCollectionRepository.getCollectionByCode(code, CATALOG_DETAIL_PROJECTION, Catalog.class);
        if (null == catalog) return null;
        int productsize = catalog.getProductsAsObjectId().size();
        if (from < productsize) {
            int to = (from + size) >= productsize ? productsize : from + size;
            List<Product> products = catalog.getProductsAsObjectId().subList(from, to).parallelStream()
                    .map(objectId -> productService.getProductById(objectId, locale))
                    .collect(Collectors.toList());
            catalog.setProducts(products);
        }
        return catalog;
    }

}
