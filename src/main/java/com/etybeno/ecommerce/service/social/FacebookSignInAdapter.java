package com.etybeno.ecommerce.service.social;

import com.etybeno.common.util.StringUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Arrays;

/**
 * Created by thangpham on 24/02/2018.
 */
public class FacebookSignInAdapter implements SignInAdapter {

    @Override
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
        UsernamePasswordAuthenticationToken facebookUser =
                new UsernamePasswordAuthenticationToken(connection.getDisplayName(), connection.createData().getAccessToken(),
                        Arrays.asList(new SimpleGrantedAuthority("FACEBOOK_USER")));
        SecurityContextHolder.getContext().setAuthentication(facebookUser);
        UserProfile userProfile = connection.fetchUserProfile();
        ConnectionData data = connection.createData();
        return connection.getDisplayName();
    }
}
