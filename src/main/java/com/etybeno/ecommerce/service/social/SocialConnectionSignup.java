package com.etybeno.ecommerce.service.social;

import com.etybeno.ecommerce.service.CustomerService;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by thangpham on 05/02/2018.
 */
@Service
public class SocialConnectionSignup implements ConnectionSignUp {

    private static final Logger LOGGER = LoggerFactory.getLogger(SocialConnectionSignup.class);

    @Resource
    private CustomerService customerService;

    @Override
    public String execute(Connection<?> connection) {
        ConnectionData data = connection.createData();
        String username = data.getProviderUserId() + "@" + data.getProviderId();
        while(customerService.checkCustomerByUsername(username)) {
            username = username + RandomStringUtils.randomAlphanumeric(2);
        }
        return username;
    }

}
