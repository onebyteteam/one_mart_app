package com.etybeno.ecommerce.service.social;

import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.repository.mongodb.LoginDataRepository;
import com.etybeno.ecommerce.service.CustomerService;
import com.etybeno.ecommerce.util.ConvenientUtil;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 25/02/2018.
 */
@Service
public class SocialService {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private LoginDataRepository loginDataRepository;

    public List<String> getUsername(LoginType loginType, Set<String> identification) {
        return customerService.getUsername(loginType, identification.toArray(new String[identification.size()]));
    }

    public List<String> getUsername(LoginType loginType, List<String> identification) {
        return customerService.getUsername(loginType, identification.toArray(new String[identification.size()]));
    }

    public List<ConnectionData> findAllConnection(String username) {
        List<Document> allLoginData = customerService.getAllLoginData(username);
        return allLoginData.stream()
                .map(o -> parseToConnectionData(o))
                .collect(Collectors.toList());
    }

    public List<ConnectionData> findAllConnection(String username, LoginType loginType) {
        List<Document> allLoginData = customerService.getAllLoginData(username, loginType);
        return allLoginData.stream()
                .map(o -> parseToConnectionData(o))
                .collect(Collectors.toList());
    }


    public ConnectionData findSingleConnection(String username, LoginType loginType, String indentification) {
        return null;
    }

    private Bson buildSocialDataProjection() {
        return Projections.fields(Projections.include("username", "identification", "first_name", "last_name",
                "profile_url", "avatar_uri", "login.login_type", "login.validation", "login.refresh_token", "login.expire_time"));
    }

    public boolean insertConnectionData(String username, Connection<?> connection) throws Exception {
        return customerService.createNewSocialCustomer(username, connection);
    }

    public boolean updateConnectionData(String username, ConnectionData connectionData) {
        Map<String, Object> setValues = new HashMap<>();
        setValues.put("profile_url", connectionData.getProfileUrl());
        setValues.put("avatar_uri", connectionData.getImageUrl());
        setValues.put("validation", connectionData.getAccessToken());
        setValues.put("refresh_token", connectionData.getRefreshToken());
        setValues.put("expire_time", connectionData.getExpireTime());
        LoginType loginType = ConvenientUtil.parseProvider(connectionData.getProviderId());
        setValues.put("login_type", loginType.name());
        return customerService.updateLoginDataByUsername(username, setValues);
    }

    public boolean deleteConnection(String username, LoginType loginType) {
        ObjectId id = customerService.getIdByUsername(username);
        return loginDataRepository.deleteLoginData(id, loginType);
    }

    public boolean deleteConnection(String username, LoginType loginType, String identification) {
        ObjectId id = customerService.getIdByUsername(username);
        return loginDataRepository.deleteLoginData(id, loginType, identification);
    }

    private ConnectionData parseToConnectionData(Document o) {
        LoginType loginType = LoginType.valueOf(o.getString("login_type"));
        String providerId = ConvenientUtil.loginTypeToSocialProvider(loginType);
        ConnectionData connectionData = new ConnectionData(providerId, o.getString("identification"),
                (o.getString("first_name") + o.getString("last_name")),
                o.getString("profile_url"), o.getString("avatar_uri"), o.getString("validation"),
                null, o.getString("refresh_token"), o.getLong("expire_time"));
        return connectionData;
    }


//    private final class ServiceProviderConnectionMapper implements RowMapper<Connection<?>> {
//
//        public Connection<?> mapRow(ResultSet rs, int rowNum) throws SQLException {
//            ConnectionData connectionData = mapConnectionData(rs);
//            ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory(connectionData.getProviderId());
//            return connectionFactory.createConnection(connectionData);
//        }
//
//        private ConnectionData mapConnectionData(ResultSet rs) throws SQLException {
//            return new ConnectionData(rs.getString("providerId"), rs.getString("providerUserId"), rs.getString("displayName"), rs.getString("profileUrl"), rs.getString("imageUrl"),
//                    decrypt(rs.getString("accessToken")), decrypt(rs.getString("secret")), decrypt(rs.getString("refreshToken")), expireTime(rs.getLong("expireTime")));
//        }
//
//        private String decrypt(String encryptedText) {
//            return encryptedText != null ? textEncryptor.decrypt(encryptedText) : encryptedText;
//        }
//
//        private Long expireTime(long expireTime) {
//            return expireTime == 0 ? null : expireTime;
//        }
//
//    }
}