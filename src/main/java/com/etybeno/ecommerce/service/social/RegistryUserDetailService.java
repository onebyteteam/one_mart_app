package com.etybeno.ecommerce.service.social;

import com.etybeno.ecommerce.data.custom.EcommerceUserDetails;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.data.model.Authentication;
import com.etybeno.ecommerce.data.model.Authorization;
import com.etybeno.ecommerce.data.model.Customer;
import com.etybeno.ecommerce.service.CustomerService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 05/02/2018.
 */
@Component
public class RegistryUserDetailService implements UserDetailsService, SocialUserDetailsService {

    @Autowired
    private CustomerService customerService;

    /**
     * This method is for registration flow. It will load user data by username
     * @param userName
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Customer customer = customerService.getCustomerByUsername(userName, LoginType.REGISTRY);
        if(null == customer) throw new UsernameNotFoundException("");
        Set<SimpleGrantedAuthority> authorizations = buildAuthorizations(customer);
        Authentication authentication = customer.getAuthentications().get(0);
        return new EcommerceUserDetails(customer.getDisplayName(), customer.getUsername(), authentication.getValidation(),
                customer.getAvatarUri(), authorizations);
    }

    /**
     * This method is for social registration flow. It will load user data by username
     * @param userId
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        Customer customer = customerService.getCustomerByUsername(userId, null);
        if(null == customer) throw new UsernameNotFoundException("");
        Set<SimpleGrantedAuthority> authorizations = buildAuthorizations(customer);
        return new EcommerceUserDetails(customer.getDisplayName(), customer.getUsername(), RandomStringUtils.randomAlphanumeric(8),
                customer.getAvatarUri(), authorizations);
    }

    //internal helper
    private Set<SimpleGrantedAuthority> buildAuthorizations(Customer customer) {
        Set<SimpleGrantedAuthority> authorizations = new HashSet<>();
        if(null != customer && null != customer.getAuthentications() && !customer.getAuthentications().isEmpty()) {
            for (Authorization auth : customer.getAuthorizations()) {
                Set<SimpleGrantedAuthority> permissions = auth.getPermissions().stream()
                        .map(s -> new SimpleGrantedAuthority(auth.getRole().name() + "@" + s))
                        .collect(Collectors.toSet());
                authorizations.addAll(permissions);
            }
        }
        return authorizations;
    }
}
