package com.etybeno.ecommerce.service;

import com.etybeno.ecommerce.data.entity.Permission;
import com.etybeno.ecommerce.data.enums.AccountType;
import com.etybeno.ecommerce.repository.mongodb.PermissionRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by thangpham on 20/02/2018.
 */
@Service
public class PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    public boolean createNewPermission(Permission permission) throws JsonProcessingException {
        permissionRepository.createPermission(permission);
        return true;
    }

    @Cacheable(cacheNames = "permission", key = "{#root.methodName, #name, #accountType}")
    public Permission getPermission(String name, AccountType accountType) {
        Permission permission = permissionRepository.getPermission(name, accountType);
        return permission;
    }

    @Cacheable(cacheNames = "permission", key = "{#root.methodName, #id}")
    public Permission getPermission(int id) {
        return permissionRepository.getById(id);
    }
}
