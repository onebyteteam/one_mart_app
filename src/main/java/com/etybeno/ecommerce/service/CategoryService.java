package com.etybeno.ecommerce.service;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.Constant.Taxonomy;
import com.etybeno.ecommerce.data.entity.I18NTranslation;
import com.etybeno.ecommerce.data.entity.Term;
import com.etybeno.ecommerce.data.entity.TermTaxonomy;
import com.etybeno.ecommerce.data.entity.TermTaxonomyRelationship;
import com.etybeno.ecommerce.data.form.CategoryForm;
import com.etybeno.ecommerce.data.model.Attribute;
import com.etybeno.ecommerce.data.model.Category;
import com.etybeno.ecommerce.repository.mongodb.I18NTranslationRepository;
import com.etybeno.ecommerce.repository.mongodb.TermRepository;
import com.etybeno.ecommerce.repository.mongodb.TermTaxonomyRepository;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.SerializationUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static com.etybeno.ecommerce.repository.mongodb.TermTaxonomyRepository.CATEGORY_PROJECTION;

/**
 * Created by thangpham on 25/12/2017.
 */
@Service
public class CategoryService {

    @Autowired
    private TermRepository termRepository;
    @Autowired
    private TermTaxonomyRepository termTaxonomyRepository;
    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;
    @Autowired
    private AttributeService attributeService;
    @Autowired
    private TermTaxonomyRelationshipService termTaxonomyRelationshipService;
    @Resource
    private CategoryService self;

    public boolean createOrUpdateCategory(String locale, CategoryForm categoryForm) {
        if (StringUtil.isNullOrEmpty(categoryForm.getCategoryId())) {
            Category newCategory = self.createNewCategory(locale, categoryForm.getName(), categoryForm.getSlug(),
                    categoryForm.getDescription(), categoryForm.getParentId());
            return true;
        } else {
            Document document = termTaxonomyRepository.getByTermTaxonomyId(new ObjectId(categoryForm.getCategoryId()));
            I18NTranslation nameI18N = i18NTranslationRepository.getModel(document.getObjectId("i18n_name_id"), locale);
            I18NTranslation descriptionI18N = document.containsKey("i18n_description_id") ?
                    i18NTranslationRepository.getModel(document.getObjectId("i18n_description_id"), locale) : null;
            boolean isUpdated = false;
            if (!nameI18N.getText().equals(categoryForm.getName()) || !nameI18N.getSlug().equals(categoryForm.getName())) {
                i18NTranslationRepository.update(nameI18N.getId(), categoryForm.getSlug(), categoryForm.getName());
                isUpdated = true;
            }
            if ((descriptionI18N == null && StringUtil.isNullOrEmpty(categoryForm.getDescription())) ||
                    !descriptionI18N.getText().equals(categoryForm.getDescription())) {
                i18NTranslationRepository.update(descriptionI18N.getId(), null, categoryForm.getDescription());
                isUpdated = true;
            }
            if ((!document.containsKey("parent_id") && !StringUtil.isNullOrEmpty(categoryForm.getParentId())) ||
                    (!document.getObjectId("parent_id").toHexString().equals(categoryForm.getParentId()))) {
                termTaxonomyRepository.updateTermTaxonomy(document.getObjectId("_id"), Arrays.asList(
                        new TargetValue("parent_id", new ObjectId(categoryForm.getParentId()))));
                isUpdated = true;
            }
            return isUpdated;
        }
    }


    public Category createNewCategory(String locale, Category category) {
        return createNewCategory(locale, category.getName(), category.getSlug(), category.getDescription(),
                category.getParentId());
    }

    public Category createNewCategory(String locale, String cateName, String slug, String description, String parentId) {
        ObjectId parentOId = null;
        if (!StringUtil.isNullOrEmpty(parentId)) {
            if (termTaxonomyRepository.checkById(parentId))
                parentOId = new ObjectId(parentId);
            else
                throw new IllegalArgumentException("Parent Id [" + parentId + "] does not exist");
        }
        Term term = termRepository.insertNewTerm(slug, cateName, locale, Constant.I18NType.CATEGORY_NAME);
        TermTaxonomy termTaxonomy = termTaxonomyRepository.insertNewTermTaxonomy(
                term.getId(), Taxonomy.PRODUCT_CATEGORY, description, parentOId, locale, Constant.I18NType.CATEGORY_DESCRIPTION);
        return new Category(termTaxonomy.getId(), cateName, slug, description);
    }

    /**
     * Get category by its primary #ObjectId
     *
     * @param categoryId
     * @return
     */
    public Category getCategoryById(String categoryId) {
        return self.getCloneCategory(new ObjectId(categoryId));
    }

    @Cacheable(value = "category", key = "{#methodName, #selectId}", unless = "#result == null")
    public Category getCategoryBySelectId(int selectId) {
        Document document = termTaxonomyRepository.getByTermTaxonomySelectId(selectId);
        if (null == document) return null;
        List<I18NTranslation> nameI18N = i18NTranslationRepository.getModels(document.getObjectId("i18n_name_id"));
        Map<String, String> i18nName = new HashMap<>();
        Map<String, String> i18nSlug = new HashMap<>();
        Map<String, String> i18NDescription = new HashMap<>();
        nameI18N.forEach(i18NTranslation -> {
            i18nName.put(i18NTranslation.getLocale(), i18NTranslation.getText());
            i18nSlug.put(i18NTranslation.getLocale(), i18NTranslation.getSlug());
        });
        if (document.containsKey("i18n_description_id")) {
            List<I18NTranslation> descriptionI18N = i18NTranslationRepository.getModels(document.getObjectId("i18n_description_id"));
            descriptionI18N.forEach(i18NTranslation -> i18NDescription.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }
        Category category = new Category(document.getObjectId("_id"), i18nSlug, i18nName, i18NDescription);
        category.setParentObjectId(document.getObjectId("parent_id"));
        category.setSelectId(document.getInteger("select_id"));
        return category;
    }

    /**
     * Get category by its primary #ObjectId
     *
     * @param categoryId
     * @return
     */
    @Cacheable(value = "category", key = "{#root.methodName, #categoryId}", unless = "#result == null")
    public Category getCategoryById(ObjectId categoryId) {
        Document document = termTaxonomyRepository.getByTermTaxonomyId(categoryId);
        List<I18NTranslation> nameI18NList = i18NTranslationRepository.getModels(document.getObjectId("i18n_name_id"));
        Map<String, String> i18nName = new HashMap<>();
        Map<String, String> i18nSlug = new HashMap<>();
        Map<String, String> i18NDescription = new HashMap<>();
        nameI18NList.forEach(i18NTranslation -> {
            i18nName.put(i18NTranslation.getLocale(), i18NTranslation.getText());
            i18nSlug.put(i18NTranslation.getLocale(), i18NTranslation.getSlug());
        });
        if (document.containsKey("i18n_description_id")) {
            List<I18NTranslation> descriptionI18NList = i18NTranslationRepository.getModels(document.getObjectId("i18n_description_id"));
            descriptionI18NList.forEach(i18NTranslation -> i18NDescription.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }
        Category category = new Category(document.getObjectId("_id"), i18nSlug, i18nName, i18NDescription);
        category.setParentObjectId(document.getObjectId("parent_id"));
        category.setSelectId(document.getInteger("select_id"));

        loadProductAttributes(category);
        return category;
    }

    /**
     * Get category by its slug. This slug is unique for whole categories
     *
     * @param slug
     * @return
     */
    @Cacheable(value = "category", key = "{#root.methodName, #slug}", unless = "#result == null")
    public Category getCategoryBySlug(String slug) {
        I18NTranslation nameI18N = i18NTranslationRepository.getOne(Arrays.asList(new TargetValue("slug", slug),
                new TargetValue("type", Constant.I18NType.CATEGORY_NAME)));
        if (null == nameI18N) return null;
        Document document = termRepository.getByTermI18nIdAndTaxonomy(nameI18N.getObjectId(), Taxonomy.PRODUCT_CATEGORY);

        List<I18NTranslation> nameI18NList = i18NTranslationRepository.getModels(document.getObjectId("i18n_name_id"));
        Map<String, String> i18nName = new HashMap<>();
        Map<String, String> i18nSlug = new HashMap<>();
        Map<String, String> i18NDescription = new HashMap<>();
        nameI18NList.forEach(i18NTranslation -> {
            i18nName.put(i18NTranslation.getLocale(), i18NTranslation.getText());
            i18nSlug.put(i18NTranslation.getLocale(), i18NTranslation.getSlug());
        });
        if (document.containsKey("i18n_description_id")) {
            List<I18NTranslation> descriptionI18NList = i18NTranslationRepository.getModels(document.getObjectId("i18n_description_id"));
            descriptionI18NList.forEach(i18NTranslation -> i18NDescription.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }
        Category category = new Category(document.getObjectId("_id"), i18nSlug, i18nName, i18NDescription);
        category.setParentObjectId(document.getObjectId("parent_id"));
        category.setSelectId(document.getInteger("select_id"));

        loadProductAttributes(category);
        return category;
    }

    private void loadProductAttributes(Category category) {
        Map<String, Set<Attribute>> attributes = attributeService.getAttributesBelongToIdAndPrefix(
                category.getCategoryObjectId(), "pa_");
        if (null != attributes && !attributes.isEmpty()) {
            List<Attribute> rs = new ArrayList<>();
            for (Map.Entry<String, Set<Attribute>> entry : attributes.entrySet()) {
                Attribute attribute = attributeService.getCloneAttribute(new ObjectId(entry.getKey().substring(3)));
                attribute.setChildren(entry.getValue());
                rs.add(attribute);
            }
            category.setAttributes(rs);
        }
    }

    /**
     * Get same branch categories by current category #ObjectId
     *
     * @param thisCateId
     * @return
     */
    public List<Category> getSameBranchCategories(String thisCateId) {
        return getSameBranchCategories(new ObjectId(thisCateId));
    }

    public List<Category> getSameBranchCategories(ObjectId objectId) {
        Category thisCate = this.getCloneCategory(objectId);
        return this.getSameBranchCategories(thisCate);
    }

    public List<Category> getSameBranchCategories(Category thisCate) {
        List<Category> allByParentId = termTaxonomyRepository.getAllByParentId(thisCate.getParentObjectId(),
                CATEGORY_PROJECTION, Category.class);
        return allByParentId;
    }

    /**
     * Get all parent categories by primary #ObjectId
     *
     * @param categoryId
     * @return
     */
    @Cacheable(value = "category", key = "{#root.methodName, #categoryId}", unless = "#result == null")
    public List<Category> getParentCategoriesById(ObjectId categoryId) {
        List<Document> result = termTaxonomyRepository.getParentTermTaxonomies(categoryId, -1);
        List<Category> categories = result.stream()
                .map(doc -> self.getCloneCategory(doc.getObjectId("_id")))
                .collect(Collectors.toList());
        return categories;
    }

    /**
     * Get all parent categories by id. This slug is unique for whole categories
     *
     * @param categoryId
     * @param maxDepth
     * @param includeQueryCate
     * @return
     */
    public List<Category> getParentCategoriesById(ObjectId categoryId, int maxDepth, boolean includeQueryCate) {
        List<Category> parents = new ArrayList<>(self.getParentCategoriesById(categoryId));
        if (maxDepth > 0 && parents.size() > maxDepth)
            parents = parents.subList(parents.size() - maxDepth, parents.size());
        if (includeQueryCate) parents.add(self.getCloneCategory(categoryId));
        return parents;
    }

    /**
     * Get all child categories by slug. This slug is unique for whole categories
     *
     * @param slug
     * @param maxDepth
     * @param includeQueryCate
     * @return
     */
    public List<Category> getChildrenCategoriesBySlug(String slug, int maxDepth, boolean includeQueryCate) {
        Category categoryBySlug = self.getCategoryBySlug(slug);
        if (null == categoryBySlug) return null;
        List<Category> children = new ArrayList<>(self.getChildrenCategoriesById(categoryBySlug.getCategoryObjectId(), maxDepth));
        if (includeQueryCate) children.add(0, self.getCategoryBySlug(slug));
        return children;
    }

    /**
     * Get all child categories by categoryId with max depth, set -1 for maximum depth.
     *
     * @param categoryId
     * @return
     */
    @Cacheable(value = "category", key = "{#root.methodName, #categoryId, #maxDepth}")
    public List<Category> getChildrenCategoriesById(ObjectId categoryId, int maxDepth) {
        List<Document> result = termTaxonomyRepository.getChildrenTermTaxonomies(categoryId, maxDepth);
        List<Category> categories = result.stream()
                .map(doc -> self.getCloneCategory(doc.getObjectId("_id")))
                .collect(Collectors.toList());

        return categories;
    }

    @Cacheable(value = "category", key = "{#root.methodName}")
    public List<Category> getAllCategories() throws JsonProcessingException {
        List<Document> rawAll = termTaxonomyRepository.getRawAll(
                Arrays.asList(new TargetValue("taxonomy", Taxonomy.PRODUCT_CATEGORY), new TargetValue("parent_id", null)),
                -1, -1, null, -1);
        List<Category> rootCate = new ArrayList<>();
        for (Document raw : rawAll) {
            ObjectId objectId = raw.getObjectId("_id");
            Category category = self.getCloneCategory(objectId);
            List<Category> childrenById = self.getChildrenById(objectId);
            category.setChildren(childrenById);
            rootCate.add(category);
        }
        return rootCate;
    }

    public List<Category> getChildrenById(ObjectId cateId) {
        TermTaxonomyRelationship values = termTaxonomyRelationshipService.getRelationship(cateId, Constant.TTRelationship.CATEGORY_CHILDREN);
        List<Category> rs = new ArrayList<>();
        if (null != values && null != values.getRelationalTTIds() && !values.getRelationalTTIds().isEmpty())
            for (ObjectId value : values.getRelationalTTIds()) {
                List<Category> childrenByIds = self.getChildrenById(value);
                Category thisCate = self.getCloneCategory(value);
                if (!childrenByIds.isEmpty()) thisCate.setChildren(childrenByIds);
                rs.add(thisCate);
            }
        return rs;
    }

    public Category getCloneCategory(ObjectId id) {
        Category category = self.getCategoryById(id);
        return SerializationUtils.clone(category);
    }

}