package com.etybeno.ecommerce.service;

import com.etybeno.ecommerce.data.entity.TermTaxonomyRelationship;
import com.etybeno.ecommerce.repository.mongodb.TermTaxonomyRelationshipRepository;
import com.etybeno.ecommerce.repository.mongodb.TermTaxonomyRepository;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by thangpham on 05/06/2018.
 */
@Service
public class TermTaxonomyRelationshipService {

    @Autowired
    private TermTaxonomyRepository termTaxonomyRepository;
    @Autowired
    private TermTaxonomyRelationshipRepository termTaxonomyRelationshipRepository;

    public ObjectId addRelationship(ObjectId parentAttributeId, List<ObjectId> childrenIds, String relationshipName) {
        boolean b = termTaxonomyRepository.checkById(parentAttributeId);
        if (!b)
            throw new IllegalArgumentException(String.format("ParentId %s does not exist", parentAttributeId));
        TermTaxonomyRelationship relationship = new TermTaxonomyRelationship();
        relationship.setTermTaxonomyId(parentAttributeId);
        relationship.setRelationalTTIds(childrenIds);
        relationship.setType(relationshipName);
        return termTaxonomyRelationshipRepository.createOrUpdateTermTaxonomyRelationship(relationship);
    }

    public TermTaxonomyRelationship getRelationship(ObjectId keyId, String relationshipType) {
        List<KeyValue> targets = Arrays.asList(
                new TargetValue("term_taxonomy_id", keyId),
                new TargetValue("type", relationshipType));
        return termTaxonomyRelationshipRepository.getOne(targets);
    }

    public List<TermTaxonomyRelationship> getRelationships(ObjectId keyId, String prefix) {
        return termTaxonomyRelationshipRepository.getTermTaxonomyRelationshipWithPrefix(keyId, prefix);
    }

}
