package com.etybeno.ecommerce.service;

import com.etybeno.ecommerce.data.entity.Supplier;
import com.etybeno.ecommerce.data.form.SellerForm;
import com.etybeno.ecommerce.data.model.Seller;
import com.etybeno.ecommerce.repository.mongodb.SupplierRepository;
import com.etybeno.mongodb.model.TargetValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;

import static com.etybeno.ecommerce.repository.mongodb.SupplierRepository.SELLER_DETAIL_PROJECTION;

/**
 * Created by thangpham on 16/05/2018.
 */
@Service
public class SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;
    @Resource
    private SupplierService self;

    public boolean createSupplier(Supplier supplier) throws JsonProcessingException {
        return supplierRepository.createNewSupplier(supplier);
    }

    public boolean createSupplier(SellerForm sellerForm) throws JsonProcessingException {
        Supplier supplier = new Supplier();
        supplier.setSlug(sellerForm.getSlug());
        supplier.setName(sellerForm.getName());
        supplier.setLogo(sellerForm.getLogo());
        supplier.setDescription(sellerForm.getDescription());
        supplier.setPhone(sellerForm.getPhone());
        supplier.setAddress(sellerForm.getAddress());
        self.createSupplier(supplier);
        return true;
    }

    public Seller getDefaultSeller() {
        return self.getSellerBySlug("onemart-trading");
    }

    @Cacheable(value = "seller", key = "{#root.methodName, #sellerId}", unless = "#result == null")
    public Seller getSellerById(ObjectId sellerId) {
        return supplierRepository.getOne(Arrays.asList(new TargetValue("_id", sellerId)),
                SELLER_DETAIL_PROJECTION, Seller.class);
    }

    @Cacheable(value = "seller", key = "{#root.methodName, #slug}", unless = "#result == null")
    public Seller getSellerBySlug(String slug) {
        return supplierRepository.getOne(Arrays.asList(new TargetValue("slug", slug)), SELLER_DETAIL_PROJECTION, Seller.class);
    }
}
