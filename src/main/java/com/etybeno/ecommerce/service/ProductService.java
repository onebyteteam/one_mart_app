package com.etybeno.ecommerce.service;

import com.etybeno.common.util.HashUtil;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.Constant.I18NType;
import com.etybeno.ecommerce.data.entity.AbstractProduct;
import com.etybeno.ecommerce.data.entity.I18NTranslation;
import com.etybeno.ecommerce.data.enums.ProductFeature;
import com.etybeno.ecommerce.data.enums.ProductVisibility;
import com.etybeno.ecommerce.data.form.MetadataForm;
import com.etybeno.ecommerce.data.form.ProductForm;
import com.etybeno.ecommerce.data.model.Attribute;
import com.etybeno.ecommerce.data.model.Category;
import com.etybeno.ecommerce.data.model.Product;
import com.etybeno.ecommerce.data.model.Seller;
import com.etybeno.ecommerce.repository.mongodb.AbstractProductRepository;
import com.etybeno.ecommerce.repository.mongodb.I18NTranslationRepository;
import com.etybeno.ecommerce.util.ConvenientUtil;
import com.etybeno.mongodb.model.KeyValue;
import com.etybeno.mongodb.model.TargetValue;
import com.etybeno.mongodb.model.TargetValues;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by thangpham on 06/01/2018.
 */
@Service
public class ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private AbstractProductRepository abstractProductRepository;
    @Autowired
    private AttributeService attributeService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private I18NTranslationRepository i18NTranslationRepository;
    @Resource
    private ProductService self;

    @CacheEvict(value = {"count_product"}, allEntries = true)
    public boolean createNewProduct(AbstractProduct abstractProduct) throws JsonProcessingException {
        boolean b = abstractProductRepository.createProduct(abstractProduct);
        return b;
    }

    @CacheEvict(value = "product", key = "{#productId, #locale}", condition = "{#result != false}")
    public boolean updateProductForOrder(ObjectId productId, int numPurchase, String locale) {
        return abstractProductRepository.updateProductForOrder(productId, numPurchase);
    }

    @CacheEvict(value = {"count_product", "product_list"}, allEntries = true)
    public boolean createNewProduct(ProductForm productForm, String locale) throws IOException {
        ObjectId i18nNameId = i18NTranslationRepository.addNew(ConvenientUtil.parseToSlug(productForm.getName()),
                productForm.getName(), I18NType.PRODUCT_NAME, locale);
        AbstractProduct abProduct = new AbstractProduct();
        abProduct.setI18nNameId(i18nNameId);
        abProduct.setSku(productForm.getSku());
        abProduct.setSalePrice(productForm.getSalePrice());
        abProduct.setPrice(productForm.getSalePrice());
        abProduct.setRegularPrice(productForm.getPrice());
        abProduct.setProductVisibility(ProductVisibility.VISIBLE);
        abProduct.setManageStock(productForm.getManageStock());
        abProduct.setStockStatus(productForm.getStockStatus());
        abProduct.setStockQuantity(productForm.getStockQuantity());
        Set<ObjectId> categoryIds = productForm.getCategories().stream()
                .map(s -> categoryService.getCategoryBySelectId(s).getCategoryObjectId())
                .collect(Collectors.toSet());
        abProduct.setCategoryIds(categoryIds);
        abProduct.setGalleryImageIds(productForm.getImages());
        Set<ObjectId> searchAttrs = new HashSet<>();
        searchAttrs.addAll(categoryIds);
        abProduct.setSearchAttributeIds(searchAttrs);
        abProduct.setSupplierId(supplierService.getDefaultSeller().getSellerObjectId());
        //Set Description
        if (!StringUtil.isNullOrEmpty(productForm.getDescription())) {
            ObjectId i18nDescId = i18NTranslationRepository.addNew(null, productForm.getDescription(),
                    Constant.I18NType.PRODUCT_DESCRIPTION, locale);
            abProduct.setI18nDescriptionId(i18nDescId);
        }
        //Set short description
        if (!StringUtil.isNullOrEmpty(productForm.getShortDescription())) {
            ObjectId shortId = i18NTranslationRepository.addNew(null, productForm.getShortDescription(),
                    Constant.I18NType.PRODUCT_SHORT_DESCRIPTION, locale);
            abProduct.setI18nShortDescriptionId(shortId);
        }
        //Set Originator
        MetadataForm keyMeta = StringUtil.OBJECT_MAPPER.readValue(productForm.getOriginatorKey(), MetadataForm.class);
        List<MetadataForm> valuesMeta = StringUtil.OBJECT_MAPPER.readValue(productForm.getOriginatorValues(), MetadataForm.LIST_TYPE);
        if (keyMeta != null && valuesMeta != null && !valuesMeta.isEmpty()) {
            Map<String, Set<ObjectId>> originators = parseAttributes(AttributeService.ORIGINATOR_TYPE, keyMeta, valuesMeta, locale);
            abProduct.setOriginators(originators);
            searchAttrs.addAll(originators.values().stream().flatMap(objectIds -> objectIds.stream()).collect(Collectors.toSet()));
        }
        return self.createNewProduct(abProduct);
    }

    @CacheEvict(value = "product", key = "{#result, #locale}", condition = "{#result != null}")
    public ObjectId updateProduct(ProductForm upData, String locale) throws IOException {
        boolean isUpdated = false;
        AbstractProduct one = abstractProductRepository.getOne(Arrays.asList(new TargetValue("sku", upData.getSku())));
        Set<ObjectId> searchAttrIds = one.getSearchAttributeIds();
        boolean searchAttrChanged = false;
        List<TargetValue> updateList = new ArrayList<>();
        //Update Price
        if (Double.compare(one.getPrice(), upData.getPrice()) != 0)
            updateList.add(new TargetValue("regular_price", upData.getPrice()));
        //Update sale price
        if (Double.compare(one.getSalePrice(), upData.getSalePrice()) != 0) {
            updateList.add(new TargetValue("sale_price", upData.getSalePrice()));
            updateList.add(new TargetValue("price", upData.getSalePrice()));
        }
        //Update categories
        if (null != upData.getCategories() && !upData.getCategories().isEmpty()) {
            Set<ObjectId> updateCates = upData.getCategories().stream()
                    .map(s -> categoryService.getCategoryBySelectId(s).getCategoryObjectId())
                    .collect(Collectors.toSet());
            if (!one.getCategoryIds().equals(updateCates)) {
                searchAttrIds.removeAll(one.getCategoryIds());
                updateList.add(new TargetValue("category_ids", updateCates));
                searchAttrIds.addAll(updateCates);
                searchAttrChanged = true;
            }
        }
        //Update stocks
        if (upData.getManageStock()) {
            if (one.getStockQuantity() != upData.getStockQuantity()) {
                updateList.add(new TargetValue("manage_stock", Boolean.TRUE));
                updateList.add(new TargetValue("stock_quantity", upData.getStockQuantity()));
            }
        } else {
            if (upData.getStockStatus() != null && !upData.getStockStatus().equals(one.getStockStatus())) {
                updateList.add(new TargetValue("manage_stock", Boolean.FALSE));
                updateList.add(new TargetValue("stock_status", upData.getStockStatus()));
            }
        }
        //Update images
        if (!upData.getImages().isEmpty() && !one.getGalleryImageIds().equals(upData.getImages())) {
            updateList.add(new TargetValue("gallery_image_ids", upData.getImages()));
        }
        //Update Originator
        if (!StringUtil.isNullOrEmpty(upData.getOriginatorKey()) && !StringUtil.isNullOrEmpty(upData.getOriginatorValues())) {
            try {
                MetadataForm keyMeta = StringUtil.OBJECT_MAPPER.readValue(upData.getOriginatorKey(), MetadataForm.class);
                List<MetadataForm> valuesMeta = StringUtil.OBJECT_MAPPER.readValue(upData.getOriginatorValues(), MetadataForm.LIST_TYPE);
                if (keyMeta != null && valuesMeta != null && !valuesMeta.isEmpty()) {
                    Map<String, Set<ObjectId>> updateOriginators = parseAttributes(AttributeService.ORIGINATOR_TYPE, keyMeta, valuesMeta, locale);
                    Map<String, Set<ObjectId>> originators = one.getOriginators();
                    Set<ObjectId> update = updateOriginators == null || updateOriginators.isEmpty() ? null : updateOriginators.values().iterator().next();
                    Set<ObjectId> origin = originators == null || originators.isEmpty() ? null : originators.values().iterator().next();
                    if (update != null && !update.equals(origin)) {
                        updateList.add(new TargetValue("originators", updateOriginators));
                        if (origin != null) searchAttrIds.removeAll(origin);
                        searchAttrIds.addAll(update);
                        searchAttrChanged = true;
                    }
                }
            } catch (IOException ex) {
                LOGGER.warn(String.format("Could not parse originator for product-sku = %s", upData.getSku()));
            }
        }
        //Update name & slug
        I18NTranslation i18nName = i18NTranslationRepository.getModel(one.getI18nNameId(), locale);
        if (!i18nName.getText().equals(upData.getName()) || !i18nName.getSlug().equals(upData.getSlug())) {
            i18NTranslationRepository.update(i18nName.getId(), upData.getSlug(), upData.getName());
            isUpdated = true;
        }
        //Update description
        I18NTranslation i18nDesc = i18NTranslationRepository.getModel(one.getI18nDescriptionId(), locale);
        if (null == i18nDesc) {
            ObjectId i18nDescId = i18NTranslationRepository.addNew(null, upData.getDescription(),
                    Constant.I18NType.PRODUCT_DESCRIPTION, locale);
            updateList.add(new TargetValue("i18n_desc_id", i18nDescId));
            isUpdated = true;
        } else if (!i18nDesc.getText().equals(upData.getDescription())) {
            i18NTranslationRepository.update(i18nDesc.getId(), null, upData.getDescription());
            isUpdated = true;
        }
        //Update short description
        I18NTranslation i18nShortDesc = i18NTranslationRepository.getModel(one.getI18nShortDescriptionId(), locale);
        if (null == i18nShortDesc) {
            ObjectId i18nShortDescId = i18NTranslationRepository.addNew(null, upData.getShortDescription(),
                    I18NType.PRODUCT_SHORT_DESCRIPTION, locale);
            updateList.add(new TargetValue("i18n_short_desc_id", i18nShortDescId));
            isUpdated = true;
        } else if (!i18nShortDesc.getText().equals(upData.getShortDescription())) {
            i18NTranslationRepository.update(i18nShortDesc.getId(), null, upData.getShortDescription());
            isUpdated = true;
        }
        //
        if (searchAttrChanged) updateList.add(new TargetValue("search_attr_ids", searchAttrIds));
        if (!updateList.isEmpty()) {
            abstractProductRepository.updateProduct(one.getId(), updateList);
            isUpdated = true;
        }
        return isUpdated ? one.getId() : null;
    }

    private Map<String, Set<ObjectId>> parseAttributes(int attrType, MetadataForm attrKey, List<MetadataForm> attrValues, String locale) {
        Attribute originatorKey = attributeService.getAttributeBySlug(attrType, attrKey.getSlug(), locale);
        Set<ObjectId> collect = attrValues.stream()
                .map(form -> {
                    Attribute valueAttr;
                    if (form.isNew()) {
                        String slugValue = ConvenientUtil.parseToSlug(form.getName());
                        valueAttr = new Attribute(slugValue, form.getName(), originatorKey.getName() + " attribute", locale, originatorKey.getAttributeObjectId());
                        attributeService.addAttribute(attrType, valueAttr);
                    } else valueAttr = attributeService.getAttributeBySlug(attrType, form.getSlug(), locale);
                    return valueAttr.getAttributeObjectId();
                }).filter(objectId -> objectId != null).collect(Collectors.toSet());
        return new ImmutableMap.Builder<String, Set<ObjectId>>()
                .put(originatorKey.getAttributeId(), collect).build();
    }

    public long countAllProduct() throws NoSuchAlgorithmException {
        List<KeyValue> targets = Arrays.asList(new TargetValue("product_visibility", ProductVisibility.VISIBLE.name()));
        return abstractProductRepository.countAll(targets, null);
    }

    public List<Product> getProductByAttributeSlug(List<Set<Attribute>> attributes, String locale, int from, int size) throws JsonProcessingException, NoSuchAlgorithmException {
        List<Set<ObjectId>> collect = attributes.stream()
                .map(set -> set.stream().map(t -> t.getAttributeObjectId()).collect(Collectors.toSet()))
                .collect(Collectors.toList());
        List<ObjectId> documents = abstractProductRepository.queryProductsWithAttrs(
                Arrays.asList(new TargetValue("product_visibility", ProductVisibility.VISIBLE.name())), collect,
                from, size, Arrays.asList(new TargetValue("date_created", -1)));
        List<Product> products = documents.stream()
                .map(d -> self.getProductById(d, locale))
                .collect(Collectors.toList());
        return products;
    }

    public long countProductByAttributes(List<Set<Attribute>> attributes) throws NoSuchAlgorithmException {
        List<Set<ObjectId>> collect = attributes.stream()
                .map(set -> set.stream().map(t -> t.getAttributeObjectId()).collect(Collectors.toSet()))
                .collect(Collectors.toList());
        return abstractProductRepository.countByAttributes(collect, Arrays.asList(ProductVisibility.VISIBLE));
    }

    @Cacheable(value = "count_product", key = "{#root.methodName, #categorySlug, #locale, #attrQueryMap}")
    public long countProductByCategory(String categorySlug, String locale, Map<String, List<String>> attrQueryMap) throws NoSuchAlgorithmException {
        List<Category> categories = categoryService.getChildrenCategoriesBySlug(categorySlug, -1, true);
        if (null == categories || categories.isEmpty()) return 0;
        List<Set<ObjectId>> attrIds = null;
        if (attrQueryMap != null && !attrQueryMap.isEmpty()) {
            attrIds = attrQueryMap.keySet().stream()
                    .map(k -> {
                        Set<ObjectId> children = attrQueryMap.get(k).stream()
                                .map(s -> {
                                    String[] split = s.split("-");
                                    return attributeService.getAttributeBySelectId(
                                            Integer.parseInt(split[split.length - 1])).getAttributeObjectId();
                                }).collect(Collectors.toSet());
                        return children;
                    })
                    .collect(Collectors.toList());
        }
        Set<ObjectId> searchedIds = categories.stream().map(c -> c.getCategoryObjectId()).collect(Collectors.toSet());
        List<KeyValue> targets = Arrays.asList(new TargetValues("category_ids", searchedIds),
                new TargetValue("product_visibility", ProductVisibility.VISIBLE.name()));
        return abstractProductRepository.countAll(targets, attrIds);
    }

    /**
     * Get all products belong to category-slug with pagination
     *
     * @param categorySlug
     * @param from
     * @param size
     * @return
     */
    public List<Product> getProductsByCategory(String categorySlug, String locale, Map<String, List<String>> attrQueryMap, int from, int size, String sort) throws JsonProcessingException, NoSuchAlgorithmException {
        List<Category> categories = categoryService.getChildrenCategoriesBySlug(categorySlug, -1, true);
        if (null == categories || categories.isEmpty()) return new ArrayList<>(0);
        List<Set<ObjectId>> attrIds = null;
        if (attrQueryMap != null && !attrQueryMap.isEmpty()) {
            attrIds = attrQueryMap.keySet().stream()
                    .map(k -> {
                        Set<ObjectId> children = attrQueryMap.get(k).stream()
                                .map(s -> {
                                    String[] split = s.split("-");
                                    return attributeService.getAttributeBySelectId(
                                            Integer.parseInt(split[split.length - 1])).getAttributeObjectId();
                                }).collect(Collectors.toSet());
                        return children;
                    })
                    .collect(Collectors.toList());
        }
        TargetValue toSort = parseToSort(sort);
        List<ObjectId> searchedIds = categories.stream().map(c -> c.getCategoryObjectId()).collect(Collectors.toList());
        List<KeyValue> targets = Arrays.asList(
                new TargetValues("category_ids", searchedIds),
                new TargetValue("product_visibility", ProductVisibility.VISIBLE.name()));
        List<ObjectId> documents = abstractProductRepository.queryProductsWithAttrs(targets, attrIds, from, size, Arrays.asList(toSort));
        List<Product> products = documents.stream()
                .map(d -> self.getProductById(d, locale))
                .collect(Collectors.toList());
        return products;
    }

    public List<Product> searchProducts(String key, String locale, int from, int size) {
        List<KeyValue> targets = Arrays.asList(
                new TargetValues("product_visibility", Arrays.asList(ProductVisibility.VISIBLE.name(), ProductVisibility.SEARCH.name())));
        List<Document> documents = abstractProductRepository.getAllSearch(key, targets, locale, from, size,
                buildSorts(new TargetValue("created_date", -1)));
        List<Product> products = documents.stream()
                .map(d -> self.getProductById(d.getObjectId("_id"), locale))
                .collect(Collectors.toList());
        return products;
    }

    @Cacheable(value = "product_list", key = "{#methodName, #hashQuery, #from, #size}", unless = "#result == null")
    public List<ObjectId> getAllProductIds(String hashQuery, List<KeyValue> targets, int from, int size, List<TargetValue> sorts) {
        List<Document> all = abstractProductRepository.getRawAll(targets, from, size, sorts, -1);
        return all.stream().map(d -> d.getObjectId("_id")).collect(Collectors.toList());
    }

    @Cacheable(value = "product_list", key = "{#methodName, #cateSelId, #productFeature, #locale, #from, #size}", unless = "#result == null || #result.empty")
    public List<Product> getFeaturedProducts(int cateSelId, ProductFeature productFeature, String locale, int from, int size) throws NoSuchAlgorithmException {
        List<KeyValue> targets = new ArrayList<>();
        if (-1 != cateSelId) {
            Category category = categoryService.getCategoryBySelectId(cateSelId);
            List<Category> allChild = categoryService.getChildrenCategoriesById(category.getCategoryObjectId(), -1);
            allChild.add(category);
            targets.add(new TargetValues("category_ids", allChild.stream().map(c -> c.getCategoryObjectId()).collect(Collectors.toList())));
        }
        targets.add(new TargetValue("product_visibility", ProductVisibility.VISIBLE.name()));
        List<TargetValue> sorts = Arrays.asList(new TargetValue(productFeatureToField(productFeature), -1));
        String hashQuery = HashUtil.createHash((StringUtil.GSON.toJson(targets) + "@" + StringUtil.GSON.toJson(sorts)), HashUtil.SHA_256).asString();
        List<ObjectId> all = self.getAllProductIds(hashQuery, targets, from, size, sorts);
        List<Product> products = all.stream()
                .map(a -> self.getProductById(a, locale))
                .collect(Collectors.toList());
        return products;
    }

    public List<Product> getRelatedProducts(String sku, String locale, int from, int size) {
        Product productBySku = self.getProductBySku(sku, locale);
        if (null == productBySku) return new ArrayList<>();
        List<Category> allSameCates = new ArrayList<>();
        for (ObjectId cate : productBySku.getCategoriesAsObjectId()) {
            allSameCates.addAll(categoryService.getSameBranchCategories(cate.toString()));
        }
        //
        List<KeyValue> targets = Arrays.asList(
                new TargetValues("category_ids", allSameCates.stream()
                        .map(c -> c.getCategoryObjectId())
                        .collect(Collectors.toList())),
                new TargetValue("product_visibility", ProductVisibility.VISIBLE.name()));
        List<Document> productIds = abstractProductRepository.getRawAll(targets, from, size,
                Arrays.asList(new TargetValue("date_created", -1)), -1);
        List<Product> relatedProducts = productIds.stream().map(id -> self.getProductById(id.getObjectId("_id"), locale))
                .filter(product -> !product.getSku().equals(sku))
                .collect(Collectors.toList());
        return relatedProducts;
    }

    public long countSearchProducts(String key, String locale) throws NoSuchAlgorithmException {
        List<KeyValue> targets = Arrays.asList(
                new TargetValues("product_visibility", Arrays.asList(ProductVisibility.VISIBLE.name(), ProductVisibility.SEARCH.name())));
        return abstractProductRepository.countAllSearch(key, targets, locale);
    }

    @Cacheable(value = "product", key = "{#productId, #locale}", unless = "#result == null")
    public Product getProductById(ObjectId productId, String locale) {
        List<KeyValue> targets = Arrays.asList(new TargetValue("_id", productId));
        AbstractProduct abProduct = abstractProductRepository.getOne(targets);
        if (null == abProduct) return null;
        List<I18NTranslation> i18nNameList = i18NTranslationRepository.getModels(abProduct.getI18nNameId());
        List<I18NTranslation> i18nDescList = abProduct.getI18nDescriptionId() == null ? null :
                i18NTranslationRepository.getModels(abProduct.getI18nDescriptionId());
        List<I18NTranslation> i18nShortDescList = abProduct.getI18nShortDescriptionId() == null ? null :
                i18NTranslationRepository.getModels(abProduct.getI18nShortDescriptionId());
        Map<String, String> i18nNameMap = new HashMap<>();
        Map<String, String> i18nSlugMap = new HashMap<>();
        Map<String, String> i18nDescMap = new HashMap<>();
        Map<String, String> i18nShortDescMap = new HashMap<>();
        if(i18nDescList != null) {
            i18nDescList.forEach(i18NTranslation -> i18nDescMap.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }
        if(i18nShortDescList != null) {
            i18nShortDescList.forEach(i18NTranslation -> i18nShortDescMap.put(i18NTranslation.getLocale(), i18NTranslation.getText()));
        }
        i18nNameList.forEach(i18NTranslation -> {
            i18nNameMap.put(i18NTranslation.getLocale(), i18NTranslation.getText());
            i18nSlugMap.put(i18NTranslation.getLocale(), i18NTranslation.getSlug());
        });

        Product product = new Product()
                .setSku(abProduct.getSku())
//                .setSlug(i18nName.getSlug())
//                .setName(i18nName.getText())
                .setI18nName(i18nNameMap)
                .setI18nSlug(i18nSlugMap)
                .setSalePrice(abProduct.getSalePrice())
                .setPrice(abProduct.getPrice())
                .setRegularPrice(abProduct.getRegularPrice())
                .setImages(abProduct.getGalleryImageIds())
                .setProductObjectId(abProduct.getId())
                .setSellerId(abProduct.getSupplierId())
                .setCategoriesAsObjectId(abProduct.getCategoryIds())
                .setOriginatorsAsObjectId(abProduct.getOriginators())
                .setVisibility(abProduct.getProductVisibility())
                .setManageStock(abProduct.getManageStock() == null ? false : abProduct.getManageStock())
                .setStockStatus(abProduct.getStockStatus())
                .setStockQuantity(abProduct.getStockQuantity())
                .setI18nDescription(i18nDescMap)
                .setI18nShortDescription(i18nShortDescMap)
                .setLocale(locale);
        boolean manageStock = abProduct.getManageStock() == null ? false : abProduct.getManageStock();
        String stockStatus = StringUtil.isNullOrEmpty(abProduct.getStockStatus()) ? "instock" : abProduct.getStockStatus();
        if (manageStock) product.setStockLevel(abProduct.getStockQuantity());
        else {
            if ("instock".equals(stockStatus)) product.setStockLevel(-1);
            else if ("temp_out_of_stock".equals(stockStatus)) product.setStockLevel(-2);
            else product.setStockLevel(0);
        }
//        if (null != abProduct.getI18nDescriptionId()) {
//            I18NTranslation i18nDesc = i18NTranslationRepository.getModel(abProduct.getI18nDescriptionId(), locale);
//            product.setDescription(i18nDesc.getText());
//        }
//        if (null != abProduct.getI18nShortDescriptionId()) {
//            I18NTranslation i18nShortDesc = i18NTranslationRepository.getModel(abProduct.getI18nShortDescriptionId(), locale);
//            product.setShortDescription(i18nShortDesc.getText());
//        }
        loadOtherData(product);
        return product;
    }

    /**
     * Get product-detail by its sku
     *
     * @param sku
     * @return
     */
    public Product getProductBySku(String sku, String locale) {
        if (StringUtil.isNullOrEmpty(sku)) throw new IllegalArgumentException("product_sku is wrong.");
        ObjectId productId = self.getProductIdBySku(sku);
        if (null == productId) return null;
        return self.getProductById(productId, locale);
    }

    public ObjectId getProductIdBySku(String sku) {
        return abstractProductRepository.getProductIdBySku(sku);
    }

    private void loadOtherData(Product product) {
        loadCategories(product);
        loadBreadcrum(product);
        loadOriginators(product);
        loadSeller(product);
    }

    private void loadBreadcrum(Product product) {
        if (!product.getCategories().isEmpty()) {
            Category category = product.getCategories().get(0);
            List<Category> parents = categoryService.getParentCategoriesById(category.getCategoryObjectId(), 3, false);
            parents.add(category);
            product.setBreadcrum(parents);
        }
    }

    private void loadSeller(Product product) {
        if (null == product) return;
        Seller seller = supplierService.getSellerById(product.getSellerId());
        product.setSeller(seller);
    }

    private void loadCategories(Product product) {
        if (null == product) return;
        List<Category> categories = new ArrayList<>();
        for (ObjectId cateId : product.getCategoriesAsObjectId()) {
            categories.add(categoryService.getCloneCategory(cateId));
        }
        product.setCategories(categories);
    }

    private void loadOriginators(Product product) {
        if (null == product || product.getOriginatorsAsObjectId() == null || product.getOriginatorsAsObjectId().isEmpty())
            return;
        Attribute key = null;
        Set<Attribute> values = null;
        String locale = product.getLocale();
        for (Map.Entry<String, Set<ObjectId>> entry : product.getOriginatorsAsObjectId().entrySet()) {
            key = attributeService.getCloneAttribute(new ObjectId(entry.getKey()));
            values = entry.getValue().stream().map(id -> attributeService.getCloneAttribute(id)).collect(Collectors.toSet());
            break;
        }
        product.setOriginatorKey(key);
        product.setOriginatorValues(values);
    }

    private void loadOptions(Product product) {
        if (null == product || product.getOptionsAsObjectId() == null || product.getOptionsAsObjectId().isEmpty())
            return;
        Map<Attribute, Set<Attribute>> traits = new HashMap<>();
        for (Map.Entry<String, Set<ObjectId>> entry : product.getOptionsAsObjectId().entrySet()) {
//            Trait traitKey = traitService.getCloneTrait(new ObjectId(entry.getKey()));
//            Set<Trait> collect = entry.getValue().stream().map(id -> traitService.getCloneTrait(id)).collect(Collectors.toSet());
//            traits.put(traitKey, collect);
        }
//        product.setOptions(traits);
    }

    private Map<String, Object> buildSorts(TargetValue... fields) {
        Map<String, Object> sorts = new HashMap<>();
        Stream.of(fields).forEach(s -> sorts.put(s.getTarget(), s.getValue()));
        return sorts;
    }

    private String productFeatureToField(ProductFeature productFeature) {
        switch (productFeature) {
            case ON_SALE:
                return null;
            case TOP_RATED:
                return "rating_counts";
            case BEST_SELLER:
                return "total_sales";
            case NEW_ARRIVAL:
                return "created_date";
            default:
                return null;
        }
    }

    private TargetValue parseToSort(String value) {
        if (StringUtil.isNullOrEmpty(value)) return new TargetValue("created_date", -1);
        switch (value) {
            case "popularity":
                return new TargetValue("total_sales", -1);
            case "rating":
                return new TargetValue("rating_counts", -1);
            case "price":
                return new TargetValue("sale_price", 1);
            case "price-desc":
                return new TargetValue("sale_price", -1);
            case "date":
            default:
                return new TargetValue("created_date", -1);
        }
    }

}
