package com.etybeno.ecommerce.controller.process;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.form.CategoryForm;
import com.etybeno.ecommerce.data.form.ProductForm;
import com.etybeno.ecommerce.data.form.SellerForm;
import com.etybeno.ecommerce.data.model.CustomerOrder;
import com.etybeno.ecommerce.service.CategoryService;
import com.etybeno.ecommerce.service.ProductService;
import com.etybeno.ecommerce.service.ShoppingService;
import com.etybeno.ecommerce.service.SupplierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Locale;

/**
 * Created by thangpham on 17/05/2018.
 */
@Controller
@RequestMapping("/admin/process")
public class AdminProcessController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminProcessController.class);

    @Autowired
    private ProductService productService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ShoppingService shoppingService;

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    public String createProduct(@ModelAttribute("productForm") @Valid ProductForm productForm,
                                BindingResult bindingResult, Locale locale) {
        String returnTo = "redirect:/admin/product/add";
        try {
            boolean b = productService.createNewProduct(productForm, locale.toLanguageTag());
            returnTo = b ? "redirect:/admin/product/add" : "redirect:/admin/";
        } catch (Exception ex) {
            LOGGER.error("Error when create new product = " + StringUtil.GSON.toJson(productForm), ex);
        }
        return returnTo;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    public String updateProduct(@ModelAttribute("productForm") @Valid ProductForm productForm,
                                BindingResult bindingResult, Locale locale) {
        String returnTo;
        try {
            boolean b = productService.updateProduct(productForm, locale.toLanguageTag()) != null;
            returnTo = b ? "redirect:/admin/product/update?sku=" + productForm.getSku() : "redirect:/admin/";
        } catch (Exception ex) {
            LOGGER.error("Error when update product = " + StringUtil.GSON.toJson(productForm), ex);
            returnTo = "redirect:/admin/";
        }
        return returnTo;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping(value = "/addSeller", method = RequestMethod.POST)
    public String createSeller(@ModelAttribute("sellerForm") @Valid SellerForm sellerForm,
                               BindingResult bindingResult) {
        String returnTo = "redirect:/admin/seller/add";
        try {
            boolean b = supplierService.createSupplier(sellerForm);
            returnTo = b ? "redirect:/admin/seller/add" : "redirect:/admin/";
        } catch (Exception ex) {
            LOGGER.error("Error when create new seller = " + StringUtil.GSON.toJson(sellerForm), ex);
        }
        return returnTo;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping(value = "/addOrUpdateCategory", method = RequestMethod.POST)
    public String createOrUpdateCategory(@ModelAttribute("categoryForm") @Valid CategoryForm categoryForm,
                                         BindingResult bindingResult,
                                         Locale locale) {
        String returnTo = "redirect:/admin/seller/add";
        try {
            boolean b = categoryService.createOrUpdateCategory(locale.toLanguageTag(), categoryForm);
            returnTo = b ? "redirect:/admin/seller/add" : "redirect:/admin/";
        } catch (Exception ex) {
            LOGGER.error("Error when create new or update category = " + StringUtil.GSON.toJson(categoryForm), ex);
        }
        return returnTo;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
    public String updateOrder(@ModelAttribute("orderForm") @Valid CustomerOrder orderForm) {
        String returnTo = "redirect:/admin/order/detail";
        try {
            boolean b = shoppingService.updateOrder(orderForm);
            returnTo = b ? "redirect:/admin/order/" + orderForm.getOrderId() : "redirect:/admin/";
        } catch (Exception ex) {
            LOGGER.error("Error when update orderId: " + orderForm.getOrderId(), ex);
        }
        return returnTo;
    }

}