package com.etybeno.ecommerce.controller.process;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.custom.EcommerceUserDetails;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.data.form.OrderForm;
import com.etybeno.ecommerce.data.form.RegistryForm;
import com.etybeno.ecommerce.data.form.UserInfoForm;
import com.etybeno.ecommerce.service.CustomerService;
import com.etybeno.ecommerce.service.ShoppingService;
import com.etybeno.ecommerce.util.ConvenientUtil;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Locale;

import static com.etybeno.ecommerce.Constant.StringConstant.CART_SESSION_NAME;
import static com.etybeno.ecommerce.Constant.StringConstant.MAIN_SESSION_NAME;

/**
 * Created by thangpham on 08/02/2018.
 */
@Controller
@RequestMapping("/process")
public class ProcessController {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessController.class);

    @Autowired
    private CustomerService customerService;
    @Autowired
    private ShoppingService shoppingService;
    @Autowired
    private String themeName;

    @Value("${ecommerce.server.cookie.domain}")
    private String cookieDomain;

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/registry", method = RequestMethod.POST)
    public String registry(@ModelAttribute("registryForm") @Valid RegistryForm registryForm,
                           BindingResult bindingResult) {
        LOGGER.info("Customer registries " + registryForm.getUsername());
        if (bindingResult.hasErrors()) return "redirect:/index";
        String returnTo;
        try {
            registryForm.setIdentification(registryForm.getUsername());
            registryForm.setValidation(registryForm.getPassword());
            boolean b = customerService.createNewRegistryCustomer(registryForm, LoginType.REGISTRY);
            returnTo = b ? "redirect:/dang-ky-thanh-cong" : "redirect:/registry";
        } catch (Exception ex) {
            LOGGER.error("Error when registry with data = " + StringUtil.GSON.toJson(registryForm), ex);
            returnTo = "redirect:/404";
        }
        return returnTo;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public String order(@ModelAttribute("orderForm") @Valid OrderForm orderForm,
                        @CookieValue(name = MAIN_SESSION_NAME) String sessionCookie,
                        BindingResult bindingResult,
                        HttpServletResponse response, Locale locale) {
        LOGGER.info("Customer place order " + StringUtil.GSON.toJson(orderForm));
        if (bindingResult.hasErrors()) return "redirect:/index";
        String returnTo;
        try {
            String thisUsername = ConvenientUtil.getThisUsername();
            boolean b = shoppingService.processOrder(thisUsername, sessionCookie, orderForm, locale.toLanguageTag());
            Cookie cookie = ConvenientUtil.buildServletCookie(CART_SESSION_NAME, null, 0, "/", cookieDomain);
            response.addCookie(cookie);
            returnTo = b ? themeName + "/thankyou-purchase" : "redirect:/404";
        } catch (Exception ex) {
            LOGGER.error("Error when process order = " + StringUtil.GSON.toJson(orderForm), ex);
            returnTo = "redirect:/404";
        }
        return returnTo;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/changeUserInfo", method = RequestMethod.POST)
    public String changeUserInfo(@ModelAttribute("userInfoForm") @Valid UserInfoForm userInfoForm,
                                 BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Locale locale) {
        if (bindingResult.hasErrors()) return "redirect:/index";
        String returnTo = "redirect:/404";
        try {
            EcommerceUserDetails thisUserDetails = ConvenientUtil.getThisUserDetails();
            if (null != thisUserDetails) {
                String displayName = ((userInfoForm.getFirstName() == null ? "" : userInfoForm.getFirstName()) +
                        " " + (userInfoForm.getLastName() == null ? "" : userInfoForm.getLastName())).trim();
                userInfoForm.setDisplayName(displayName);
                boolean b = customerService.updateUserInfoByUsername(thisUserDetails.getUsername(), userInfoForm);
                if (b) {
                    CookieClearingLogoutHandler cookieClearingLogoutHandler = new CookieClearingLogoutHandler(
                            AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY);
                    SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
                    cookieClearingLogoutHandler.logout(request, response, null);
                    securityContextLogoutHandler.logout(request, response, null);
                    returnTo = "redirect:/dang-nhap";
                } else {
                    returnTo = "redirect:/tai-khoan";
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error when process order = " + StringUtil.GSON.toJson(userInfoForm), ex);
            returnTo = "redirect:/404";
        }
        return returnTo;
    }
}
