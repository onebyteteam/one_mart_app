package com.etybeno.ecommerce.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by thangpham on 16/02/2018.
 */
@RestController
@RequestMapping("/api/catalog/v1")
public class CatalogRestController {

    static final Logger LOGGER = LoggerFactory.getLogger(CatalogRestController.class);


}
