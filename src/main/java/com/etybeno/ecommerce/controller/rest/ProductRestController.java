package com.etybeno.ecommerce.controller.rest;

import com.etybeno.common.model.ResponseDataModel;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.enums.ProductFeature;
import com.etybeno.ecommerce.data.model.Product;
import com.etybeno.ecommerce.service.ProductService;
import com.etybeno.ecommerce.util.RestControllerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.etybeno.ecommerce.Constant.HttpString.JSON_UTF8;
import static com.etybeno.ecommerce.Constant.UserSite.PAGES_TO_SHOW;

/**
 * Created by thangpham on 28/12/2017.
 */
@RestController
@RequestMapping("/api/product/v1")
public class ProductRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRestController.class);

    @Autowired
    private ProductService productService;

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/getAll", produces = JSON_UTF8)
    public ResponseDataModel getAllProduct(@RequestParam(name = "page", defaultValue = "1") int page,
                                           @RequestParam(name = "size", defaultValue = "12") int size,
                                           @RequestParam(name = "locale", defaultValue = "vi-VN") String locale) {
        LOGGER.info("Get all product at page" + page);
        ResponseDataModel response;
        try {
            List<Product> mainProducts = productService.getFeaturedProducts(-1, ProductFeature.NEW_ARRIVAL, locale, (page - 1) * size, size);
            long count = productService.countAllProduct();
            response = RestControllerUtil.responseData(mainProducts, (int) Math.ceil((double) count / size), page, PAGES_TO_SHOW, count);
        } catch (Exception ex) {
            LOGGER.error("Error when getAllProduct", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/getFeatured", produces = JSON_UTF8)
    public ResponseDataModel getFeaturedProduct(
            @RequestParam("feature") String feature,
            @RequestParam(value = "category", required = false, defaultValue = "-1") int cateSelId,
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "12") int size,
            @RequestParam(name = "locale", defaultValue = "vi-VN") String locale) {
        LOGGER.info(String.format("Get featured products: category=[%d], feature=[%s], page=[%d], size=[%d]", cateSelId, feature, page, size));
        ResponseDataModel response;
        try {
            List<Product> relatedProducts = productService.getFeaturedProducts(cateSelId, ProductFeature.valueOf(feature), locale, (page - 1) * size, size);
            response = RestControllerUtil.responseData(relatedProducts);
        } catch (Exception ex) {
            LOGGER.error(String.format("Error when getFeaturedProduct: feature=[%s], page=[%d], size=[%d]", feature, page, size), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('administrator')")
    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = JSON_UTF8, produces = JSON_UTF8)
    public ResponseDataModel createNewProduct(@RequestBody Product product) {
        LOGGER.info("Create new product " + StringUtil.GSON.toJson(product));
        ResponseDataModel response;
        try {
//            boolean b = productService.createNewProduct(product);
//            response = b ?
//                    RestControllerUtil.responseOK() :
//                    RestControllerUtil.responseFailed("Could not create new product");
            response = RestControllerUtil.responseFailed("Not supported");
        } catch (Exception ex) {
            LOGGER.error("Error when createNewProduct with data = " + StringUtil.GSON.toJson(product), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/getRelated", produces = JSON_UTF8)
    public ResponseDataModel getRelatedProduct(@RequestParam("sku") String sku,
                                               @RequestParam(value = "page", defaultValue = "1") int page,
                                               @RequestParam(value = "size", defaultValue = "8") int size,
                                               @RequestParam(name = "locale", defaultValue = "vi-VN") String locale) {
        LOGGER.info("Get related products ");
        ResponseDataModel response;
        try {
            List<Product> relatedProducts = productService.getRelatedProducts(sku, locale, (page - 1) * size, size);
            response = RestControllerUtil.responseData(relatedProducts);
        } catch (Exception ex) {
            LOGGER.error("Error when getRelatedProduct", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

}