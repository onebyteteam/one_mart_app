package com.etybeno.ecommerce.controller.rest;

import com.etybeno.common.model.ResponseDataModel;
import com.etybeno.common.util.FileUtil;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.model.*;
import com.etybeno.ecommerce.processor.importer.DataImporter;
import com.etybeno.ecommerce.service.DataMonitorService;
import com.etybeno.ecommerce.service.ProductService;
import com.etybeno.ecommerce.util.RestControllerUtil;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 20/02/2018.
 */
@RestController
@RequestMapping("/api/administration/v1")
public class AdministrationRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdministrationRestController.class);

    @Autowired
    private DataImporter dataImporter;
    @Autowired
    private DataMonitorService dataMonitorService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CacheManager cacheManager;


    @PreAuthorize("permitAll()")
    @RequestMapping("/removeAllCache")
    public ResponseDataModel removeAllCache() {
        cacheManager.getCacheNames().parallelStream().forEach(name -> cacheManager.getCache(name).clear());
        LOGGER.info("Evicting all entries from product.");
        return RestControllerUtil.responseOK();
    }

    @PreAuthorize("permitAll()")
    @RequestMapping("/checkProductSku")
    public ResponseDataModel checkProductSku(@RequestParam("sku") String sku) {
        ResponseDataModel response;
        try {
            ObjectId idBySku = productService.getProductIdBySku(sku);
            response = RestControllerUtil.responseData(null == idBySku);
        } catch (Exception ex) {
            LOGGER.error(String.format("Check product sku %s" , sku), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/listSellerData")
    public ResponseDataModel listSellerData(@RequestParam(value = "p", required = false, defaultValue = "-1") int page,
                                            @RequestParam(value = "s", required = false, defaultValue = "-1") int size) {
        LOGGER.info(String.format("Received list all seller data: page=[%d], size=[%d]", page, size));
        ResponseDataModel response;
        try {
            List<Seller> sellerList = dataMonitorService.getSellerList(page, size);
            response = RestControllerUtil.responseData(sellerList, 100);
        } catch (Exception ex) {
            LOGGER.error("List all seller ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/listAccountData")
    public ResponseDataModel listAccountData(@RequestParam(value = "p", required = false, defaultValue = "-1") int page,
                                             @RequestParam(value = "s", required = false, defaultValue = "-1") int size) {
        LOGGER.info(String.format("Received list all account data: page=[%d], size=[%d]", page, size));
        ResponseDataModel response;
        try {
            List<AccountReport> accountList = dataMonitorService.getAccountList(page, size);
            response = RestControllerUtil.responseData(accountList, 100);
        } catch (Exception ex) {
            LOGGER.error("List all account ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/listCategoryData")
    public ResponseDataModel listCategoryData(Locale locale) {
        LOGGER.info("Received list all category data");
        ResponseDataModel response;
        try {
            List<Category> orderList = dataMonitorService.getAllCategory();
            response = RestControllerUtil.responseData(orderList);
        } catch (Exception ex) {
            LOGGER.error("List all category ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/listProductData")
    public ResponseDataModel listProductData(@RequestParam(value = "p", required = false, defaultValue = "-1") int page,
                                             @RequestParam(value = "s", required = false, defaultValue = "-1") int size,
                                             Locale locale) {
        LOGGER.info(String.format("Received list all product data: page=[%d], size=[%d]", page, size));
        ResponseDataModel response;
        try {
            List<Product> orderList = dataMonitorService.getProductList(locale.toLanguageTag(), page, size);
            response = RestControllerUtil.responseData(orderList, 100);
        } catch (Exception ex) {
            LOGGER.error("List all product ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/listOrderData")
    public ResponseDataModel listOrderData(@RequestParam(value = "p", required = false, defaultValue = "-1") int page,
                                           @RequestParam(value = "s", required = false, defaultValue = "-1") int size) {
        LOGGER.info(String.format("Received list all order data: page=[%d], size=[%d]", page, size));
        ResponseDataModel response;
        try {
            List<OrderReport> orderList = dataMonitorService.getOrderList(page, size);
            response = RestControllerUtil.responseData(orderList, 10);
        } catch (Exception ex) {
            LOGGER.error("List all order ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/getSaleData")
    public ResponseDataModel getSaleData(@RequestParam(value = "granularity", required = false) String granularityStr,
                                         @RequestParam(value = "duration", required = false) String duration,
                                         @RequestParam(value = "quick_range", required = false) String quickRange) {
        LOGGER.info(String.format("Received sale data report request granularity=[%s], duration=[%s], quickRange=[%s]",
                granularityStr, duration, quickRange));
        ResponseDataModel response;
        try {
            Map<String, Map<String, Object>> saleData;
            if (StringUtil.isNullOrEmpty(granularityStr) && StringUtil.isNullOrEmpty(duration)) {
                saleData = dataMonitorService.getSaleData(quickRange);
            } else saleData = dataMonitorService.getSaleData(granularityStr, duration);
            response = RestControllerUtil.responseData(saleData);
        } catch (Exception ex) {
            LOGGER.error(String.format("Failed sale data report granularity=[%s], duration=[%s], quickRange=[%s]",
                    granularityStr, duration, quickRange), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/countTotalProduct")
    public ResponseDataModel countTotalProduct() {
        LOGGER.info("Received count total product");
        ResponseDataModel response;
        try {
            response = RestControllerUtil.responseData(null, dataMonitorService.countTotalProduct());
        } catch (Exception ex) {
            LOGGER.error("Count total product ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/countTotalCustomer")
    public ResponseDataModel countTotalCustomer() {
        LOGGER.info("Received count total customer");
        ResponseDataModel response;
        try {
            response = RestControllerUtil.responseData(null, dataMonitorService.countTotalCustomer());
        } catch (Exception ex) {
            LOGGER.error("Count total customer ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/countTotalSale")
    public ResponseDataModel countTotalSale() {
        LOGGER.info("Received count total sale");
        ResponseDataModel response;
        try {
            response = RestControllerUtil.responseData(null, dataMonitorService.countTotalSale());
        } catch (Exception ex) {
            LOGGER.error("Count total sale ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR@normal')")
    @RequestMapping("/countTotalOrder")
    public ResponseDataModel countTotalOrder() {
        LOGGER.info("Received count total order");
        ResponseDataModel response;
        try {
            response = RestControllerUtil.responseData(null, dataMonitorService.countTotalOrder());
        } catch (Exception ex) {
            LOGGER.error("Count total order ", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }


    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadProductList", method = RequestMethod.POST)
    public ResponseDataModel uploadProductList(
            @RequestParam("locale") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload product list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importProductsFromJson(strings.stream().collect(Collectors.joining("")), locale);
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadProductList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadCategoryList", method = RequestMethod.POST)
    public ResponseDataModel uploadCategoryList(
            @RequestParam(name = "locale", required = false, defaultValue = "vi-VN") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload category list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importCategoriesFromJson(strings.stream().collect(Collectors.joining("")), locale);
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadCategoryList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadCategoryAttributeList", method = RequestMethod.POST)
    public ResponseDataModel uploadCategoryAttributeList(
            @RequestParam(name = "locale", required = false, defaultValue = "vi-VN") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload attribute list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importCategoryAttributesFromJson(strings.stream().collect(Collectors.joining("")), locale);
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadCategoryAttributeList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadAttributeList", method = RequestMethod.POST)
    public ResponseDataModel uploadAttributeList(
            @RequestParam(value = "locale", required = false, defaultValue = "vi-VN") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload attribute list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importAttributesFromJson(strings.stream().collect(Collectors.joining("")), locale);
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadAttributeList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadSellerList", method = RequestMethod.POST)
    public ResponseDataModel uploadSellerList(
            @RequestParam(value = "locale", required = false, defaultValue = "vi-VN") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload seller list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importSupplierFromJson(strings.stream().collect(Collectors.joining("")));
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadSellerList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }


    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadCatalogList", method = RequestMethod.POST)
    public ResponseDataModel uploadCatalogList(
            @RequestParam(value = "locale", required = false, defaultValue = "vi-VN") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload catalog list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importCatalogsFromJson(strings.stream().collect(Collectors.joining("")), locale);
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadCatalogList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/uploadPermissionList", method = RequestMethod.POST)
    public ResponseDataModel uploadPermissionList(
            @RequestParam(value = "locale", required = false, defaultValue = "vi-VN") String locale,
            @RequestParam("file") MultipartFile file) {
        LOGGER.info("Received upload permission list " + file.getOriginalFilename());
        ResponseDataModel response;
        try {
            List<String> strings = FileUtil.readLines(file.getInputStream());
            dataImporter.importPermissionsFromJson(strings.stream().collect(Collectors.joining("")));
            response = RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when uploadPermissionList with data = " + file.getOriginalFilename(), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

}
