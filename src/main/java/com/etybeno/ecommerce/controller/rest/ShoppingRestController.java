package com.etybeno.ecommerce.controller.rest;

import com.etybeno.common.model.ResponseDataModel;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.model.CookieMapValue;
import com.etybeno.ecommerce.service.ShoppingService;
import com.etybeno.ecommerce.util.ConvenientUtil;
import com.etybeno.ecommerce.util.RestControllerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Base64;
import java.util.Map;

import static com.etybeno.ecommerce.Constant.ResponseCode.INSERT_OK;
import static com.etybeno.ecommerce.Constant.ResponseCode.UPDATE_OK;
import static com.etybeno.ecommerce.Constant.StringConstant.*;
import static com.etybeno.ecommerce.Constant.Time.SEVEN_DAYS_IN_SECONDS;

/**
 * Created by thangpham on 07/03/2018.
 */
@RestController
@RequestMapping(value = "/api/shopping/v1")
public class ShoppingRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingRestController.class);

    @Autowired
    private ShoppingService shoppingService;

    @Value("${ecommerce.server.cookie.domain}")
    private String cookieDomain;

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/addProduct", method = RequestMethod.POST, consumes = "application/json")
    public ResponseDataModel addNewProductToCart(
            @CookieValue(name = MAIN_SESSION_NAME, required = false, defaultValue = "") String mainCookie,
            @CookieValue(name = CART_SESSION_NAME, required = false, defaultValue = "") String cartCookie,
            @RequestBody Map<String, String> params,
            HttpServletRequest request,
            HttpServletResponse response) {
        ResponseDataModel responseDataModel;
        try {
            if (StringUtil.isNullOrEmpty(mainCookie)) {
                HttpSession session = request.getSession(true);
                mainCookie = session.getId();
            }
            int quantity = StringUtil.safeParseInt(params.get("quantity"));
            String productSku = params.get("product_sku");
            if (quantity <= 0 || StringUtil.isNullOrEmpty(productSku)) {
                responseDataModel = RestControllerUtil.responseFailed(6002, "Either product_sku or quantity is wrong.");
            } else {
                boolean b = shoppingService.addNewProductToCart(ConvenientUtil.getThisUsername(), mainCookie, productSku, quantity);
                if (b) {
                    responseDataModel = RestControllerUtil.responseCode(INSERT_OK);
                    CookieMapValue mapValue = ConvenientUtil.parseBase64CookieMapValue(cartCookie);
                    mapValue.addInt(CART_NUMPRODUCT_NAME, quantity);
                    Cookie cookie = ConvenientUtil.buildServletCookie(CART_SESSION_NAME, Base64.getEncoder().encodeToString(mapValue.formatToBytes()),
                            SEVEN_DAYS_IN_SECONDS, "/", cookieDomain);
                    response.addCookie(cookie);
                } else {
                    responseDataModel = RestControllerUtil.responseCode(UPDATE_OK);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error when add new product to cart", ex);
            responseDataModel = RestControllerUtil.responseError(ex.getMessage());
        }
        return responseDataModel;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/deleteProduct", method = RequestMethod.DELETE)
    public ResponseDataModel deleteProductFromCart(
            @CookieValue(name = MAIN_SESSION_NAME) String mainCookie,
            @CookieValue(name = CART_SESSION_NAME, required = false, defaultValue = "") String cartCookie,
            @RequestParam(name = "product_sku") String productSku,
            HttpServletResponse response) {
        ResponseDataModel responseDataModel;
        try {
            if (StringUtil.isNullOrEmpty(mainCookie)) {
                responseDataModel = RestControllerUtil.
                        responseFailed(6001, "Request does contain session name " + MAIN_SESSION_NAME);
                return responseDataModel;
            }
            boolean b = shoppingService.deleteProductFromCart(ConvenientUtil.getThisUsername(), mainCookie, productSku);
            if (b) {
                responseDataModel = RestControllerUtil.responseCode(INSERT_OK);
                CookieMapValue mapValue = ConvenientUtil.parseBase64CookieMapValue(cartCookie).addInt(CART_NUMPRODUCT_NAME, -1);
                Cookie cookie = ConvenientUtil.buildServletCookie(CART_SESSION_NAME, Base64.getEncoder().encodeToString(mapValue.formatToBytes()),
                        SEVEN_DAYS_IN_SECONDS, "/", cookieDomain);
                response.addCookie(cookie);
            } else {
                responseDataModel = RestControllerUtil.responseFailed(3000, "Could not delete cart_product");
            }
        } catch (Exception ex) {
            LOGGER.error("Error when add new product to cart", ex);
            responseDataModel = RestControllerUtil.responseError(ex.getMessage());
        }
        return responseDataModel;
    }

}