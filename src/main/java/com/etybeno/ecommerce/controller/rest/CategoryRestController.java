package com.etybeno.ecommerce.controller.rest;

import com.etybeno.common.model.ResponseDataModel;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.model.Category;
import com.etybeno.ecommerce.service.CategoryService;
import com.etybeno.ecommerce.util.RestControllerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Locale;

/**
 * Created by thangpham on 29/12/2017.
 */
@RestController
@RequestMapping("/api/category/v1")
public class CategoryRestController {

    static final Logger LOGGER = LoggerFactory.getLogger(CategoryRestController.class);

    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/create")
    @PreAuthorize("hasAnyAuthority('administrator')")
    public ResponseDataModel createNewCategory(@RequestBody Category category) {
        LOGGER.info("Create new category " + StringUtil.GSON.toJson(category));
        ResponseDataModel response;
        try {
            Category b = null;
//            Category b = categoryService.createNewCategory(null, category);
            response = null == b.getCategoryId() ?
                    RestControllerUtil.responseFailed("Could not create new category") :
                    RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when createNewCategory with data = " + StringUtil.GSON.toJson(category), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @RequestMapping("/getAll")
    @PreAuthorize("permitAll()")
    public ResponseDataModel getAllCategory() {
        LOGGER.info("Call get all categories");
        ResponseDataModel response;
        try {
            List<Category> b = categoryService.getAllCategories();
            response = null == b ?
                    RestControllerUtil.responseFailed("Could not create new category") :
                    RestControllerUtil.responseData(b, b.size());
        } catch (Exception ex) {
            LOGGER.error("Error when getAllCategory", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }
}
