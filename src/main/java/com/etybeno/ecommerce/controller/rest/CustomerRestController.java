package com.etybeno.ecommerce.controller.rest;

import com.etybeno.common.model.ResponseDataModel;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.model.Customer;
import com.etybeno.ecommerce.service.CustomerService;
import com.etybeno.ecommerce.util.RestControllerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by thangpham on 02/02/2018.
 */
@RestController
@RequestMapping("/api/customer/v1")
public class CustomerRestController {

    static final Logger LOGGER = LoggerFactory.getLogger(CustomerRestController.class);

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/create")
    @PreAuthorize("hasAnyAuthority('administrator')")
    public ResponseDataModel createNewAccount(@RequestBody Customer customer) {
        LOGGER.info("Create new customer " + StringUtil.GSON.toJson(customer));
        ResponseDataModel response;
        try {
            boolean b = customerService.createNewRegistryCustomer(null ,null);
            response = b ?
                    RestControllerUtil.responseOK() :
                    RestControllerUtil.responseFailed("Could not create new customer");
        } catch (Exception ex) {
            LOGGER.error("Error when createNewCustomer with data = " + StringUtil.GSON.toJson(customer), ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }
}
