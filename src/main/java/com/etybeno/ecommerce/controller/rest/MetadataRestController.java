package com.etybeno.ecommerce.controller.rest;

import com.etybeno.common.model.ResponseDataModel;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.Constant;
import com.etybeno.ecommerce.data.model.Attribute;
import com.etybeno.ecommerce.data.model.Category;
import com.etybeno.ecommerce.service.AttributeService;
import com.etybeno.ecommerce.service.CategoryService;
import com.etybeno.ecommerce.util.RestControllerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by thangpham on 13/06/2018.
 */
@RestController
@RequestMapping("/api/metadata/v1")
public class MetadataRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetadataRestController.class);

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttributeService attributeService;

    @RequestMapping(value = "/get/{id}")
    @PreAuthorize("hasAnyAuthority('administrator')")
    public ResponseDataModel getCategoryById(@PathParam(value = "id") String cateId) {
        LOGGER.info("Get category by cateId: " + cateId);
        ResponseDataModel response;
        try {
            Category b = categoryService.getCategoryById(cateId);
            response = null == b.getCategoryId() ?
                    RestControllerUtil.responseFailed("Could not get category with id: " + cateId) :
                    RestControllerUtil.responseOK();
        } catch (Exception ex) {
            LOGGER.error("Error when Get category by cateId: " + cateId, ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @RequestMapping("/getAllCategory")
    @PreAuthorize("permitAll()")
    public ResponseDataModel getAllCategory(Locale locale) {
        LOGGER.info("Call get all categories");
        ResponseDataModel response;
        try {
            List<Category> b = categoryService.getAllCategories();
            response = null == b ?
                    RestControllerUtil.responseFailed("Could not get all category") :
                    RestControllerUtil.responseData(b, b.size());
        } catch (Exception ex) {
            LOGGER.error("Error when getAllCategory", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping("/getAllOriginator")
    public ResponseDataModel getAllOriginator(Locale locale) {
        LOGGER.info("Call get all originators");
        ResponseDataModel response;
        try {
            Set<Attribute> b = attributeService.getAllAttributesByType(AttributeService.ORIGINATOR_TYPE, locale.toLanguageTag());
            response = null == b ?
                    RestControllerUtil.responseFailed("Could not get all originators") :
                    RestControllerUtil.responseData(b, b.size());
        } catch (Exception ex) {
            LOGGER.error("Error when getAllOriginator", ex);
            response = RestControllerUtil.responseError(ex.getMessage());
        }
        return response;
    }
}
