package com.etybeno.ecommerce.controller.mvc;

import com.etybeno.common.model.Pager;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.data.enums.ProductFeature;
import com.etybeno.ecommerce.data.model.*;
import com.etybeno.ecommerce.service.*;
import com.etybeno.ecommerce.util.ConvenientUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerMapping;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

import static com.etybeno.ecommerce.Constant.StringConstant.*;
import static com.etybeno.ecommerce.Constant.Time.SEVEN_DAYS_IN_SECONDS;
import static com.etybeno.ecommerce.Constant.UserSite.PAGES_TO_SHOW;
import static com.etybeno.ecommerce.MessageConstant.BEST_SELLER;
import static com.etybeno.ecommerce.MessageConstant.NEW_ARRIVAL;

/**
 * Created by thangpham on 06/01/2018.
 */
@Controller
public class MainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Resource
    private String themeName;
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private ShoppingService shoppingService;
    @Autowired
    private AttributeService attributeService;
    @Autowired
    private CustomerService customerService;

    @Autowired
    private MessageSource messageSource;
    @Value("${ecommerce.server.cookie.domain}")
    private String cookieDomain;

    @RequestMapping(value = "/")
    public String main(@RequestParam(value = "p", defaultValue = "1") int page,
                       @RequestParam(value = "s", defaultValue = "20") int size,
                       ModelMap model, Locale locale) throws JsonProcessingException {
        try {
            long c = productService.countAllProduct();
            model.put("categories", categoryService.getAllCategories());
            model.put("products", productService.getFeaturedProducts(-1, ProductFeature.NEW_ARRIVAL, locale.toLanguageTag(),
                    (page - 1) * size, size));
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            return themeName + "/index";
        } catch (Exception ex) {
            LOGGER.error("Error when get main", ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/san-pham-moi")
    public String newProducts(@RequestParam(value = "p", defaultValue = "1") int page,
                              @RequestParam(value = "s", defaultValue = "12") int size,
                              ModelMap model, Locale locale) {
        try {
            long c = productService.countAllProduct();
            model.put("products", productService.getFeaturedProducts(-1, ProductFeature.NEW_ARRIVAL, locale.toLanguageTag(), (page - 1) * size, size));
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            model.put("page_title", messageSource.getMessage(NEW_ARRIVAL, null, locale));
            return themeName + "/feature-products";
        } catch (Exception ex) {
            LOGGER.error("Error when get newProducts", ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/tim-kiem")
    public String searchProducts(@RequestParam(value = "q") String searchKey,
                                 @RequestParam(value = "p", defaultValue = "1") int page,
                                 @RequestParam(value = "s", defaultValue = "12") int size,
                                 ModelMap model, Locale locale) {
        try {
            long c = productService.countSearchProducts(searchKey, locale.toLanguageTag());
            model.put("products", productService.searchProducts(searchKey, locale.toLanguageTag(), (page - 1) * size, size));
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            model.put("page_title", "Từ khoá: " + searchKey);
            return themeName + "/feature-products";
        } catch (Exception ex) {
            LOGGER.error("Error when get bestSellerProducts", ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/top-ban-chay")
    public String bestSellerProducts(@RequestParam(value = "p", defaultValue = "1") int page,
                                     @RequestParam(value = "s", defaultValue = "12") int size,
                                     ModelMap model, Locale locale) {
        try {
            long c = productService.countAllProduct();
            model.put("products", productService.getFeaturedProducts(-1, ProductFeature.BEST_SELLER, locale.toLanguageTag(), (page - 1) * size, size));
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            model.put("page_title", messageSource.getMessage(BEST_SELLER, null, locale));
            return themeName + "/feature-products";
        } catch (Exception ex) {
            LOGGER.error("Error when get bestSellerProducts", ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/san-pham/{product_name}-{product_sku}")
    public String getProductBySku(@PathVariable(value = "product_name") String productName,
                                  @PathVariable(value = "product_sku") String productSku,
                                  ModelMap model, Locale locale) {
        try {
            Product productBySku = productService.getProductBySku(productSku, locale.toLanguageTag());
            model.put("product", productBySku);
            return themeName + "/product-detail";
        } catch (Exception ex) {
            LOGGER.error("Error when get product by sku: " + productSku, ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/bo-suu-tap/{catalogSlug}-{catalog_code}")
    public String getCatalogByCode(@PathVariable(value = "catalogSlug") String catalogSlug,
                                   @PathVariable(value = "catalog_code") String catalogCode,
                                   @RequestParam(value = "p", defaultValue = "1") int page,
                                   @RequestParam(value = "s", defaultValue = "12") int size,
                                   ModelMap model, Locale locale) {
        try {
            Catalog catalog = catalogService.getCatalogByCode(catalogCode, locale.toLanguageTag(), (page - 1) * size, size);
            int count = catalog.getProductsAsObjectId().size();
            model.put("catalog", catalog);
            model.put("pager", new Pager((int) Math.ceil((double) count / size), page, PAGES_TO_SHOW, count));
            return "/views/main/catalog-products";
        } catch (Exception ex) {
            LOGGER.error("Error when get catalog by code: " + catalogCode, ex);
            return themeName + "/404";
        }
    }

    @RequestMapping(value = "/{category_slug}-c{select_id}")
    public String listByCategory(@PathVariable(value = "category_slug") String cateSlug,
                                 @PathVariable(value = "select_id") int selectId,
                                 @RequestParam(value = "aq", required = false) String attributeQuery,
                                 @RequestParam(value = "p", defaultValue = "1") int page,
                                 @RequestParam(value = "s", defaultValue = "12") int size,
                                 @RequestParam(value = "st", required = false, defaultValue = "popularity") String sort,
                                 ModelMap model, Locale locale) {
        try {
            String localeStr = locale.toLanguageTag();
            Map<String, List<String>> attrMapQuery = new HashMap<>();
            if (!StringUtil.isNullOrEmpty(attributeQuery)) {
                String[] filters = attributeQuery.split(",");
                for (String filter : filters) {
                    String[] split = filter.split(":");
                    if (split.length < 2) continue;
                    attrMapQuery.put(split[0], Arrays.asList(split[1].split("\\|")));
                }
            }
            long c = productService.countProductByCategory(cateSlug, localeStr, attrMapQuery);
            model.put("products", productService.getProductsByCategory(cateSlug, localeStr, attrMapQuery, (page - 1) * size, size, sort));
            Category categoryBySlug = categoryService.getCategoryBySelectId(selectId);
            model.put("this_category", categoryBySlug);

            model.put("categories", categoryService.getParentCategoriesById(categoryBySlug.getCategoryObjectId(), 3, true));
            model.put("child_categories", categoryService.getChildrenCategoriesBySlug(cateSlug, 0, false));
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            return themeName + "/category-products";
        } catch (Exception ex) {
            LOGGER.error("Error when list products by category: " + cateSlug, ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/thanh-toan")
    public String checkOutUserCart(@CookieValue(name = MAIN_SESSION_NAME) String sessionCookie, ModelMap model, Locale locale) {
        String thisUsername = ConvenientUtil.getThisUsername();
        try {
            if (StringUtil.isNullOrEmpty(sessionCookie)) {
                LOGGER.error("Session cookie is null, can not process");
                return "redirect:/404";
            }
            ObjectId userId = customerService.getIdByUsername(thisUsername);
            String newOrder = shoppingService.createNewOrder(userId, sessionCookie, locale.toLanguageTag());
            CartSession cartSession = shoppingService.getCartSession(userId, sessionCookie, locale.toLanguageTag());
            model.put("order_id", newOrder);
            model.put("cart", cartSession);
            return themeName + "/checkout";
        } catch (Exception ex) {
            LOGGER.error("Error when get cart for this username: " + thisUsername + " with session " + sessionCookie, ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/gio-hang")
    public String getCurrentUserCart(@CookieValue(name = MAIN_SESSION_NAME, required = false) String sessionCookie,
                                     ModelMap model, HttpServletResponse response, Locale locale) {
        String thisUsername = ConvenientUtil.getThisUsername();
        try {
            if (StringUtil.isNullOrEmpty(sessionCookie)) {
                LOGGER.warn("Session cookie is null, can not process");
                return themeName + "/cart";
            }
            ObjectId userId = customerService.getIdByUsername(thisUsername);
            CartSession cartSession = shoppingService.getCartSession(userId, sessionCookie, locale.toLanguageTag());
            if(null != cartSession) {
                int cartSize = cartSession.getCartProducts().stream().map(p -> p.getQuantity()).collect(Collectors.summingInt(value -> value));
                CookieMapValue mapValue = new CookieMapValue()
                        .addInt(CART_NUMPRODUCT_NAME, (cartSession == null) ? 0 :
                                (cartSession.getCartProducts() == null) ? 0 : cartSize);
                Cookie cookie = ConvenientUtil.buildServletCookie(CART_SESSION_NAME, Base64.getEncoder().encodeToString(mapValue.formatToBytes()),
                        SEVEN_DAYS_IN_SECONDS, "/", cookieDomain);
                response.addCookie(cookie);
                model.put("cart", cartSession);
            }
            return themeName + "/cart";
        } catch (Exception ex) {
            LOGGER.error("Error when get cart for this username: " + thisUsername + " with session " + sessionCookie, ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/bo-suu-tap")
    public String listCatalogs(@RequestParam(value = "p", defaultValue = "1") int page,
                               @RequestParam(value = "s", defaultValue = "12") int size,
                               ModelMap model) {
        try {
            long c = catalogService.countAllCatalog();
            model.put("catalogs", catalogService.getAllCatalog((page - 1) * size, size));
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            return "/views/main/display-catalogs";
        } catch (Exception ex) {
            LOGGER.error("Error when list all catalog", ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = {"/thuong-hieu/{value_slug}", "/tac-gia/{value_slug}"})
    public String listByOriginator(@PathVariable(value = "value_slug") String valueSlug,
                                   @RequestParam(value = "p", defaultValue = "1") int page,
                                   @RequestParam(value = "s", defaultValue = "12") int size,
                                   ModelMap model, HttpServletRequest request, Locale locale) {
        try {
            String[] split = ((String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)).split("/");
            Attribute originator = attributeService.getAttributeBySlug(AttributeService.ORIGINATOR_TYPE, split[1], locale.toLanguageTag());
            Attribute value = attributeService.getAttributeBySlug(AttributeService.ORIGINATOR_TYPE, valueSlug, locale.toLanguageTag());
            Set<Attribute> hashSet = new HashSet();
            hashSet.add(value);
            List<Set<Attribute>> hashSetList = Arrays.asList(hashSet);
            long c = productService.countProductByAttributes(hashSetList);
            List<Product> products = productService.getProductByAttributeSlug(hashSetList, locale.toLanguageTag(), (page - 1) * size, size);
            model.put("products", products);
            model.put("pager", new Pager((int) Math.ceil((double) c / size), page, PAGES_TO_SHOW, c));
            model.put("page_title", String.format("%s: %s", originator.getName(), value.getName()));
            return themeName + "/feature-products";
        } catch (Exception ex) {
            LOGGER.error(String.format("Error when list by value_slug: %s", valueSlug), ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/dang-nhap")
    public String authentication(@RequestParam(value = "em", required = false) String errorMessage,
                                 ModelMap model, Locale locale) {
        if (!StringUtil.isNullOrEmpty(errorMessage)) {
            String message = messageSource.getMessage(errorMessage, null, locale);
            model.put("error_message", message);
        }
        return themeName + "/authentication";
    }

    @RequestMapping(value = "/tai-khoan")
    public String account(ModelMap modelMap) {
        String thisUsername = ConvenientUtil.getThisUsername();
        try {
            Customer customer = customerService.getCustomerByUsername(thisUsername, null);
            customer.setAuthentications(null);//TODO: remove for security
            customer.setAuthorizations(null);//TODO: remove for security
            modelMap.put("account", customer);
            return themeName + "/account";
        } catch (Exception ex) {
            LOGGER.error("Error when information for this username: " + thisUsername, ex);
            return "redirect:/404";
        }
    }

    @RequestMapping(value = "/tai-khoan/don-hang")
    public String accountOrder(ModelMap modelMap) {
        String thisUsername = ConvenientUtil.getThisUsername();
        try {
            List<CustomerOrder> allOrders = shoppingService.getAllOrderByUsername(thisUsername);
            modelMap.put("all_orders", allOrders);
            return themeName + "/account-order";
        } catch (Exception ex) {
            LOGGER.error("Error when information for this username: " + thisUsername, ex);
            return "redirect:/404";
        }
    }
}