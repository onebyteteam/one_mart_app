package com.etybeno.ecommerce.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * Created by thangpham on 15/03/2018.
 */
@Controller
public class StaticController {

    @Resource
    private String themeName;

    @RequestMapping(value = "/404")
    public String error404() {
        return themeName + "/404";
    }

    @RequestMapping(value = "/dang-ky-thanh-cong")
    public String registrySuccessful() {
        return themeName + "/thankyou-signup";
    }

    @RequestMapping(value = "/chinh-sach-onemart")
    public String policy() {
        return themeName + "/policy";
    }
}