
package com.etybeno.ecommerce.controller.mvc;

import com.etybeno.ecommerce.data.model.CustomerOrder;
import com.etybeno.ecommerce.data.model.Product;
import com.etybeno.ecommerce.service.ProductService;
import com.etybeno.ecommerce.service.ShoppingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Locale;

/**
 * Created by thangpham on 04/04/2018.
 */
@Controller
@RequestMapping("/admin")
public class AdminMainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminMainController.class);

    @Resource
    private String adminThemeName;
    @Autowired
    private ProductService productService;
    @Autowired
    private ShoppingService shoppingService;

    @RequestMapping(value = {"/", ""})
    public String index(ModelMap modelMap) {
        modelMap.put("_page", "index");
        return adminThemeName + "/index";
    }

    @RequestMapping("/order")
    public String order(ModelMap modelMap) {
        modelMap.put("_page", "order");
        return adminThemeName + "/order/index";
    }

    @RequestMapping("/order/{order_id}")
    public String orderDetail(@PathVariable("order_id") String orderId, ModelMap modelMap) {
        modelMap.put("_page", "order-detail");
        CustomerOrder customerOrderById = shoppingService.getCustomerOrderById(orderId);
        modelMap.put("order", customerOrderById);
        return adminThemeName + "/order/order-detail";
    }

    @RequestMapping("/product/add")
    public String addProduct(ModelMap modelMap) {
        modelMap.put("_page", "product-add");
        return adminThemeName + "/product/product-add";
    }

    @RequestMapping("/product/update")
    public String updateProduct(@RequestParam("sku") String productSku,
                                ModelMap modelMap, Locale locale) {
        LOGGER.info("Call product update page: " + productSku);
        Product productBySku = productService.getProductBySku(productSku, locale.toLanguageTag());
        modelMap.put("targetProduct", productBySku);
        modelMap.put("_page", "product-update");
        return adminThemeName + "/product/product-update";
    }

    @RequestMapping("/product")
    public String product(ModelMap modelMap) {
        modelMap.put("_page", "product-list");
        return adminThemeName + "/product/index";
    }

    @RequestMapping("/category")
    public String category(ModelMap modelMap) {
        modelMap.put("_page", "product/category-list");
        return adminThemeName + "/category";
    }

    @RequestMapping("/account")
    public String account(ModelMap modelMap) {
        modelMap.put("_page", "account");
        return adminThemeName + "/account";
    }

    @RequestMapping("/seller")
    public String seller(ModelMap modelMap) {
        modelMap.put("_page", "seller-list");
        return adminThemeName + "/seller/index";
    }

    @RequestMapping("/seller/add")
    public String addSeller(ModelMap modelMap) {
        modelMap.put("_page", "seller-add");
        return adminThemeName + "/seller/seller-add";
    }

    @RequestMapping("/login")
    public String login() {
        return adminThemeName + "/login";
    }

    @RequestMapping("/test")
    public String test(ModelMap modelMap) {
        modelMap.put("_page", "test");
        return adminThemeName + "/test";
    }

}
