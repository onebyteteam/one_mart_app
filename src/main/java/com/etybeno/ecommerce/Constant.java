package com.etybeno.ecommerce;

/**
 * Created by thangpham on 25/12/2017.
 */
public class Constant {

    public interface MongoDB {
        String PRODUCTION_ECOMMERCE_NAME = "ecommerce";
        String DEV_ECOMMERCE_NAME = "dev";
        String TEST_ECOMMERCE_NAME = "test";
    }

    public interface Taxonomy {
        String PRODUCT_CATEGORY = "prod_cate";
        String PRODUCT_ATTRIBUTE = "prod_attr";
        String PRODUCT_ORIGINATOR = "prod_origin";
    }

    public interface TTRelationship {
        String CATEGORY_CHILDREN = "cate_child";
        String ATTRIBUTE_CHILDREN = "attr_child";
        String ORIGINATOR_CHILDREN = "origin_child";
    }

    public interface I18NType {
        String CATEGORY_NAME = "cate_name";
        String CATEGORY_DESCRIPTION = "cate_desc";
        String ATTRIBUTE_NAME = "attr_name";
        String ATTRIBUTE_DESCRIPTION = "attr_desc";
        String ORIGINATOR_NAME = "origin_name";
        String ORIGINATOR_DESCRIPTION = "origin_desc";
        String PRODUCT_NAME = "prod_name";
        String PRODUCT_DESCRIPTION = "prod_desc";
        String PRODUCT_SHORT_DESCRIPTION = "prod_short_desc";
    }

    public interface HttpString {
        String JSON_UTF8 = "application/json;charset=utf-8;";
    }

    public interface UserSite {
        int PAGES_TO_SHOW = 5;
    }

    public interface SocialProvider {
        String FACEBOOK = "facebook";
        String GOOGLE = "google";
        String ZALO = "zalo";
        String INSTAGRAM = "instagram";
    }

    public interface StringConstant {
        String MAIN_SESSION_NAME = "om";
        String CART_SESSION_NAME = "om.cart";
        String CART_NUMPRODUCT_NAME = "cart.np";
    }

    public interface ResponseCode {
        int OK = 1000;
        int INSERT_OK = 2000;
        int UPDATE_OK = 2001;
        int DELETE_OK = 2002;
        int ERROR = 6000;
    }

    public interface Time {
        int ONE_DAY_IN_SECONDS = 86400;
        int THREE_DAYS_IN_SECONDS = 259200;
        int SEVEN_DAYS_IN_SECONDS = 604800;
        int FOURTEEN_DAYS_IN_SECONDS = 1209600;
        int THIRTY_DAYS_IN_SECONDS = 2592000;
        int ONE_YEAR_IN_SECONDS = 31536000;
    }
}