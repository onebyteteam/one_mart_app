package com.etybeno.ecommerce.data.form;

import java.util.List;

/**
 * Created by thangpham on 17/05/2018.
 */
public class ProductForm {

    private String sku;
    private String name;
    private String slug;
    private List<Integer> categories;
    private Double price;
    private Double salePrice;
    private String shortDescription;
    private String description;
    private String originatorKey;
    private String originatorValues;
    private List<String> images;
    private boolean manageStock;
    private Integer stockQuantity;
    private String stockStatus;

    public Boolean getManageStock() {
        return manageStock;
    }

    public void setManageStock(Boolean manageStock) {
        this.manageStock = manageStock;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getOriginatorKey() {
        return originatorKey;
    }

    public void setOriginatorKey(String originatorKey) {
        this.originatorKey = originatorKey;
    }

    public String getOriginatorValues() {
        return originatorValues;
    }

    public void setOriginatorValues(String originatorValues) {
        this.originatorValues = originatorValues;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
