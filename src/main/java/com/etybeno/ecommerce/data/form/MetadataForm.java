package com.etybeno.ecommerce.data.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;

/**
 * Created by thangpham on 14/06/2018.
 */
public class MetadataForm {

    public static final TypeReference LIST_TYPE = new TypeReference<List<MetadataForm>>() {};

    private String slug;
    private String name;
    @JsonProperty("isNew")
    private boolean isNew;

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}
