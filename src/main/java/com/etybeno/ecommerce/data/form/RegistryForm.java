package com.etybeno.ecommerce.data.form;

import com.etybeno.ecommerce.annotations.Email;
import com.etybeno.ecommerce.annotations.UniqueLogin;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by thangpham on 06/02/2018.
 */
public class RegistryForm {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @UniqueLogin
    private String username;
    @Email
    private String email;
    private String password;
    @JsonProperty("confirm_password")
    private String confirmPassword;
    private String identification;
    private String validation;

    public String getIdentification() {
        return identification;
    }

    public RegistryForm setIdentification(String identification) {
        this.identification = identification;
        return this;
    }

    public String getValidation() {
        return validation;
    }

    public RegistryForm setValidation(String validation) {
        this.validation = validation;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public RegistryForm setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public RegistryForm setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public RegistryForm setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public RegistryForm setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public RegistryForm setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public RegistryForm setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
        return this;
    }
}