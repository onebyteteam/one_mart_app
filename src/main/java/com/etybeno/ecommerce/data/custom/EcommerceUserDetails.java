package com.etybeno.ecommerce.data.custom;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.social.security.SocialUserDetails;

import java.util.Collection;

/**
 * Created by thangpham on 04/03/2018.
 */
public class EcommerceUserDetails extends User implements SocialUserDetails {

    private static final long serialVersionUID = -2453735680677895049L;

    private String displayName;
    private String avatarUri;

    public EcommerceUserDetails(String displayName, String username, String password, String avatarUri, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.displayName = displayName;
        this.avatarUri = avatarUri;
    }

    public EcommerceUserDetails(String displayName, String username, String password, String avatarUri, boolean enabled, boolean accountNonExpired,
                                boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.displayName = displayName;
        this.avatarUri = avatarUri;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    @Override
    public String getUserId() {
        return getUsername();
    }
}
