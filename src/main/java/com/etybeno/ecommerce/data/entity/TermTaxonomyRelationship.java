package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * Created by thangpham on 11/01/2018.
 */
@Collection(name = "term_taxonomy_relationship")
public class TermTaxonomyRelationship extends BaseEntity {

    @JsonProperty("term_taxonomy_id")
    @BsonProperty("term_taxonomy_id")
    private ObjectId termTaxonomyId;
    @JsonProperty("relational_tt_ids")
    @BsonProperty("relational_tt_ids")
    private List<ObjectId> relationalTTIds;
    private String type;
    private Integer order;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ObjectId getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public void setTermTaxonomyId(ObjectId termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public List<ObjectId> getRelationalTTIds() {
        return relationalTTIds;
    }

    public void setRelationalTTIds(List<ObjectId> relationalTTIds) {
        this.relationalTTIds = relationalTTIds;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
