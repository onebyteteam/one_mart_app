package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.AccountType;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

import javax.sql.rowset.serial.SerialArray;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by thangpham on 20/02/2018.
 */
@Collection(name = "permission")
public class Permission implements Serializable {

    @BsonProperty("_id")
    @JsonProperty("_id")
    private Integer id;
    private String name;
    private String description;
    @BsonProperty("account_type")
    @JsonProperty("account_type")
    private AccountType accountType;
    @BsonProperty("created_date")
    @JsonProperty("created_date")
    private Date createdDate;
    @BsonProperty("modified_date")
    @JsonProperty("modified_date")
    private Date modifiedDate;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
