package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.OrderStatus;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by thangpham on 27/03/2018.
 */
@Collection(name = "order")
public class Order {

    @JsonProperty("_id")
    @BsonProperty("_id")
    private String orderId;
    @JsonProperty("user_id")
    @BsonProperty("user_id")
    private ObjectId userId;
    @JsonProperty("cart_id")
    @BsonProperty("cart_id")
    private ObjectId cartId;
    @JsonProperty("created_date")
    @BsonProperty("created_date")
    private Date createdDate;
    @JsonProperty("modified_date")
    @BsonProperty("modified_date")
    private Date modifiedDate;
    @JsonProperty("product_num")
    @BsonProperty("product_num")
    private Integer productNum;
    @JsonProperty("total_amount")
    @BsonProperty("total_amount")
    private Double totalAmount;
    @JsonProperty("order_status")
    @BsonProperty("order_status")
    private OrderStatus orderStatus;
    private String note;
    @JsonProperty("shipping_address")
    @BsonProperty("shipping_address")
    private ShippingAddress shippingAddress;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public Integer getProductNum() {
        return productNum;
    }

    public void setProductNum(Integer productNum) {
        this.productNum = productNum;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public ObjectId getCartId() {
        return cartId;
    }

    public void setCartId(ObjectId cartId) {
        this.cartId = cartId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}
