package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.CollectionVisibility;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * Created by thangpham on 16/02/2018.
 */
@Collection(name = "abstract_collection")
public class AbstractCollection extends BaseEntity {

    private String name;
    private String slug;
    private String code;
    private String description;
    @BsonProperty("created_date")
    @JsonProperty("created_date")
    private Date createdDate;
    @BsonProperty("modified_date")
    @JsonProperty("modified_date")
    private Date modifiedDate;
    @JsonProperty("product_ids")
    @BsonProperty("product_ids")
    private List<ObjectId> productIds;
    @BsonProperty("gallery_image_ids")
    @JsonProperty("gallery_image_ids")
    private List<String> galleryImageIds;
    @JsonProperty("collection_visibility")
    @BsonProperty("collection_visibility")
    private CollectionVisibility collectionVisibility;

    public List<String> getGalleryImageIds() {
        return galleryImageIds;
    }

    public void setGalleryImageIds(List<String> galleryImageIds) {
        this.galleryImageIds = galleryImageIds;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public List<ObjectId> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<ObjectId> productIds) {
        this.productIds = productIds;
    }

    public CollectionVisibility getCollectionVisibility() {
        return collectionVisibility;
    }

    public void setCollectionVisibility(CollectionVisibility collectionVisibility) {
        this.collectionVisibility = collectionVisibility;
    }
}
