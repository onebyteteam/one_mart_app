package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * Created by thangpham on 25/12/2017.
 */
@Collection(name = "term")
public class Term extends BaseEntity {

    @BsonProperty("i18n_name_id")
    @JsonProperty("i18n_name_id")
    private ObjectId i18nNameId;
    @BsonProperty("term_group")
    @JsonProperty("term_group")
    private Integer termGroup;

    public Term() {
    }

    public Term(ObjectId id, ObjectId i18nNameId) {
        this.id = id;
        this.i18nNameId = i18nNameId;
    }

    public Term(ObjectId i18nNameId) {
        this.i18nNameId = i18nNameId;
    }

    public ObjectId getI18nNameId() {
        return i18nNameId;
    }

    public void setI18nNameId(ObjectId i18nNameId) {
        this.i18nNameId = i18nNameId;
    }

    public Integer getTermGroup() {
        return termGroup;
    }

    public void setTermGroup(Integer termGroup) {
        this.termGroup = termGroup;
    }
}
