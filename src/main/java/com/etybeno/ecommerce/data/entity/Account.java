package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.AccountType;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by thangpham on 31/01/2018.
 */
@Collection(name = "account")
public class Account extends BaseEntity {

    @BsonProperty("user_id")
    private ObjectId userId;
    @BsonProperty("account_type")
    @JsonProperty("account_type")
    private AccountType accountType;
    private List<Integer> authorizations;
    private Boolean active;
    @BsonProperty("created_date")
    @JsonProperty("created_date")
    private Date createdDate;
    @BsonProperty("modified_date")
    @JsonProperty("modified_date")
    private Date modifiedDate;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public List<Integer> getAuthorizations() {
        return authorizations;
    }

    public void setAuthorizations(List<Integer> authorizations) {
        this.authorizations = authorizations;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
