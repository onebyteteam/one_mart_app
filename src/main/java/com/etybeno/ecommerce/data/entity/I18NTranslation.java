package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * Created by thangpham on 10/06/2018.
 */
@Collection(name = "i18n_translation")
public class I18NTranslation {

    @BsonProperty("_id")
    @JsonIgnore
    private Long id;
    @JsonProperty("object_id")
    @BsonProperty("object_id")
    private ObjectId objectId;
    private String slug;
    private String text;
    private String type;
    private String locale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
