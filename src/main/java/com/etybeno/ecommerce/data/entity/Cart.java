package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.CartStatus;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by thangpham on 07/03/2018.
 */
@Collection(name = "cart")
public class Cart extends BaseEntity {

    @JsonProperty("user_id")
    @BsonProperty("user_id")
    private ObjectId userId;
    @JsonProperty("session_id")
    @BsonProperty("session_id")
    private String sessionId;
    @JsonProperty("created_date")
    @BsonProperty("created_date")
    private Date createdDate;
    @JsonProperty("modified_date")
    @BsonProperty("modified_date")
    private Date modifiedDate;
    @JsonProperty("expire_on")
    @BsonProperty("expire_on")
    private Date expireOn;
    @JsonProperty("cart_status")
    @BsonProperty("cart_status")
    private CartStatus cartStatus;

    public CartStatus getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(CartStatus cartStatus) {
        this.cartStatus = cartStatus;
    }

    public Date getExpireOn() {
        return expireOn;
    }

    public void setExpireOn(Date expireOn) {
        this.expireOn = expireOn;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
