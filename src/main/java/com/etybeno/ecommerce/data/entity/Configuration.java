package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thangpham on 10/04/2018.
 */
@Collection(name = "configuration")
public class Configuration {

    @JsonProperty("key")
    @SerializedName("key")
    private String _id;
    private Object value;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}