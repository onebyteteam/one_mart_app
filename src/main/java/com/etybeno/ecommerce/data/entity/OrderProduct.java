package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * Created by thangpham on 27/03/2018.
 */
@Collection(name = "order_product")
public class OrderProduct extends BaseEntity {

    @JsonProperty("order_id")
    @BsonProperty("order_id")
    private String orderId;
    @BsonProperty("product_id")
    @JsonProperty("product_id")
    private ObjectId productId;
    @JsonProperty("product_name")
    @BsonProperty("product_name")
    private String productName;
    private Integer quantity;
    private Double price;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public ObjectId getProductId() {
        return productId;
    }

    public void setProductId(ObjectId productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
