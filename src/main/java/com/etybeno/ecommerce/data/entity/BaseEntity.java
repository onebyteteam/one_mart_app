package com.etybeno.ecommerce.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;

/**
 * Created by thangpham on 25/12/2017.
 */
public abstract class BaseEntity implements Serializable {

    @BsonProperty("_id")
    @JsonIgnore
    protected ObjectId id;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
