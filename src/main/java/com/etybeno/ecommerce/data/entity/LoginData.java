package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by thangpham on 31/01/2018.
 */
@Collection(name = "login_data")
public class LoginData extends BaseEntity {

    @JsonProperty("user_id")
    @BsonProperty("user_id")
    private ObjectId userId;
    private String identification;
    private String validation;
    @JsonProperty("refresh_token")
    @BsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("expire_time")
    @BsonProperty("expire_time")
    private Long expireTime;
    @BsonProperty("login_type")
    @JsonProperty("login_type")
    private LoginType loginType;
    @JsonProperty("profile_url")
    @BsonProperty("profile_url")
    private String profileUrl;
    @JsonProperty("image_url")
    @BsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("created_date")
    @BsonProperty("created_date")
    private Date createdDate;
    @JsonProperty("modified_date")
    @BsonProperty("modified_date")
    private Date modifiedDate;
    private Integer rank;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
