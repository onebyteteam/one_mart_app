package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.util.Date;

/**
 * Created by thangpham on 20/01/2018.
 */
@Collection(name = "user")
public class User extends BaseEntity {

    @JsonProperty("created_date")
    @BsonProperty("created_date")
    private Date createdDate;
    @BsonProperty("modified_date")
    @JsonProperty("modified_date")
    private Date modifiedDate;
    @JsonProperty("first_name")
    @BsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    @BsonProperty("last_name")
    private String lastName;
    private String email;
    private String username;
    @JsonProperty("display_name")
    @BsonProperty("display_name")
    private String displayName;
    @JsonProperty("avatar_uri")
    @BsonProperty("avatar_uri")
    private String avatarUri;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
