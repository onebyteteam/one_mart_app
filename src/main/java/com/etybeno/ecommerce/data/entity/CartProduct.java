package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * Created by thangpham on 07/03/2018.
 */
@Collection(name = "cart_product")
public class CartProduct extends BaseEntity {

    @BsonProperty("cart_id")
    private ObjectId cartId;
    @BsonProperty("product_id")
    private ObjectId productId;
    private Integer quantity;

    public ObjectId getCartId() {
        return cartId;
    }

    public void setCartId(ObjectId cartId) {
        this.cartId = cartId;
    }

    public ObjectId getProductId() {
        return productId;
    }

    public void setProductId(ObjectId productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
