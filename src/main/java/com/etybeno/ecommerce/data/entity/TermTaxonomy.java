package com.etybeno.ecommerce.data.entity;

import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * Created by thangpham on 25/12/2017.
 */
@Collection(name = "term_taxonomy")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TermTaxonomy extends BaseEntity {

    @BsonProperty("term_id")
    @JsonProperty("term_id")
    private ObjectId termId;
    private String taxonomy;
    @BsonProperty("i18n_description_id")
    @JsonProperty("i18n_description_id")
    private ObjectId i18nDescriptionId;
    @BsonProperty("parent_id")
    @JsonProperty("parent_id")
    private ObjectId parentId;
    @JsonProperty("select_id")
    @BsonProperty("select_id")
    private Integer selectId;
    private Long count;

    public TermTaxonomy() {
    }

    public TermTaxonomy(ObjectId termId, String taxonomy, ObjectId i18nDescriptionId) {
        this.termId = termId;
        this.taxonomy = taxonomy;
        this.i18nDescriptionId = i18nDescriptionId;
    }

    public TermTaxonomy(ObjectId termId, String taxonomy, ObjectId i18nDescriptionId, ObjectId parentId) {
        this.termId = termId;
        this.taxonomy = taxonomy;
        this.i18nDescriptionId = i18nDescriptionId;
        this.parentId = parentId;
    }

    public TermTaxonomy(ObjectId termId, String taxonomy, ObjectId i18nDescriptionId, ObjectId parentId, Integer selectId) {
        this.termId = termId;
        this.taxonomy = taxonomy;
        this.i18nDescriptionId = i18nDescriptionId;
        this.parentId = parentId;
        this.selectId = selectId;
    }

    public Integer getSelectId() {
        return selectId;
    }

    public void setSelectId(Integer selectId) {
        this.selectId = selectId;
    }

    public ObjectId getTermId() {
        return termId;
    }

    public void setTermId(ObjectId termId) {
        this.termId = termId;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public ObjectId getI18nDescriptionId() {
        return i18nDescriptionId;
    }

    public void setI18nDescriptionId(ObjectId i18nDescriptionId) {
        this.i18nDescriptionId = i18nDescriptionId;
    }

    public ObjectId getParentId() {
        return parentId;
    }

    public void setParentId(ObjectId parentId) {
        this.parentId = parentId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
