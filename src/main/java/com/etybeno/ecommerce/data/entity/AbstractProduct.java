package com.etybeno.ecommerce.data.entity;

import com.etybeno.ecommerce.data.enums.ProductType;
import com.etybeno.ecommerce.data.enums.ProductVisibility;
import com.etybeno.mongodb.annotation.Collection;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by thangpham on 06/01/2018.
 */
@Collection(name = "abstract_product")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AbstractProduct extends BaseEntity {

    @JsonProperty("i18n_name_id")
    @BsonProperty("i18n_name_id")
    private ObjectId i18nNameId;
    @BsonProperty("created_date")
    @JsonProperty("created_date")
    private Date createdDate;
    @BsonProperty("modified_date")
    @JsonProperty("modified_date")
    private Date modifiedDate;
    private String status;
    private Boolean featured;
    @BsonProperty("product_visibility")
    @JsonProperty("product_visibility")
    private ProductVisibility productVisibility;
    @BsonProperty("product_type")
    @JsonProperty("product_type")
    private ProductType productType;
    @JsonProperty("i18n_desc_id")
    @BsonProperty("i18n_desc_id")
    private ObjectId i18nDescriptionId;
    @BsonProperty("i18n_short_desc_id")
    @JsonProperty("i18n_short_desc_id")
    private ObjectId i18nShortDescriptionId;
    private String sku;
    private Double price;
    @BsonProperty("regular_price")
    @JsonProperty("regular_price")
    private Double regularPrice;
    @BsonProperty("sale_price")
    @JsonProperty("sale_price")
    private Double salePrice;
    @BsonProperty("total_sales")
    @JsonProperty("total_sales")
    private int totalSales;
    @BsonProperty("tax_status")
    @JsonProperty("tax_status")
    private String taxStatus;
    @BsonProperty("tax_class")
    @JsonProperty("tax_class")
    private String taxClass;
    @BsonProperty("manage_stock")
    @JsonProperty("manage_stock")
    private Boolean manageStock;
    @BsonProperty("stock_quantity")
    @JsonProperty("stock_quantity")
    private Integer stockQuantity;
    @BsonProperty("stock_status")
    @JsonProperty("stock_status")
    private String stockStatus;
    @BsonProperty("back_orders")
    @JsonProperty("back_orders")
    private String backorders;
    @BsonProperty("sold_individually")
    @JsonProperty("sold_individually")
    private Boolean soldIndividually;
    private Double weight;
    private Double length;
    private Double width;
    private Double height;
    @BsonProperty("upsell_ids")
    @JsonProperty("upsell_ids")
    private Set<ObjectId> upsellIds;
    @BsonProperty("cross_sell_ids")
    @JsonProperty("cross_sell_ids")
    private Set<ObjectId> crossSellIds;
    @BsonProperty("reviews_allowed")
    @JsonProperty("reviews_allowed")
    private Boolean reviewAllowed;
    @BsonProperty("purchase_note")
    @JsonProperty("purchase_note")
    private String purchaseNote;
    @BsonProperty("category_ids")
    @JsonProperty("category_ids")
    private Set<ObjectId> categoryIds;
    @BsonProperty("search_attr_ids")
    @JsonProperty("search_attr_ids")
    private Set<ObjectId> searchAttributeIds;
    @BsonProperty("originators")
    @JsonProperty("originators")
    private Map<String, Set<ObjectId>> originators;
    @BsonProperty("option_attr_ids")
    @JsonProperty("option_attr_ids")
    private Map<String, Set<ObjectId>> optionAttributeIds;
    @BsonProperty("supplier_id")
    @JsonProperty("supplier_id")
    private ObjectId supplierId;
    @BsonProperty("image_id")
    @JsonProperty("image_id")
    private Integer imageId;
    @BsonProperty("gallery_image_ids")
    @JsonProperty("gallery_image_ids")
    private List<String> galleryImageIds;
    @BsonProperty("rating_counts")
    @JsonProperty("rating_counts")
    private Map<String, Integer> ratingCounts;
    @BsonProperty("average_rating")
    @JsonProperty("average_rating")
    private Float averageRating;
    @BsonProperty("review_count")
    @JsonProperty("review_count")
    private Integer reviewCount;

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Set<ObjectId> getUpsellIds() {
        return upsellIds;
    }

    public void setUpsellIds(Set<ObjectId> upsellIds) {
        this.upsellIds = upsellIds;
    }

    public Set<ObjectId> getCrossSellIds() {
        return crossSellIds;
    }

    public void setCrossSellIds(Set<ObjectId> crossSellIds) {
        this.crossSellIds = crossSellIds;
    }

    public Double getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(Double regularPrice) {
        this.regularPrice = regularPrice;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Set<ObjectId> getSearchAttributeIds() {
        return searchAttributeIds;
    }

    public void setSearchAttributeIds(Set<ObjectId> searchAttributeIds) {
        this.searchAttributeIds = searchAttributeIds;
    }

    public Map<String, Set<ObjectId>> getOriginators() {
        return originators;
    }

    public void setOriginators(Map<String, Set<ObjectId>> originators) {
        this.originators = originators;
    }

    public Map<String, Set<ObjectId>> getOptionAttributeIds() {
        return optionAttributeIds;
    }

    public void setOptionAttributeIds(Map<String, Set<ObjectId>> optionAttributeIds) {
        this.optionAttributeIds = optionAttributeIds;
    }

    public ObjectId getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(ObjectId supplierId) {
        this.supplierId = supplierId;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public ProductVisibility getProductVisibility() {
        return productVisibility;
    }

    public void setProductVisibility(ProductVisibility productVisibility) {
        this.productVisibility = productVisibility;
    }

    public ObjectId getI18nNameId() {
        return i18nNameId;
    }

    public void setI18nNameId(ObjectId i18nNameId) {
        this.i18nNameId = i18nNameId;
    }

    public ObjectId getI18nDescriptionId() {
        return i18nDescriptionId;
    }

    public void setI18nDescriptionId(ObjectId i18nDescriptionId) {
        this.i18nDescriptionId = i18nDescriptionId;
    }

    public ObjectId getI18nShortDescriptionId() {
        return i18nShortDescriptionId;
    }

    public void setI18nShortDescriptionId(ObjectId i18nShortDescriptionId) {
        this.i18nShortDescriptionId = i18nShortDescriptionId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(int totalSales) {
        this.totalSales = totalSales;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    public Boolean getManageStock() {
        return manageStock;
    }

    public void setManageStock(Boolean manageStock) {
        this.manageStock = manageStock;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getBackorders() {
        return backorders;
    }

    public void setBackorders(String backorders) {
        this.backorders = backorders;
    }

    public Boolean getSoldIndividually() {
        return soldIndividually;
    }

    public void setSoldIndividually(Boolean soldIndividually) {
        this.soldIndividually = soldIndividually;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Boolean getReviewAllowed() {
        return reviewAllowed;
    }

    public void setReviewAllowed(Boolean reviewAllowed) {
        this.reviewAllowed = reviewAllowed;
    }

    public String getPurchaseNote() {
        return purchaseNote;
    }

    public void setPurchaseNote(String purchaseNote) {
        this.purchaseNote = purchaseNote;
    }

    public Set<ObjectId> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Set<ObjectId> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public List<String> getGalleryImageIds() {
        return galleryImageIds;
    }

    public void setGalleryImageIds(List<String> galleryImageIds) {
        this.galleryImageIds = galleryImageIds;
    }

    public Map<String, Integer> getRatingCounts() {
        return ratingCounts;
    }

    public void setRatingCounts(Map<String, Integer> ratingCounts) {
        this.ratingCounts = ratingCounts;
    }

    public Float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Float averageRating) {
        this.averageRating = averageRating;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }
}