package com.etybeno.ecommerce.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * Created by thangpham on 05/07/2018.
 */
public class CustomerOrderProduct {

    @JsonIgnore
    @BsonProperty("_id")
    private Object orderProductAsObjectId;
    @BsonProperty("product_id")
    @JsonProperty("product_id")
    private ObjectId productId;
    @JsonProperty("product_name")
    @BsonProperty("product_name")
    private String productName;
    private Integer quantity;
    private Double price;

    public Object getOrderProductAsObjectId() {
        return orderProductAsObjectId;
    }

    public CustomerOrderProduct setOrderProductAsObjectId(Object orderProductAsObjectId) {
        this.orderProductAsObjectId = orderProductAsObjectId;
        return this;
    }

    public ObjectId getProductId() {
        return productId;
    }

    public CustomerOrderProduct setProductId(ObjectId productId) {
        this.productId = productId;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public CustomerOrderProduct setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public CustomerOrderProduct setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public CustomerOrderProduct setPrice(Double price) {
        this.price = price;
        return this;
    }
}
