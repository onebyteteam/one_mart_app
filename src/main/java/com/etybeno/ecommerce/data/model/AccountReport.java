package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.enums.AccountType;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.util.List;

/**
 * Created by thangpham on 14/04/2018.
 */
public class AccountReport {

    private String username;
    @BsonProperty("display_name")
    @JsonProperty("display_name")
    private String displayName;
    @BsonProperty("avatar_uri")
    @JsonProperty("avatar_uri")
    private String avatarUri;
    @BsonProperty("account_type")
    @JsonProperty("account_type")
    private AccountType accountType;
    private Boolean active;
    private List<Integer> authorizations;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Integer> getAuthorizations() {
        return authorizations;
    }

    public void setAuthorizations(List<Integer> authorizations) {
        this.authorizations = authorizations;
    }
}
