package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.enums.CollectionVisibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 16/02/2018.
 */
public class Catalog implements Serializable {

    @JsonProperty("catalog_id")
    private ObjectId catalogId;
    private String code;
    private String name;
    private String slug;
    private String description;
    private CollectionVisibility visible;
    @BsonProperty("gallery_image_ids")
    private List<String> images;
    @BsonIgnore
    private List<Product> products;
    @BsonProperty("product_ids")
    @JsonIgnore
    private List<ObjectId> productsAsObjectId;
    @JsonProperty("product_ids")
    @BsonIgnore
    private List<String> productIds;

    public List<String> getImages() {
        return images;
    }

    public Catalog setImages(List<String> images) {
        this.images = images;
        return this;
    }

    public CollectionVisibility getVisible() {
        return visible;
    }

    public Catalog setVisible(CollectionVisibility visible) {
        this.visible = visible;
        return this;
    }

    public ObjectId getCatalogId() {
        return catalogId;
    }

    public Catalog setCatalogId(ObjectId catalogId) {
        this.catalogId = catalogId;
        return this;
    }

    public synchronized List<ObjectId> getProductsAsObjectId() {
        if (null == productsAsObjectId) {
            if (productIds != null && !productIds.isEmpty())
                productsAsObjectId = productIds.stream().map(s -> new ObjectId(s)).collect(Collectors.toList());
            else if (products != null && !products.isEmpty())
                productsAsObjectId = products.stream().map(p -> p.getProductObjectId()).collect(Collectors.toList());
            else productsAsObjectId = new ArrayList<>();
        }
        return productsAsObjectId;
    }

    public Catalog setProductsAsObjectId(List<ObjectId> productsAsObjectId) {
        this.productsAsObjectId = productsAsObjectId;
        return this;
    }

    public synchronized List<String> getProductIds() {
        if (null == productIds) {
            if (productsAsObjectId != null && !productsAsObjectId.isEmpty())
                productIds = productsAsObjectId.stream().map(objectId -> objectId.toString()).collect(Collectors.toList());
            else if (products != null && !products.isEmpty())
                productIds = products.stream().map(p -> p.getProductId()).collect(Collectors.toList());
            else productIds = new ArrayList<>();
        }
        return productIds;
    }

    public Catalog setProductIds(List<String> productIds) {
        this.productIds = productIds;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Catalog setCode(String code) {
        this.code = code;
        return this;
    }

    public String getName() {
        return name;
    }

    public Catalog setName(String name) {
        this.name = name;
        return this;
    }

    public String getSlug() {
        return slug;
    }

    public Catalog setSlug(String slug) {
        this.slug = slug;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Catalog setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<Product> getProducts() {
        return products;
    }

    public Catalog setProducts(List<Product> products) {
        this.products = products;
        return this;
    }
}
