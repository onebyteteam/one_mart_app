package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.enums.AccountType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by thangpham on 24/02/2018.
 */
public class Authorization {

    private int hash;

    @BsonProperty("account_type")
    private AccountType role;
    private Boolean active;
    @BsonIgnore
    private Set<String> permissions;
    @JsonIgnore
    @BsonProperty("authorizations")
    private Set<Integer> permissionIds;

    public Set<Integer> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(Set<Integer> permissionIds) {
        this.permissionIds = permissionIds;
    }

    public AccountType getRole() {
        return role;
    }

    public void setRole(AccountType role) {
        this.role = role;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public int hashCode() {
        int h = hash;
        if (h == 0) {
            h = h + 45 * role.hashCode();
            h += 40 * active.hashCode();
            h += permissions.stream().map(s -> s.hashCode()).collect(Collectors.summingInt(value -> value));
        }
        return h;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj || !(obj instanceof Authorization)) return false;
        Authorization cast = (Authorization) obj;
        if (this.role != cast.role) return false;
        if (this.active != cast.active) return false;
        if ((null == this.permissions && null != cast.permissions) || null != this.permissions && null == cast.permissions)
            return false;
        if (this.permissions.size() != cast.permissions.size()) return false;
        if (!this.permissions.containsAll(cast.permissions)) return false;
        return true;
    }
}
