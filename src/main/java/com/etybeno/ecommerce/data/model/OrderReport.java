package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.enums.OrderStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by thangpham on 13/04/2018.
 */
public class OrderReport implements Serializable{

    @BsonProperty("_id")
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("product_num")
    @BsonProperty("product_num")
    private Integer productNum;
    @JsonProperty("total_amount")
    @BsonProperty("total_amount")
    private Double totalAmount;
    @JsonProperty("created_date")
    @BsonProperty("created_date")
    private Date createdDate;
    @JsonProperty("modified_date")
    @BsonProperty("modified_date")
    private Date modifiedDate;
    @JsonProperty("order_status")
    @BsonProperty("order_status")
    private OrderStatus orderStatus;

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getProductNum() {
        return productNum;
    }

    public void setProductNum(Integer productNum) {
        this.productNum = productNum;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
