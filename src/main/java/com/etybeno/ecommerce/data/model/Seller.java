package com.etybeno.ecommerce.data.model;

import com.etybeno.common.util.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;

/**
 * Created by thangpham on 17/05/2018.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Seller implements Serializable{

    @BsonIgnore
    @JsonProperty("seller_id")
    private String sellerId;
    @JsonIgnore
    @BsonProperty("seller_id")
    private ObjectId sellerObjectId;

    private String name;
    private String slug;
    private String logo;
    private String address;
    private String phone;
    private String description;

    public String getSellerId() {
        if(null == sellerId && sellerObjectId != null) sellerId = sellerObjectId.toHexString();
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public ObjectId getSellerObjectId() {
        if(null == sellerObjectId && !StringUtil.isNullOrEmpty(sellerId)) sellerObjectId = new ObjectId(sellerId);
        return sellerObjectId;
    }

    public void setSellerObjectId(ObjectId sellerObjectId) {
        this.sellerObjectId = sellerObjectId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
