package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.enums.LoginType;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 24/02/2018.
 */
public class Authentication {

    private String identification;
    private String validation;
    @BsonProperty("login_type")
    @JsonProperty("login_type")
    private LoginType loginType;

    public String getIdentification() {
        return identification;
    }

    public Authentication setIdentification(String identification) {
        this.identification = identification;
        return this;
    }

    public String getValidation() {
        return validation;
    }

    public Authentication setValidation(String validation) {
        this.validation = validation;
        return this;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public Authentication setLoginType(LoginType loginType) {
        this.loginType = loginType;
        return this;
    }
}
