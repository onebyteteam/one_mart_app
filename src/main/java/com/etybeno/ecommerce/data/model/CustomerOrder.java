package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.entity.ShippingAddress;
import com.etybeno.ecommerce.data.enums.OrderStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * Created by thangpham on 05/07/2018.
 */
public class CustomerOrder {

    @BsonProperty("order_id")
    @JsonProperty("order_id")
    private String orderId;
    @JsonIgnore
    @BsonProperty("user_id")
    private ObjectId userId;
    @JsonProperty("display_name")
    private String displayName;
    private String note;
    @JsonProperty("shipping_address")
    private ShippingAddress shippingAddress;
    @JsonProperty("short_products_name")
    private String shortProductNames;
    @JsonProperty("created_date")
    private Date createdDate;
    @JsonProperty("total_amount")
    private Double totalAmount;
    @JsonProperty("order_status")
    private OrderStatus orderStatus;
    private List<CustomerOrderProduct> orderProducts;

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public CustomerOrder setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public CustomerOrder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public String getNote() {
        return note;
    }

    public CustomerOrder setNote(String note) {
        this.note = note;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public CustomerOrder setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public CustomerOrder setUserId(ObjectId userId) {
        this.userId = userId;
        return this;
    }

    public String getShortProductNames() {
        return shortProductNames;
    }

    public CustomerOrder setShortProductNames(String shortProductNames) {
        this.shortProductNames = shortProductNames;
        return this;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public CustomerOrder setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public CustomerOrder setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public CustomerOrder setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public List<CustomerOrderProduct> getOrderProducts() {
        return orderProducts;
    }

    public CustomerOrder setOrderProducts(List<CustomerOrderProduct> orderProducts) {
        this.orderProducts = orderProducts;
        return this;
    }
}
