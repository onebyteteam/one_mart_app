package com.etybeno.ecommerce.data.model;

import org.bson.codecs.pojo.annotations.BsonProperty;

import java.io.Serializable;

/**
 * Created by thangpham on 05/04/2018.
 */
public class SaleDataReport implements Serializable{

    @BsonProperty("_id")
    private Object key;
    private int order;
    private int sale;
    private double revenue;
    private double profit;

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getSale() {
        return sale;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
}
