package com.etybeno.ecommerce.data.model;

import com.etybeno.common.util.StringUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class works on cookie value pattern present as key-value.
 * The pattern is key:value|key1:value1...
 * Created by thangpham on 21/03/2018.
 */
public class CookieMapValue {

    private Map<String, String> dataMap = null;

    public CookieMapValue(String src) {
        dataMap = parseStringToMap(src);
    }

    public CookieMapValue() {
        dataMap = new HashMap<>();
    }

    public String formatToString() {
        return formatMapToString(dataMap);
    }

    public byte[] formatToBytes() {
        return formatToString().getBytes();
    }

    public CookieMapValue addDouble(String key, double value) {
        String valueStr = dataMap.get(key);
        if (null == valueStr) dataMap.put(key, value + "");
        else dataMap.put(key, (StringUtil.safeParseDouble(valueStr) + value) + "");
        return this;
    }

    public CookieMapValue putDouble(String key, double value) {
        dataMap.put(key, value + "");
        return this;
    }

    public CookieMapValue addInt(String key, int value) {
        String valueStr = dataMap.get(key);
        if (null == valueStr) dataMap.put(key, value + "");
        else dataMap.put(key, (StringUtil.safeParseInt(valueStr) + value) + "");
        return this;
    }

    public CookieMapValue putInt(String key, int value) {
        dataMap.put(key, value + "");
        return this;
    }

    public CookieMapValue putString(String key, String value) {
        dataMap.put(key, value);
        return this;
    }

    public String getCartDataValue(String key) {
        return dataMap.get(key);
    }

    /**
     * This source string must have pattern as key:value|key1:value1 and so on
     *
     * @param src
     * @return
     */
    private Map<String, String> parseStringToMap(String src) {
        String[] split = src.split("\\|");
        Map<String, String> keyValues = new HashMap<>();
        for (String part : split) {
            String[] pair = part.split(":");
            if (pair.length == 2) keyValues.put(pair[0], pair[1]);
        }
        return keyValues;
    }

    private String formatMapToString(Map<String, String> src) {
        return src.keySet().stream()
                .map(e -> e + ":" + src.get(e))
                .collect(Collectors.joining("|"));
    }
}
