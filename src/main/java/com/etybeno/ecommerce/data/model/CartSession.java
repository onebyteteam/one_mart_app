package com.etybeno.ecommerce.data.model;

import com.etybeno.common.util.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.List;

/**
 * Created by thangpham on 09/03/2018.
 */
public class CartSession implements Serializable {

    @BsonIgnore
    @JsonProperty("cart_id")
    private String cartId;
    @JsonIgnore
    @BsonProperty("_id")
    private ObjectId cartIdAsObjectId;
    @JsonIgnore
    @BsonProperty("user_id")
    private ObjectId userId;
    @JsonProperty("session_id")
    @BsonProperty("session_id")
    private String sessionId;
    @BsonIgnore
    @JsonProperty("total_sum")
    private Double totalSum = 0.0;
    @JsonProperty("cart_products")
    @BsonProperty("cart_products")
    private List<CartProductSession> cartProducts;

    public Double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }

    public List<CartProductSession> getCartProducts() {
        return cartProducts;
    }

    public void setCartProducts(List<CartProductSession> cartProducts) {
        this.cartProducts = cartProducts;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCartId() {
        if (StringUtil.isNullOrEmpty(cartId) && cartIdAsObjectId != null) cartId = cartIdAsObjectId.toHexString();
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public ObjectId getCartIdAsObjectId() {
        return cartIdAsObjectId;
    }

    public void setCartIdAsObjectId(ObjectId cartIdAsObjectId) {
        this.cartIdAsObjectId = cartIdAsObjectId;
    }

}
