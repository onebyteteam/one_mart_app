package com.etybeno.ecommerce.data.model;

import com.etybeno.ecommerce.data.enums.ProductVisibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.*;

/**
 * Created by thangpham on 06/01/2018.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Product implements Serializable {

    @JsonIgnore
    @BsonProperty("_id")
    private ObjectId productObjectId;
    @BsonIgnore
    @JsonProperty("product_id")
    private String productId;
    private String sku;
    private String name;
    private String slug;
    @JsonProperty("short_description")
    @BsonProperty("short_description")
    private String shortDescription;
    private String description;
    private Double price;
    private ProductVisibility visibility = ProductVisibility.VISIBLE;
    @JsonProperty("sale_price")
    @BsonProperty("sale_price")
    private Double salePrice;
    @JsonProperty("regular_price")
    @BsonProperty("regular_price")
    private Double regularPrice;
    @BsonProperty("gallery_image_ids")
    private List<String> images;
    @BsonIgnore
    private Seller seller;
    @JsonIgnore
    @BsonProperty("seller_id")
    private ObjectId sellerId;
    private Integer stockLevel;
    @BsonProperty("manage_stock")
    @JsonProperty("manage_stock")
    private boolean manageStock;
    @BsonProperty("stock_status")
    @JsonProperty("stock_status")
    private String stockStatus;
    @BsonProperty("stock_quantity")
    @JsonProperty("stock_quantity")
    private Integer stockQuantity;
    private String locale;

    @JsonProperty("i18n_slug")
    private Map<String, String> i18nSlug;
    @JsonProperty("i18n_name")
    private Map<String, String> i18nName;
    @JsonProperty("i18n_short_description")
    private Map<String, String> i18nShortDescription;
    @JsonProperty("i18n_description")
    private Map<String, String> i18nDescription;

    @BsonIgnore
    @JsonProperty("breadcrum")
    private List<Category> breadcrum;

    @BsonIgnore
    @JsonProperty("originator_key")
    private Attribute originatorKey;
    @BsonIgnore
    @JsonProperty("originator_values")
    private Set<Attribute> originatorValues;

    @BsonProperty("category_ids")
    @JsonIgnore
    private Set<ObjectId> categoriesAsObjectId;
    @JsonProperty("category_ids")
    @BsonIgnore
    private List<String> categoryIds;
    @BsonIgnore
    private List<Category> categories;

    @JsonIgnore
    @BsonProperty("originators")
    private Map<String, Set<ObjectId>> originatorsAsObjectId;

    @JsonIgnore
    @BsonProperty("option_attr_ids")
    private Map<String, Set<ObjectId>> optionsAsObjectId;

    public Map<String, String> getI18nSlug() {
        return i18nSlug;
    }

    public Product setI18nSlug(Map<String, String> i18nSlug) {
        this.i18nSlug = i18nSlug;
        return this;
    }

    public Map<String, String> getI18nName() {
        return i18nName;
    }

    public Product setI18nName(Map<String, String> i18nName) {
        this.i18nName = i18nName;
        return this;
    }

    public Map<String, String> getI18nShortDescription() {
        return i18nShortDescription;
    }

    public Product setI18nShortDescription(Map<String, String> i18nShortDescription) {
        this.i18nShortDescription = i18nShortDescription;
        return this;
    }

    public Map<String, String> getI18nDescription() {
        return i18nDescription;
    }

    public Product setI18nDescription(Map<String, String> i18nDescription) {
        this.i18nDescription = i18nDescription;
        return this;
    }

    public boolean setManageStock() {
        return manageStock;
    }

    public Product setManageStock(boolean manageStock) {
        this.manageStock = manageStock;
        return this;
    }

    public String getStockStatus() {
        return stockStatus;
    }

    public Product setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
        return this;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public Product setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
        return this;
    }

    public Integer getStockLevel() {
        return stockLevel;
    }

    public Product setStockLevel(Integer stockLevel) {
        this.stockLevel = stockLevel;
        return this;
    }

    public Double getRegularPrice() {
        return regularPrice;
    }

    public Product setRegularPrice(Double regularPrice) {
        this.regularPrice = regularPrice;
        return this;
    }

    public List<Category> getBreadcrum() {
        return breadcrum;
    }

    public Product setBreadcrum(List<Category> breadcrum) {
        this.breadcrum = breadcrum;
        return this;
    }

    public Attribute getOriginatorKey() {
        return originatorKey;
    }

    public Product setOriginatorKey(Attribute originatorKey) {
        this.originatorKey = originatorKey;
        return this;
    }

    public Set<Attribute> getOriginatorValues() {
        return originatorValues;
    }

    public Product setOriginatorValues(Set<Attribute> originatorValues) {
        this.originatorValues = originatorValues;
        return this;
    }

    public String getLocale() {
        return locale;
    }

    public Product setLocale(String locale) {
        this.locale = locale;
        return this;
    }

    public Map<String, Set<ObjectId>> getOriginatorsAsObjectId() {
        return originatorsAsObjectId;
    }

    public Product setOriginatorsAsObjectId(Map<String, Set<ObjectId>> originatorsAsObjectId) {
        this.originatorsAsObjectId = originatorsAsObjectId;
        return this;
    }

    public Map<String, Set<ObjectId>> getOptionsAsObjectId() {
        return optionsAsObjectId;
    }

    public Product setOptionsAsObjectId(Map<String, Set<ObjectId>> optionsAsObjectId) {
        this.optionsAsObjectId = optionsAsObjectId;
        return this;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public Product setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public Seller getSeller() {
        return seller;
    }

    public Product setSeller(Seller seller) {
        this.seller = seller;
        return this;
    }

    public ObjectId getSellerId() {
        return sellerId;
    }

    public Product setSellerId(ObjectId sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    public ProductVisibility getVisibility() {
        return visibility;
    }

    public Product setVisibility(ProductVisibility visibility) {
        this.visibility = visibility;
        return this;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public Product setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
        return this;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Product setCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Product addCategory(Category... categories) {
        if (this.categories == null) this.categories = new ArrayList<>();
        this.categories.addAll(Arrays.asList(categories));
        return this;
    }

    public Set<ObjectId> getCategoriesAsObjectId() {
        return categoriesAsObjectId;
    }

    public Product setCategoriesAsObjectId(Set<ObjectId> categoriesAsObjectId) {
        this.categoriesAsObjectId = categoriesAsObjectId;
        return this;
    }

    public String getProductId() {
        if (null == productId && productObjectId != null) productId = productObjectId.toString();
        return productId;
    }

    public Product setProductId(String productId) {
        this.productId = productId;
        return this;
    }

    public ObjectId getProductObjectId() {
        return productObjectId;
    }

    public Product setProductObjectId(ObjectId productObjectId) {
        this.productObjectId = productObjectId;
        return this;
    }

    public String getSlug() {
        return slug;
    }

    public Product setSlug(String slug) {
        this.slug = slug;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public Product setPrice(Double price) {
        this.price = price;
        return this;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public Product setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }


    public String getSku() {
        return sku;
    }

    public Product setSku(String sku) {
        this.sku = sku;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getImages() {
        return images;
    }

    public Product setImages(List<String> images) {
        this.images = images;
        return this;
    }
}
