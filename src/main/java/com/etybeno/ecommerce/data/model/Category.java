package com.etybeno.ecommerce.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.*;

/**
 * Created by thangpham on 26/12/2017.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Category implements Serializable {

    @BsonProperty("category_id")
    @JsonIgnore
    private ObjectId categoryObjectId;
    @BsonIgnore
    @JsonProperty("category_id")
    private String categoryId;
    private String name;
    private String slug;
    private String description;
    @BsonIgnore
    private List<Attribute> attributes;
    @BsonProperty("select_id")
    @JsonProperty("select_id")
    private int selectId;

    @BsonProperty("i18n_slug")
    @JsonProperty("i18n_slug")
    private Map<String, String> i18nSlug;
    @BsonProperty("i18n_name")
    @JsonProperty("i18n_name")
    private Map<String, String> i18nName;
    @BsonProperty("i18n_description")
    @JsonProperty("i18n_description")
    private Map<String, String> i18nDescription;

    private Category parent;
    private Set<Category> children;

    @BsonProperty("parent_id")
    @JsonIgnore
    private ObjectId parentObjectId;
    @BsonIgnore
    @JsonProperty("parent_id")
    private String parentId;

    public Category() {
    }

    public Category(ObjectId categoryId, String slug, String name, String description) {
        this.categoryObjectId = categoryId;
        this.slug = slug;
        this.name = name;
        this.description = description;
    }

    public Category(ObjectId categoryObjectId, Map<String, String> i18nSlug, Map<String, String> i18nName, Map<String, String> i18nDescription) {
        this.categoryObjectId = categoryObjectId;
        this.i18nSlug = i18nSlug;
        this.i18nName = i18nName;
        this.i18nDescription = i18nDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getI18nSlug() {
        return i18nSlug;
    }

    public void setI18nSlug(Map<String, String> i18nSlug) {
        this.i18nSlug = i18nSlug;
    }

    public Map<String, String> getI18nName() {
        return i18nName;
    }

    public void setI18nName(Map<String, String> i18nName) {
        this.i18nName = i18nName;
    }

    public Map<String, String> getI18nDescription() {
        return i18nDescription;
    }

    public void setI18nDescription(Map<String, String> i18nDescription) {
        this.i18nDescription = i18nDescription;
    }

    public int getSelectId() {
        return selectId;
    }

    public void setSelectId(int selectId) {
        this.selectId = selectId;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getCategoryId() {
        if (null == categoryId && categoryObjectId != null) categoryId = categoryObjectId.toString();
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getParentId() {
        if (null == parentId && parentObjectId != null) parentId = parentObjectId.toString();
        return parentId;
    }

    public Category setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public ObjectId getCategoryObjectId() {
        return categoryObjectId;
    }

    public Category setCategoryObjectId(ObjectId categoryObjectId) {
        this.categoryObjectId = categoryObjectId;
        return this;
    }

    public Category getParent() {
        return parent;
    }

    public Category setParent(Category parent) {
        this.parent = parent;
        return this;
    }

    public Set<Category> getChildren() {
        if (null == children) children = new HashSet<>();
        return children;
    }

    public Category setChildren(Collection<Category> children) {
        this.children = new HashSet<>(children);
        return this;
    }

    public Category addChild(Category... categories) {
        if (null == this.children) this.children = new HashSet<>();
        this.children.addAll(Arrays.asList(categories));
        return this;
    }

    public ObjectId getParentObjectId() {
        return parentObjectId;
    }

    public Category setParentObjectId(ObjectId parentObjectId) {
        this.parentObjectId = parentObjectId;
        return this;
    }

    @Override
    public int hashCode() {
        if (null != this.categoryObjectId) return this.categoryObjectId.hashCode();
        else if (null != this.categoryId) return this.categoryId.hashCode();
        else return this.i18nName.hashCode();
    }
}
