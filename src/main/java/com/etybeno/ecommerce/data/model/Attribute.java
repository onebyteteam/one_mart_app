package com.etybeno.ecommerce.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.tomcat.jni.Local;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.*;

/**
 * Created by thangpham on 02/05/2018.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Attribute implements Serializable, Cloneable {

    @BsonProperty("attribute_id")
    @JsonIgnore
    private ObjectId attributeObjectId;
    @BsonIgnore
    @JsonProperty("attribute_id")
    private transient String attributeId;
    private String name;
    private String slug;
    private String description;
    @BsonProperty("select_id")
    @JsonProperty("select_id")
    private int selectId;

    @BsonProperty("i18n_slug")
    @JsonProperty("i18n_slug")
    private Map<String, String> i18nSlug;
    @BsonProperty("i18n_name")
    @JsonProperty("i18n_name")
    private Map<String, String> i18nName;
    @BsonProperty("i18n_description")
    @JsonProperty("i18n_description")
    private Map<String, String> i18nDescription;
    @BsonIgnore
    private Attribute parent;
    @BsonIgnore
    private Set<Attribute> children;

    @BsonIgnore
    private String locale;

    @BsonProperty("parent_id")
    @JsonIgnore
    private ObjectId parentObjectId;
    @BsonIgnore
    @JsonProperty("parent_id")
    private transient String parentId;

    public Attribute() {
    }

    public Attribute(ObjectId attributeObjectId, String slug, String name, String description, String locale) {
        this.attributeObjectId = attributeObjectId;
        this.name = name;
        this.slug = slug;
        this.description = description;
        this.locale = locale;
    }

    public Attribute(ObjectId attributeObjectId, String slug, String name, String description, String locale, int selectId) {
        this.attributeObjectId = attributeObjectId;
        this.name = name;
        this.slug = slug;
        this.description = description;
        this.selectId = selectId;
        this.locale = locale;
    }

    public Attribute(ObjectId attributeObjectId, Map<String, String> i18nSlug, Map<String, String> i18nName, Map<String, String> i18nDescription) {
        this.attributeObjectId = attributeObjectId;
        this.i18nSlug = i18nSlug;
        this.i18nName = i18nName;
        this.i18nDescription = i18nDescription;
    }

    public Attribute(String slug, String name, String description, String locale, ObjectId parentId) {
        this.name = name;
        this.slug = slug;
        this.description = description;
        this.locale = locale;
        this.parentObjectId = parentId;
    }

    public Map<String, String> getI18nSlug() {
        return i18nSlug;
    }

    public void setI18nSlug(Map<String, String> i18nSlug) {
        this.i18nSlug = i18nSlug;
    }

    public Map<String, String> getI18nName() {
        return i18nName;
    }

    public void setI18nName(Map<String, String> i18nName) {
        this.i18nName = i18nName;
    }

    public Map<String, String> getI18nDescription() {
        return i18nDescription;
    }

    public void setI18nDescription(Map<String, String> i18nDescription) {
        this.i18nDescription = i18nDescription;
    }

    public int getSelectId() {
        return selectId;
    }

    public void setSelectId(int selectId) {
        this.selectId = selectId;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Attribute getParent() {
        return parent;
    }

    public void setParent(Attribute parent) {
        this.parent = parent;
    }

    public ObjectId getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(ObjectId parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Set<Attribute> getChildren() {
        return children;
    }

    public Attribute setChildren(Set<Attribute> children) {
        this.children = children;
        return this;
    }

    public Attribute addChild(Attribute child) {
        if(null == this.children) this.children = new HashSet<>();
        this.children.add(child);
        return this;
    }

    public Attribute addChildren(List<Attribute> children) {
        if(null == this.children) this.children = new HashSet<>();
        this.children.addAll(children);
        return this;
    }

    public Attribute addChildren(Attribute... children) {
        if(null == this.children) this.children = new HashSet<>();
        this.children.addAll(Arrays.asList(children));
        return this;
    }

    public ObjectId getAttributeObjectId() {
        return attributeObjectId;
    }

    public void setAttributeObjectId(ObjectId attributeObjectId) {
        this.attributeObjectId = attributeObjectId;
    }

    public String getAttributeId() {
        if(null == attributeId && attributeObjectId != null) attributeId = attributeObjectId.toString();
        return attributeId;
    }

    public void setAttributeId(String traitId) {
        this.attributeId = traitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        if(null != this.attributeObjectId) return this.attributeObjectId.hashCode();
        else if(null != this.attributeId) return this.attributeId.hashCode();
        else return this.slug.hashCode();
    }
}
