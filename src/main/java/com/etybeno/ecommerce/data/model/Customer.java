package com.etybeno.ecommerce.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.io.Serializable;
import java.util.*;

/**
 * Created by thangpham on 31/01/2018.
 */
public class Customer implements Serializable {

    private String username;
    @JsonProperty("display_name")
    @BsonProperty("display_name")
    private String displayName;
    private String email;
    @BsonProperty("first_name")
    @JsonProperty("first_name")
    private String firstName;
    @BsonProperty("last_name")
    @JsonProperty("last_name")
    private String lastName;
    @BsonProperty("avatar_uri")
    @JsonProperty("avatar_uri")
    private String avatarUri;
    private List<Authentication> authentications = new ArrayList<>();
    private List<Authorization> authorizations = new ArrayList<>();

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Authentication> getAuthentications() {
        return authentications;
    }

    public Customer addAuthentications(Authentication... authentications) {
        this.authentications.addAll(Arrays.asList(authentications));
        return this;
    }

    public Customer setAuthentications(List<Authentication> authentications) {
        this.authentications = authentications;
        return this;
    }

    public List<Authorization> getAuthorizations() {
        return authorizations;
    }

    public Customer addAuthorizations(Authorization... authorizations) {
        this.authorizations.addAll(Arrays.asList(authorizations));
        return this;
    }

    public Customer setAuthorizations(List<Authorization> authorizations) {
        this.authorizations = authorizations;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Customer setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customer setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Customer setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Customer setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
}
