package com.etybeno.ecommerce.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.io.Serializable;

/**
 * Created by thangpham on 11/03/2018.
 */
public class CartProductSession implements Serializable {

    @JsonIgnore
    @BsonProperty("_id")
    private ObjectId idAsObjectId;
    @JsonIgnore
    @BsonProperty("product_id")
    private ObjectId productIdAsObjectId;
    @BsonIgnore
    private Product product;
    private Integer quantity;
    @BsonIgnore
    private Double total;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public ObjectId getIdAsObjectId() {
        return idAsObjectId;
    }

    public void setIdAsObjectId(ObjectId idAsObjectId) {
        this.idAsObjectId = idAsObjectId;
    }

    public ObjectId getProductIdAsObjectId() {
        return productIdAsObjectId;
    }

    public void setProductIdAsObjectId(ObjectId productIdAsObjectId) {
        this.productIdAsObjectId = productIdAsObjectId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
