package com.etybeno.ecommerce.data.imported;

import java.util.List;

/**
 * Created by thangpham on 09/06/2018.
 */
public class ImportedAttribute {

    private String name;
    private String language;
    private List<String> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
