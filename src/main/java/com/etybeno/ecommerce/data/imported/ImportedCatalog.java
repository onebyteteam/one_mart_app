package com.etybeno.ecommerce.data.imported;

import com.etybeno.ecommerce.data.enums.CollectionVisibility;

import java.util.List;

/**
 * Created by thangpham on 17/02/2018.
 */
public class ImportedCatalog {

    private String name;
    private String slug;
    private String code;
    private String description;
    private CollectionVisibility visible;
    private List<String> images;
    private List<ImportedProduct> products;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public CollectionVisibility getVisible() {
        return visible;
    }

    public void setVisible(CollectionVisibility visible) {
        this.visible = visible;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ImportedProduct> getProducts() {
        return products;
    }

    public void setProducts(List<ImportedProduct> products) {
        this.products = products;
    }
}
