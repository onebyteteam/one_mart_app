package com.etybeno.ecommerce.data.imported;

import java.util.List;

/**
 * Created by thangpham on 07/05/2018.
 */
public class ImportedProductTrait {

    private String type;
    private String name;
    private List<String> values;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}