package com.etybeno.ecommerce.data.imported;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

/**
 * Created by thangpham on 04/05/2018.
 */
public class ImportedCateAtt {

    @JsonProperty("category_slug")
    private String cateSlug;
    private List<ImportedAttr> attributes;
    @JsonProperty("apply_to_children")
    private boolean applyToChildren;
    @JsonProperty("apply_to_parent")
    private boolean applyToParent;

    public boolean isApplyToChildren() {
        return applyToChildren;
    }

    public void setApplyToChildren(boolean applyToChildren) {
        this.applyToChildren = applyToChildren;
    }

    public boolean isApplyToParent() {
        return applyToParent;
    }

    public void setApplyToParent(boolean applyToParent) {
        this.applyToParent = applyToParent;
    }

    public String getCateSlug() {
        return cateSlug;
    }

    public void setCateSlug(String cateSlug) {
        this.cateSlug = cateSlug;
    }

    public List<ImportedAttr> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<ImportedAttr> attributes) {
        this.attributes = attributes;
    }

    public static class ImportedAttr {
        private int group;
        private String name;
        private List<String> values;

        public int getGroup() {
            return group;
        }

        public void setGroup(int group) {
            this.group = group;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getValues() {
            return values;
        }

        public void setValues(List<String> values) {
            this.values = values;
        }
    }
}
