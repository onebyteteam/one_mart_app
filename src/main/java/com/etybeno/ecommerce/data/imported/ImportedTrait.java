package com.etybeno.ecommerce.data.imported;

import java.util.List;

/**
 * Created by thangpham on 04/06/2018.
 */
public class ImportedTrait {

    private String slug;
    private String name;
    private String description;
    private String type;
    private List<ImportedTrait> children;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ImportedTrait> getChildren() {
        return children;
    }

    public void setChildren(List<ImportedTrait> children) {
        this.children = children;
    }
}
