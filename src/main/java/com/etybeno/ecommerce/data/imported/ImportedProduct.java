package com.etybeno.ecommerce.data.imported;

import com.etybeno.ecommerce.data.enums.ProductVisibility;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

/**
 * Created by thangpham on 10/02/2018.
 */
public class ImportedProduct {

    private String sku;
    private String name;
    private String description;
    @JsonProperty("short_description")
    private String shortDescription;
    private String supplier;
    private Double price;
    @JsonProperty("use_sku")
    private boolean useSku;
    @JsonProperty("sale_price")
    private Double salePrice;
    private List<String> images;
    @JsonProperty("categories_slug")
    private List<String> categoriesSlug;
    private Map<String, List<String>> originators;
    @JsonProperty("option_traits")
    private Map<String, List<String>> optionTraits;
    @JsonProperty("product_type")
    private ProductVisibility visibility = ProductVisibility.VISIBLE;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Map<String, List<String>> getOriginators() {
        return originators;
    }

    public void setOriginators(Map<String, List<String>> originators) {
        this.originators = originators;
    }

    public Map<String, List<String>> getOptionTraits() {
        return optionTraits;
    }

    public void setOptionTraits(Map<String, List<String>> optionTraits) {
        this.optionTraits = optionTraits;
    }

    public ProductVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(ProductVisibility visibility) {
        this.visibility = visibility;
    }

    public Boolean getUseSku() {
        return useSku;
    }

    public void setUseSku(Boolean useSku) {
        this.useSku = useSku;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getCategoriesSlug() {
        return categoriesSlug;
    }

    public void setCategoriesSlug(List<String> categoriesSlug) {
        this.categoriesSlug = categoriesSlug;
    }

}
