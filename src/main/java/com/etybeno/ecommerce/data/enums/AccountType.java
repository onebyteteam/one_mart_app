package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 31/01/2018.
 */
public enum AccountType {

    @JsonProperty("CUSTOMER")
    @BsonProperty("CUSTOMER")
    CUSTOMER("CUSTOMER"),
    @JsonProperty("DISTRIBUTOR")
    @BsonProperty("DISTRIBUTOR")
    DISTRIBUTOR("DISTRIBUTOR"),
    @JsonProperty("ADMINISTRATOR")
    @BsonProperty("ADMINISTRATOR")
    ADMINISTRATOR("ADMINISTRATOR");

    private String value;

    private AccountType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }

}
