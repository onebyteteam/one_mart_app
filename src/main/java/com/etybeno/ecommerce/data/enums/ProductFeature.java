package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by thangpham on 01/04/2018.
 */
public enum ProductFeature {

    @JsonProperty("BEST_SELLER")
    BEST_SELLER("BEST_SELLER"),
    @JsonProperty("NEW_ARRIVAL")
    NEW_ARRIVAL("NEW_ARRIVAL"),
    @JsonProperty("TOP_RATED")
    TOP_RATED("TOP_RATED"),
    @JsonProperty("ON_SALE")
    ON_SALE("ON_SALE");

    private String value;

    private ProductFeature(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }


}
