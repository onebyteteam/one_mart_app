package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 31/01/2018.
 */
public enum LoginType {

    @JsonProperty("GOOGLE")
    @BsonProperty("GOOGLE")
    GOOGLE("GOOGLE"),
    @JsonProperty("TWITTER")
    @BsonProperty("TWITTER")
    TWITTER("TWITTER"),
    @JsonProperty("FACEBOOK")
    @BsonProperty("FACEBOOK")
    FACEBOOK("FACEBOOK"),
    @JsonProperty("INSTAGRAM")
    @BsonProperty("INSTAGRAM")
    INSTAGRAM("INSTAGRAM"),
    @JsonProperty("ZALO")
    @BsonProperty("ZALO")
    ZALO("ZALO"),
    @JsonProperty("REGISTRY")
    @BsonProperty("REGISTRY")
    REGISTRY("REGISTRY");

    private String value;

    private LoginType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
