package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.io.Serializable;

/**
 * Created by thangpham on 12/02/2018.
 */

public enum ProductVisibility implements Serializable{

    @JsonProperty("VISIBLE")
    @BsonProperty("VISIBLE")
    VISIBLE("VISIBLE"),
    @JsonProperty("CATALOGUE")
    @BsonProperty("CATALOGUE")
    CATALOGUE("CATALOGUE"),
    @JsonProperty("HIDDEN")
    @BsonProperty("HIDDEN")
    HIDDEN("HIDDEN"),
    @JsonProperty("SEARCH")
    @BsonProperty("SEARCH")
    SEARCH("SEARCH");

    private String value;

    private ProductVisibility(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
