package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 09/06/2018.
 */
public enum ProductType {

    @JsonProperty("NORMAL_PRODUCT")
    @BsonProperty("NORMAL_PRODUCT")
    NORMAL_PRODUCT("NORMAL_PRODUCT"),
    @JsonProperty("VARIABLE_PRODUCT")
    @BsonProperty("VARIABLE_PRODUCT")
    VARIABLE_PRODUCT("VARIABLE_PRODUCT");

    private String value;

    private ProductType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
