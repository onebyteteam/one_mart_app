package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by thangpham on 03/05/2018.
 */
public enum AttributeType {

    @JsonProperty("ORI_AUTHOR")
    ORI_AUTHOR("ORI_AUTHOR"),
    @JsonProperty("ORI_BRANCH")
    ORI_BRANCH("ORI_BRANCH"),
    @JsonProperty("PRODUCER")
    PRODUCER("PRODUCER"),
    @JsonProperty("MADE_IN")
    MADE_IN("MADE_IN");

    private String value;

    private AttributeType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
