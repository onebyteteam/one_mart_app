package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 22/03/2018.
 */
public enum CartStatus {

    @JsonProperty("ACTIVE")
    @BsonProperty("ACTIVE")
    ACTIVE("ACTIVE"),
    @JsonProperty("EXPIRED")
    @BsonProperty("EXPIRED")
    EXPIRED("EXPIRED"),
    @JsonProperty("ORDERED")
    @BsonProperty("ORDERED")
    ORDERED("ORDERED");

    private String value;

    private CartStatus(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
