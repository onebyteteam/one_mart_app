package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by thangpham on 06/01/2018.
 */
public enum ProductStatus {

    @JsonProperty("PUBLISH")
    PUBLISH("PUBLISH"),
    @JsonProperty("OUT_OF_STOCK")
    OUT_OF_STOCK("OUT_OF_STOCK");

    private String value;

    private ProductStatus(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }

}
