package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 16/02/2018.
 */
public enum CollectionVisibility {

    @JsonProperty("VISIBLE")
    @BsonProperty("VISIBLE")
    VISIBLE("VISIBLE"),
    @JsonProperty("HIDDEN")
    @BsonProperty("HIDDEN")
    HIDDEN("HIDDEN");

    private String value;

    private CollectionVisibility(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
