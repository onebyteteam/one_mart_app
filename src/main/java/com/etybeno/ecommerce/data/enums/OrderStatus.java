package com.etybeno.ecommerce.data.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * Created by thangpham on 27/03/2018.
 */
public enum OrderStatus {

    @BsonProperty("AWAITING_FULFILMENT")
    @JsonProperty("AWAITING_FULFILMENT")
    AWAITING_FULFILMENT("AWAITING_FULFILMENT"),
    @BsonProperty("VERIFICATION_REQUIRED")
    @JsonProperty("VERIFICATION_REQUIRED")
    VERIFICATION_REQUIRED("VERIFICATION_REQUIRED"),
    @BsonProperty("AWAITING_SHIPMENT")
    @JsonProperty("AWAITING_SHIPMENT")
    AWAITING_SHIPMENT("AWAITING_SHIPMENT"),
    @BsonProperty("COMPLETED")
    @JsonProperty("COMPLETED")
    COMPLETED("COMPLETED"),
    @BsonProperty("CANCELLED")
    @JsonProperty("CANCELLED")
    CANCELLED("CANCELLED");

    private String value;

    private OrderStatus(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }
}
