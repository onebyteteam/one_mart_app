package com.etybeno.ecommerce.util;

import com.etybeno.common.util.HashUtil;
import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.data.custom.EcommerceUserDetails;
import com.etybeno.ecommerce.data.enums.LoginType;
import com.etybeno.ecommerce.data.model.CookieMapValue;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.Cookie;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static com.etybeno.ecommerce.Constant.SocialProvider.*;

/**
 * Created by thangpham on 25/02/2018.
 */
public class ConvenientUtil {

    public static EcommerceUserDetails getThisUserDetails() {
        try {
            return (EcommerceUserDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
        } catch (Exception ex) {
            return null;
        }
    }

    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static void reloadAuthentication() {

    }

    public static String getThisUsername() {
        EcommerceUserDetails thisUserDetails = getThisUserDetails();
        return null == thisUserDetails ? null : thisUserDetails.getUsername();
    }

    public static String generateTimeAndRandomHash() throws NoSuchAlgorithmException {
        return generateTimeAndRandomHash((int) (System.currentTimeMillis() / 1000));
    }

    public static String generateTimeAndRandomHash(int timeInSeconds) throws NoSuchAlgorithmException {
        String s = HashUtil.createHash(timeInSeconds + "", HashUtil.SHA1).asHexString().substring(0, 12);
        String s1 = RandomStringUtils.randomAlphanumeric(16, 16);
        return s + "-" + s1;
    }

    public static String buildTaxonomy(int type, String value) {
        if(1 == type) //Product Attribute
            return String.format("pa_%s", value);
        else
            return null;
    }

    public static String parseToSlug(String data) {
        return StringUtil.stripAccents(data.trim().toLowerCase()).replaceAll(" ","-");
    }

    public static LoginType parseProvider(String providerId) {
        switch (providerId) {
            case FACEBOOK:
                return LoginType.FACEBOOK;
            case GOOGLE:
                return LoginType.GOOGLE;
            case ZALO:
                return LoginType.ZALO;
            case INSTAGRAM:
                return LoginType.INSTAGRAM;
            default:
                return null;
        }
    }

    public static String loginTypeToSocialProvider(LoginType loginType) {
        if (LoginType.FACEBOOK == loginType) return FACEBOOK;
        else if (LoginType.GOOGLE == loginType) return GOOGLE;
        else if (LoginType.ZALO == loginType) return ZALO;
        else if (LoginType.INSTAGRAM == loginType) return INSTAGRAM;
        else return null;
    }

    public static Cookie buildServletCookie(String key, String value, int expiry) {
        Cookie cookie = new Cookie(key, value);
        cookie.setMaxAge(expiry);
        return cookie;
    }

    public static Cookie buildServletCookie(String key, String value, int expiry, String path) {
        Cookie cookie = new Cookie(key, value);
        cookie.setPath(path);
        cookie.setMaxAge(expiry);
        return cookie;
    }

    public static Cookie buildServletCookie(String key, String value, int expiry, String path, String domain) {
        Cookie cookie = new Cookie(key, value);
        cookie.setDomain(domain);
        cookie.setPath(path);
        cookie.setMaxAge(expiry);
        return cookie;
    }

    public static CookieMapValue parseBase64CookieMapValue(String src) {
        CookieMapValue mapValue = new CookieMapValue(StringUtil.isNullOrEmpty(src)?"":new String(Base64.getDecoder().decode(src)));
        return mapValue;
    }
}
