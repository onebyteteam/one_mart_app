package com.etybeno.ecommerce.controller;

import com.etybeno.common.enums.Status;
import com.etybeno.ecommerce.data.model.Product;
import com.etybeno.ecommerce.controller.rest.ProductRestController;
import org.bson.types.ObjectId;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by thangpham on 11/01/2018.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductRestControllerTest extends BaseRestControllerTest<ProductRestController> {

    @Test
    public void dummyTest() {

    }

//    @Test
    public void testCase1CreateNewProduct() throws Exception {
        Product product = createProductDTO();
        postPerform("/create", product).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(Status.SUCCESS.getValue()))
                .andExpect(jsonPath("$.data").value("OK"));
        //Try to insert a duplicate data
//        postPerform("/create", product).andExpect(status().isOk())
//                .andExpect(jsonPath("$.status").value(Status.ERROR.getValue()));
    }

//    @Test
    public void testCase2CreateProducts() throws Exception {
        Product product = createProductDTO();
        String name = product.getName();
        String slug = product.getSlug();
        String sku = product.getSku();
        for (int i = 0; i < 15; i++) {
            product.setSku(sku + i);
            product.setSlug(slug + "-" + i);
            product.setName(name + " " + i);
            postPerform("/create", product).andExpect(status().isOk())
                    .andExpect(jsonPath("$.status").value(Status.SUCCESS.getValue()))
                    .andExpect(jsonPath("$.data").value("OK"));
        }
    }

    private Product createProductDTO() {
        Product p = new Product();
        p.setName("Bao cao su");
        p.setSlug("bao-cao-su");
//        p.setCategoriesAsObjectId(Arrays.asList(new ObjectId("5a5a45d104dac07ff331bdd6")));
        p.setDescription("Bao cao su hay nhắm");
        p.setImages(Arrays.asList("27.jpg", "28.jpg", "29.jpg", "30.jpg", "31.jpg"));
        p.setSku("BCS101");
        p.setPrice(300000.0);
        p.setSalePrice(200000.0);
        return p;
    }

}
