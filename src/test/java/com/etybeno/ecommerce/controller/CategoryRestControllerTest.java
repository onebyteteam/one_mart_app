package com.etybeno.ecommerce.controller;

import com.etybeno.common.enums.Status;
import com.etybeno.ecommerce.data.model.Category;
import com.etybeno.ecommerce.controller.rest.CategoryRestController;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by thangpham on 28/12/2017.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoryRestControllerTest extends BaseRestControllerTest<CategoryRestController> {

    @Test
    @WithMockUser(username = "admin", authorities = "administrator")
    public void testCase1CreateNewCategory() throws Exception {
        Category category = createCategoryDTO(null);
        postPerform("/create", category).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(Status.SUCCESS.getValue()))
                .andExpect(jsonPath("$.data").value("OK"));
        //Try to insert a duplicate data
        postPerform("/create", category).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(Status.ERROR.getValue()));
    }

    @Test
    public void testCase2UpdateCategory() throws Exception {
//        getPerform("").andExpect(status().isOk())
//                .andExpect(jsonPath("$.status").value(Status.SUCCESS.getValue()))
//                .andExpect(jsonPath("$.data").value("OK"));
        //
//        ResultActions getPerform = getPerform("/getEmployeeInfo", "cashier_id", employeeInfoDTO.getCashierId() + "");
//        getPerform.andExpect(status().isOk())
//                .andExpect(jsonPath("$.status").value(Status.SUCCESS.getValue()));
//        EmployeeInfoDTO retrievedDTO = convertResponseToDTO(getPerform.andReturn().getResponse().getContentAsString());
//        AssertionErrors.assertEquals("Employee info is wrong", "Update_test", retrievedDTO.getName());
//        AssertionErrors.assertEquals("Employee info is wrong", Language.FRENCH, retrievedDTO.getLanguage());
    }
//

    @Test
    public void testCase3GetAll() throws Exception {
        getPerform("/getAll").andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(Status.SUCCESS.getValue()))
                .andExpect(jsonPath("$.count").isNumber());
    }

//    @Test
//    public void testCase3UpdatePermission() {
//        EmployeeInfoDTO employeeInfoDTO = createEmployeeInfoDTO();
//    }
//
    private Category createCategoryDTO(String parentId) {
        Category test = new Category();
        test.setName("Đồng Hồ");
        test.setSlug("dong-ho");
        test.setDescription("Đồng hồ hihihi");
        test.setParentId(parentId);
        return test;
    }
//
//    private EmployeeInfoDTO convertResponseToDTO(String data) throws IOException {
//        ResponseDataModel responseDataModel = StringUtil.OBJECT_MAPPER.readValue(data, ResponseDataModel.class);
//        return StringUtil.OBJECT_MAPPER.readValue(
//                StringUtil.OBJECT_MAPPER.writeValueAsString(responseDataModel.getData()), EmployeeInfoDTO.class);
//    }
}
