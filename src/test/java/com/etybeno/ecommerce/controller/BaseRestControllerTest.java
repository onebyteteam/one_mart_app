package com.etybeno.ecommerce.controller;

import com.etybeno.common.util.StringUtil;
import com.etybeno.ecommerce.EcommerceWebApplication;
import com.etybeno.ecommerce.configuration.WebSecurityConfig;
import com.etybeno.mongodb.config.MongoDBConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Created by thangpham on 28/12/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {EcommerceWebApplication.class, WebSecurityConfig.class})
@ContextConfiguration
@WebAppConfiguration
@ActiveProfiles("test")
public abstract class BaseRestControllerTest<T> {

    @Autowired
    WebApplicationContext context;

    protected MockMvc mockMvc;
    protected String pathAPI;

    protected int machine = 1000;
    protected int time = 946659600;
    protected int incre = 1000;

    protected ObjectId createNewObjectId() {
        return ObjectId.createFromLegacyFormat(time, machine, incre++);
    }

    @Before
    public void beforeTest() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        //get controller path-api
        ParameterizedType superclass = (ParameterizedType) this.getClass().getGenericSuperclass();
        Class<T> controllerClass = (Class<T>) superclass.getActualTypeArguments()[0];
        RequestMapping requestAnno = controllerClass.getAnnotation(RequestMapping.class);
        if (requestAnno.path().length == 0) pathAPI = requestAnno.value()[0];
        else if (requestAnno.value().length == 0) pathAPI = requestAnno.path()[0];
        else pathAPI = "";
    }

    @After
    public void afterTest() throws IOException, ConfigurationException {
        MongoDBConfiguration._load().getMongoDB("test").drop();
    }

    protected ResultActions postPerform(String api, Object object) throws Exception {
        return mockMvc.perform(post(pathAPI + api)
                .content(StringUtil.OBJECT_MAPPER.writeValueAsString(object))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
    }

    protected ResultActions putPerform(String api, Object object) throws Exception {
        return mockMvc.perform(put(pathAPI + api)
                .content(StringUtil.OBJECT_MAPPER.writeValueAsString(object))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
    }

    protected ResultActions getPerform(String api, String... params) throws Exception {
        MockHttpServletRequestBuilder getPerform = get(pathAPI + api);
        for (int i = 0; i < params.length; i += 2) {
            getPerform.param(params[i], params[i + 1]);
        }
        return mockMvc.perform(getPerform);
    }

}