var categoryOptions = Vue.component('category-options', {
    props: {categories: Array, locale: String, selCategories: Array},
    data: function () { return {categoryElements: new Array()} },
    methods: {
        flatCategories: function () {
            var flatCates = new Array();
            for (var i in this.categories) this.flatCategory(flatCates, this.categories[i], 0);
            this.categoryElements = flatCates;
        },
        flatCategory: function (categoryElements, category, depth) {
            if (depth == 1) category.depth = {'padding-left': '3em'};
            else if (depth == 2) category.depth = {'padding-left': '6em'};
            else if (depth == 3) category.depth = {'padding-left': '9em'};
            categoryElements.push(category);
            if (category.children && category.children.length > 0) {
                for (var i in category.children) this.flatCategory(categoryElements, category.children[i], depth + 1);
                category.disabled = true; category.children = null;
            } else if (typeof this.selCategories !== "undefined" && this.selCategories.length > 0) {
                for (var x in this.selCategories) {
                    if(this.selCategories[x].selectId === category.select_id) category.selected = true;
                }
            }
        }
    },
    template: '<select name="categories" id="categories" multiple class="form-control" :size="categoryElements.length / 2">' +
    '<option v-for="cate in categoryElements" :style="cate.depth" :disabled="cate.disabled" ' +
    ':value="cate.select_id" :selected="cate.selected"> {{ cate.i18n_name[locale.toString()] }}</option>' +
    '</select>'
});

var categorySelect = Vue.component('category-select', {
    props: {categories: Array, locale: String, selCategory: Array},
    data: function () {
        return {categoryElements: ''}
    },
    methods: {
        flatCategories: function () {
            var flatCates = new Array();
            for (var i in this.categories) this.flatCategory(flatCates, this.categories[i], 0);
            this.categoryElements = flatCates;
        },
        flatCategory: function (categoryElements, category, depth) {
            if (depth == 1) category.depth = {'padding-left': '3em'};
            else if (depth == 2) category.depth = {'padding-left': '6em'};
            else if (depth == 3) category.depth = {'padding-left': '9em'};
            categoryElements.push(category);
            if (category.children && category.children.length > 0) {
                for (var i in category.children) this.flatCategory(categoryElements, category.children[i], depth + 1);
                category.children = null;
            }
            if (typeof this.selCategory !== "undefined") {
                if (this.selCategory.selectId === category.select_id) category.selected = true;
            }
        }
    },
    template: '<select name="categories" id="categories" class="form-control" :size="categoryElements.length / 2">' +
    '<option v-for="c in categoryElements" :style="c.depth" :value="c.slug" :selected="c.selected">{{c.name}}</option>' +
    '</select>'
});

Vue.component('v-multiselect', window.VueMultiselect.default);