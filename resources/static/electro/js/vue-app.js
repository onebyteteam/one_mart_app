Vue.use(VueRouter);
var loadAllCategories = new Vue({
    el: '#load_categories',
    data: {
        categories: '', locale: ''
    }, updated: function () {
        buildDeparmentsMenu();
    }, methods: {
        fetchData: function (locale) {
            this.locale = locale;
            if (document.getElementById('load_categories') === null) return;
            this.$http.get('/api/category/v1/getAll').then(function (response) {
                this.categories = response.body.data;
            }, function (data, status, request) {
                console.log('status ' + status)
            });
        }
    }
});

var loadFeaturedProducts = new Vue({
    el: '#load_featured_products',
    data: {
        featured_products: '', locale: '', chunk_size: 4
    }, methods: {
        fetchData: function (locale, feature, category, page, size) {
            if (document.getElementById('load_featured_products') === null) return;
            this.locale = locale;
            this.$http.get('/api/product/v1/getFeatured?feature='.concat(feature, '&category=', category, '&page=', page, '&size=', size)
            ).then(function (response) {
                this.featured_products = response.body.data;
            }, function (data, status, request) {
                console.log('status' + status);
            })
        }
    }, computed: {
        chunkProducts: function () {
            return chunkArray(this.featured_products, this.chunk_size)
        },
        chunkSize: {
            set: function (chunkSize) {
                this.chunk_size = chunkSize;
            }
        }
    }
});

var loadRelatedProducts = new Vue({
    el: '#load_related_products',
    data: {
        related_products: '', locale: ''
    }, methods: {
        fetchData: function (sku, locale) {
            if (document.getElementById('load_related_products') === null) return;
            this.locale = locale;
            this.$http.get('/api/product/v1/getRelated?sku=' + sku).then(function (response) {
                this.related_products = response.body.data
            }, function (data, status, request) {
                console.log('status ' + status)
            });
        }
    }, computed: {
        chunkProducts: function (size) {
            return chunkArray(related_products, size);
        }
    }
});

var formMethods = new Vue({
    el: '#form_methods',
    methods: {
        submit: function (formId) {
            document.getElementById(formId).submit();
        }
    }
});

var cartDisplay = new Vue({
    el: '#cart_display',
    data: {
        product_num: '0',
        products: ''
    }, created: function () {
        this.fetchData();
    }, methods: {
        fetchData: function () {
            if (document.getElementById('cart_display') === null) return;
            var cookieStr = this.$cookies.get('om.cart');
            if (cookieStr != null) {
                var cookieValues = Base64.decode(cookieStr).split("|");
                for (var i = 0; i < cookieValues.length; i++) {
                    var keyValue = cookieValues[i].split(":");
                    if (keyValue.length != 2) continue;
                    if (keyValue[0] == 'cart.np') this.product_num = keyValue[1];
                }
            }
        }
    }
});

var addToCart = new Vue({
    el: '#add_to_cart',
    data: {
        errors: [],
        quantity: 1
    },
    methods: {
        add: function () {
            this.errors = [];
            if (!this.quantity || this.quantity < 1) {
                this.errors.push("Số lượng phải là số nguyên dương.");
                return;
            }
            loadingScreen.isLoading = true;
            var params = {};
            params['quantity'] = this.quantity;
            params['product_sku'] = this.$refs.product_sku.value;
            this.$http.post('/api/shopping/v1/addProduct', params, headers = {'Content-Type': 'application/json;charset=utf-8;'}).then(function (response) {
                cartDisplay.fetchData();
                document.getElementById("cart_display").scrollIntoView();
                loadingScreen.isLoading = false;
            }, function (data, status, request) {
                console.log('status ' + status);
                loadingScreen.isLoading = false;
            });
        }
    }
});

var manageCart = new Vue({
    el: '#manage_cart',
    methods: {
        deleteFromCart: function ($event) {
            loadingScreen.isLoading = true;
            var sku = $event.target.getAttribute('value');
            this.$http.delete('/api/shopping/v1/deleteProduct?product_sku=' + sku).then(function (response) {
                cartDisplay.fetchData();
                loadingScreen.isLoading = false;
                location.reload()
            }, function (data, status, request) {
                console.log('status ' + status);
                loadingScreen.isLoading = false;
            })
        }
    }
});

var loadingScreen = new Vue({
    el: '#loading-section',
    data: {
        isLoading: false
    }
})

var productBar = new Vue({el: '#product-control-bar'});

var loadingAttributes = new Vue({
    el: '#loading-attribute',
    data: {
        attributes: '',
        filterMap: {}
    },
    created: function () {
        var url = new URL(decodeURIComponent(window.location.href)).searchParams.get('aq');
        if (!url) return;
        var aql = url.split(',');
        for (var i = 0; i < aql.length; i++) {
            var filter = aql[i].split(':');
            this.filterMap[filter[0]] = filter[1].split('|');
        }
    },
    methods: {
        fetchData: function (attributes) {
            this.attributes = attributes;
            for (var key in this.attributes) {
                var attrGroup = this.attributes[key];
                if (this.filterMap[attrGroup.slug]) {
                    var values = this.filterMap[attrGroup.slug];
                    for (var j in attrGroup.children) {
                        var child = attrGroup.children[j];
                        attrGroup.children[j].checked = values.includes(child.slug + '-' + child.selectId);
                    }
                }
            }
        },
        addAttribute: function (parentAttribute, childAttribute) {
            var pSlug = parentAttribute.slug, cSlug = childAttribute.slug + '-' + childAttribute.selectId;
            var values = this.filterMap[pSlug];
            if (values instanceof Array && values.length > 0) {
                if (!values.includes(cSlug)) values.push(cSlug);
                else {
                    values.splice(values.indexOf(cSlug), 1);
                    if (values.length == 0) delete this.filterMap[pSlug];
                }
            } else this.filterMap[pSlug] = [cSlug];
            var query = Object.keys(this.filterMap)
                    .map(k => encodeURIComponent(k) + ':' + encodeURIComponent(this.filterMap[k].join('|'))
            ).
            join(',');
            var finalQuery = decodeURIComponent(window.location.origin + window.location.pathname +
                (query && query.length > 0 ? ('?aq=' + query) : ''));
            window.location.href = finalQuery;
        }
    }
})

var validateLoginForm = new Vue({
    el: '#login_form',
    data: {
        username: '', password: '',
        valUNMess: '', valPassMess: ''
    },
    methods: {
        checkUN: function () {
            this.valUNMess = '';
            if (_checkUsername(this.username)) return true;
            else {
                this.valUNMess = '<p>Xin hãy nhập tên tài khoản: từ 5-24 kí tự, bao gồm chữ cái, số, _ và -</p>';
                return false;
            }
        },
        checkPW: function () {
            this.valPassMess = '';
            if (_checkPassword(this.password)) return true;
            else {
                this.valPassMess = '<p>Mật khẩu bắt đầu bằng chữ cái, 6-30 kí tự, ít nhất một số, bao gồm chữ cái, số và _ - @ !</p>'
                return false;
            }
        },
        validateForm: function () {
            var allPass = true;
            allPass &= this.checkUN();
            allPass &= this.checkPW();
            if (!allPass) this.$el.scrollIntoView();
            else this.$el.submit();
        }
    },
    watch: {
        username: function (newVal, oldVal) {
            this.checkUN();
        },
        password: function (newVal, oldVal) {
            this.checkPW();
        }
    }
})

var validateRegistryForm = new Vue({
    el: '#registry_form',
    data: {
        firstName: '', username: '',
        email: '', password: '', repassword: '',
        valFNMess: '', valUNMess: '', valEmailMess: '', valPassMess: '', valRePassMess: ''
    },
    methods: {
        checkFN: function () {
            this.valFNMess = '';
            if (_checkName(this.firstName)) return true;
            else {
                this.valFNMess = '<p>Hãy nhập tên của bạn: tối đa 5 chữ, mỗi chữ 2-15 kí tự</p>';
                return false;
            }
        },
        checkUN: function () {
            this.valUNMess = '';
            if (_checkUsername(this.username)) return true;
            else {
                this.valUNMess = '<p>Hãy nhập tên tài khoản: từ 5-24 kí tự, bao gồm chữ cái, số, _ và -</p>';
                return false;
            }
        },
        checkEmail: function () {
            this.valEmailMess = '';
            if (_checkEmail(this.email)) return true;
            else {
                this.valEmailMess = '<p>Hãy nhập email, vd: abcdef@xyz.com</p>';
                return false;
            }
        },
        checkPW: function () {
            this.valPassMess = '';
            if (_checkPassword(this.password)) return true;
            else {
                this.valPassMess = '<p>Mật khẩu bắt đầu bằng chữ cái, 6-30 kí tự, ít nhất một số, bao gồm chữ cái, số và _ - @ !</p>'
                return false;
            }
        },
        checkRePW: function () {
            this.valRePassMess = '';
            if (this.password === this.repassword) return true;
            else {
                this.valRePassMess = '<p>Nhập lại mật khẩu phải trùng với mật khẩu</p>'
                return false;
            }
        },
        validateForm: function () {
            var allPass = true;
            allPass &= this.checkFN();
            allPass &= this.checkUN();
            allPass &= this.checkEmail();
            allPass &= this.checkPW();
            allPass &= this.checkRePW();
            if (!allPass) this.$el.scrollIntoView();
            else this.$el.submit();
        }
    },
    watch: {
        firstName: function (newVal, oldVal) {
            this.checkFN();
        },
        username: function (newVal, oldVal) {
            this.checkUN();
        },
        email: function (newVal, oldVal) {
            this.checkEmail();
        },
        password: function (newVal, oldVal) {
            this.checkPW();
        },
        repassword: function (newVal, oldVal) {
            this.checkRePW();
        }
    }
})

var updateUserInfoForm = new Vue({
    el: '#user_info_form',
    data: {
        firstName: '', lastName: '', username: '',
        email: '', oldPassword: '', newPassword: '', reNewPassword: '', changePassword: false,
        valFNMess: '', valLNMess: '', valOldPassMess: '', valNewPassMess: '', valReNewPassMess: ''
    },
    methods: {
        fetchData: function (data) {
            this.firstName = data.firstName;
            this.lastName = data.lastName;
            this.username = data.username;
            this.email = data.email;
        },
        checkFN: function () {
            this.valFNMess = '';
            if (_checkName(this.firstName)) return true;
            else {
                this.valFNMess = '<p>Hãy nhập tên của bạn: tối đa 5 chữ, mỗi chữ 2-15 kí tự</p>';
                return false;
            }
        },
        checkLN: function () {
            this.valLNMess = '';
            if (_checkName(this.lastName)) return true;
            else {
                this.valLNMess = '<p>Hãy nhập tên của bạn: tối đa 5 chữ, mỗi chữ 2-15 kí tự</p>';
                return false;
            }
        },
        // checkOPW: function() {
        //     if(!this.changePassword) return;
        //     this.valLNMess = '';
        //     if(!_checkPassword(this.oldPassword)) this.valOldPassMess =
        //         '<p>Mật khẩu cũ bắt đầu bằng chữ cái, từ 6-30 kí tự, chứa ít nhất một số, bao gồm _ - @ !</p>';
        // },
        checkNPW: function () {
            if (!this.changePassword) return true;
            this.valNewPassMess = '';
            if (_checkPassword(this.newPassword)) return true;
            else {
                this.valNewPassMess =
                    '<p>Mật khẩu mới bắt đầu bằng chữ cái, 6-30 kí tự, ít nhất một số, bao gồm chữ cái, số và _ - @ !</p>'
                return false;
            }
        },
        checkReNPW: function () {
            if (!this.changePassword) return true;
            this.valReNewPassMess = '';
            if (this.newPassword === this.reNewPassword) return true;
            else {
                this.valReNewPassMess = '<p>Nhập lại mật khẩu phải trùng với mật khẩu mới</p>';
                return false;
            }
        },
        validateForm: function (e) {
            var allPass = true;
            allPass &= this.checkFN();
            allPass &= this.checkLN();
            allPass &= this.checkNPW();
            allPass &= this.checkReNPW();
            if (!allPass) {
                this.$el.scrollIntoView();
                e.preventDefault();
                return;
            }
            // else this.$el.submit();
        }
    },
    watch: {
        firstName: function (newVal, oldVal) {
            this.checkFN();
        },
        lastName: function (newVal, oldVal) {
            this.checkLN();
        },
        // oldPassword: function(newVal, oldVal) { this.checkOPW(); },
        newPassword: function (newVal, oldVal) {
            this.checkNPW();
        },
        reNewPassword: function (newVal, oldVal) {
            this.checkReNPW();
        }
    }
})
