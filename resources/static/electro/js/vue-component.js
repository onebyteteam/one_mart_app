//=============== COMPONENT MODULARS
var loadingComponent = Vue.component('loading-component', {
    data: function () {
        return {
            'message': 'Xin chờ trong giây lát...',
            'url': '/electro/images/loading.gif'
        }
    },
    template: '<div class="loading-container">' +
    '<div class="loading-backdrop"><div class="loading">' +
    '<div class="loading-icon"> <img :src="url" v-if="url"/> </div>' +
    '<div class="loading-label"> <h5>{{message}}</h5> </div>' +
    '</div></div>' +
    '</div>'
})

var sortingComponent = Vue.component('sorting-product', {
    props: { params: '', sortVal: 'none'},
    created: function () {
        this.params = _getUrlVars();
        this.sortVal = typeof this.params['st'] === "undefined" ? 'popularity' : this.params['st'];
    },
    watch: {
        sortVal: function(newVal, oldVal) {
            if(newVal === 'none' || newVal === this.params['st']) return;
            var params = _getUrlVars(); params['st'] = newVal;
            var finalUrl = decodeURIComponent(window.location.origin + window.location.pathname + '?' +_parseVarsToUrl(params));
            window.location.href=finalUrl;
        }
    },
    template: '<form class="woocommerce-ordering" method="get">'
    + '<select name="sort" class="orderby" v-model="sortVal">'
    + '<option value="popularity" >Bán chạy nhất</option>'
    + '<option value="rating" >Đánh giá tốt nhất</option>'
    + '<option value="date" >Hàng mới nhất</option>'
    + '<option value="price" >Giá từ thấp đến cao</option>'
    + '<option value="price-desc">Giá từ cao đến thấp</option>'
    + '</select>'
    + '</form>'
})

var attributeComponent = Vue.component('attribute-item', {
    props: {'attribute': Array, 'key': String},
    data: function() { return { dSize: 3} },
    computed: {
        dAttribute: function () {
            if(this.attribute.children.length < 1) return null;
            var dAttribute = Object.assign({}, this.attribute);
            if(dAttribute.children.length > this.dSize) dAttribute.children = dAttribute.children.slice(0, this.dSize);
            return dAttribute;
      }
    },
    template: '<aside class="widget woocommerce widget_layered_nav" v-if="dAttribute">' +
        '<h3 class="widget-title">{{ dAttribute.name }}</h3>' +
        '<ul>' +
            '<li v-for="attr in dAttribute.children" v-bind:class="{ chosen: attr.checked }">' +
            '<a href="#" @click.prevent="loadingAttributes.addAttribute(dAttribute, attr)">{{ attr.name }}</a>' +
            '</li>' +
        '</ul>' +
        '<p v-if="attribute.children.length > dAttribute.children.length" class="maxlist-more">' +
            '<a href="#" @click.prevent="dSize+=dSize">+ Xem thêm</a></p>' +
    '</aside>'
})